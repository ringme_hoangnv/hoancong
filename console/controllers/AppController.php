<?php
/**
 * User: TheCodeholic
 * Date: 12/29/2020
 * Time: 3:08 PM
 */

namespace console\controllers;


use yii\console\Controller;
use yii\helpers\Console;
use backend\models\User;
use Yii;

/**
 * Class AppController
 *
 * @author  Zura Sekhniashvili <zurasekhniashvili@gmail.com>
 * @package console\controllers
 */
class AppController extends Controller
{
    public function actionCreateAdminUser($username, $password = null)
    {
        $user = new \backend\models\User();
//        $user->setScenario(User::SCENARIO_CREATE_USER);
        $user->is_first_login = 1;
        $user->status = 1;
        $user->email = $username.'@example.com';
        $user->username = $username;
        $user->user_type = 'admin';
        $user->fullname = 'hoangnv';
        $user->address = 'aaaa';
        $user->status = User::STATUS_ACTIVE;
//        $user->auth_key = \Yii::$app->security->generateRandomString();
        $user->auth_key = 'HEZc-o05jDfUwFLtIA5Op-cAQnSwhSjY';
        $password = $password ?: \Yii::$app->security->generateRandomString(8);
        $user->created_at = '1629151878';
        $user->updated_at = '1629151878';
        $user->setPassword($password);
        if ($user->save()) {
            $this->assignRole($user,true);
            Console::output("User has been created");
            Console::output("Username: ".$username);
            Console::output("Password: ".$password);
        } else {
            Console::error("User \"$username\" was not created");
            var_dump($user->errors);
        }

    }

    public function assignRole($backendUser, $isCreateNew = false)
    {
        Console::output($backendUser);
        // Gan quyen theo usertype
        $role = Yii::$app->authManager->getRole($backendUser->user_type);
        Yii::$app->authManager->assign($role, $backendUser->id);
        // Neu user cap thap --> ko cho tao admin
    }
}