<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class BtsPlan extends \common\models\BtsPlanBase {

    public $selected_item_ids;

    public function rules()
    {
        return [
            [['name', 'start_at', 'end_at', 'bts_code', 'item_category_id', 'partner_id'], 'required'],
            [['start_at', 'end_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'selected_item_ids'], 'safe'],
            [['partner_id', 'item_category_id', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['is_exported', 'status'], 'integer'],
            [['bts_code'], 'string', 'max' => 200],
            [['selected_item_ids'], 'safe'],

            [['postes_num', 'ret_num', 'retenida_num', 'panel_solar_num', 'vientos_num', 'cuerpos_num', 'pozos_num'], 'required'],
            [['postes_num', 'ret_num', 'retenida_num', 'panel_solar_num', 'vientos_num', 'cuerpos_num', 'pozos_num'], 'integer'],

            [['end_at'], 'validateEndTime'],
            ['item_category_id', 'unique', 'targetAttribute' => ['bts_code', 'item_category_id'],
                'message' => Yii::t('backend', 'This category has already been taken')
            ]


//            ['start_at', 'date', 'timestampAttribute' => 'start_at'],
//            ['end_at', 'date','timestampAttribute' => 'end_at'],
//            ['start_at', 'compare', 'compareAttribute'=> 'end_at', 'operator' => '<',
//                'enableClientValidation' =>true],
        ];
    }


    public function validateEndTime($attribute, $params) {

        if ($this->start_at && $this->end_at) {
            if (strtotime($this->start_at) > strtotime($this->end_at)) {
                $this->addError($attribute, Yii::t('backend', 'End time must be larger than start time!'));
                return false;
            }
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Statement number'),
            'description' => Yii::t('backend', 'Description'),
            'is_exported' => Yii::t('backend', 'Is Exported'),
            'status' => Yii::t('backend', 'Status'),
            'start_at' => Yii::t('backend', 'Start At'),
            'end_at' => Yii::t('backend', 'End At'),
            'partner_id' => Yii::t('backend', 'Partner'),
            'bts_code' => Yii::t('backend', 'BTS'),
            'item_category_id' => Yii::t('backend', 'Item Category'),
            'created_by' => Yii::t('backend', 'Created By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }


//    public function save($runValidation = true, $attributeNames = NULL) {
//
//
//        if(!$this->isNewRecord) {
//            $currentItems = $this->getItemsIds();
//
//            if (count($currentItems)) {
//                foreach ($currentItems as $index => $item) {
//
//                    // Neu truoc do da chon --> xoa bot di
//                    $exists = false;
//                    if (is_array($this->selected_item_ids) && count($this->selected_item_ids)) {
//                        foreach ($this->selected_item_ids as $selectedId) {
//                            if($selectedId == $item->item_id) {
//                                $exists = true;
//                                break;
//                            }
//                        }
//
//                        // neu bo chon --> xoa di
//                        if($exists === false) {
//                            BtsPlanItem::deleteAll([
//                                'plan_id' => $this->id,
//                                'item_id' => $item->item_id,
//                            ]);
//
//                        }
//                    } else {
//
//                        BtsPlanItem::deleteAll([
//                            'plan_id' => $this->id,
//                        ]);
//                    }
//
//                }
//
//            }
//
//            // them nhung item moi
//            if (is_array($this->selected_item_ids) && count($this->selected_item_ids)) {
//                foreach ($this->selected_item_ids as $item) {
//                    // tag co san
//                    $exists = false;
//
//                    foreach ($currentItems as $item) {
//                        if($item->item_id == $item) {
//                            $exists = true;
//                            break;
//                        }
//                    }
//
//                    // neu post chua co tag nay thi gan tag nay vao post
//                    if($exists === false) {
//                        $tag = new BtsPlanItem();
//                        $tag->plan_id = $this->id;
//                        $tag->item_id = $item;
//                        $tag->save();
//                    }
//
//                }
//            }
//
//
//            return parent::save($runValidation, $attributeNames);
//
//        } else {
//            $return = parent::save($runValidation, $attributeNames);
//            if (is_array($this->selected_item_ids) && count($this->selected_item_ids)) {
//                foreach ($this->selected_item_ids as $item) {
//                    // tag co san
//                    if ($item) {
//                        $tag = new BtsPlanItem();
//                        $tag->plan_id = $this->id;
//                        $tag->item_id = $item;
//                        $tag->save();
//                    }
//                }
//            }
//
//            return $return;
//        }
//    }


    public function getItemsIds()
    {
        return BtsPlanItem::find()
            ->select('item_id')
            ->where(
                ['plan_id' => $this->id]
            )
            ->all();
    }

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_item_ids = ArrayHelper::map($this->getItemsIds(), 'item_id', 'item_id');

        }

        return parent::loadDefaultValues($skipIfSet);
    }
}