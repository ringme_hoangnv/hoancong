<?php

namespace backend\controllers;

use backend\models\BtsItem;
use backend\models\BtsPlanItemImage;
use common\core\SuperAppApiGw;
use common\models\BtsPlanItemBase;
use common\models\BtsPlanItemImageBase;
use common\models\SystemSettingBase;
use Yii;
use backend\models\BtsPlan;
use backend\models\BtsPlanSearch;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\validators\ImageValidator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use yii\web\UploadedFile;


class BtsPlanItemImageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionApprove()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

//        if (!Yii::$app->user->can('admin') || !Yii::$app->user->can('ho')) {
//            $response = [
//                'error_code' => 403,
//                'message' => Yii::t('backend', 'Please contact admin to do this function')
//            ];
//            return $response;
//        }

        $response = [];

        $id = Yii::$app->request->post('id');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            $model->status = BtsPlanItemImage::STATUS_APPROVED;
            $model->approved_by = Yii::$app->user->identity->getId();
            $model->approved_at = date('Y-m-d H:i:s');
            $model->note = null;
            $model->save(false);

            // update item status
            BtsPlanItemBase::updateStatus($model->plan_id, $model->item_id);

            $response['error_code'] = 0;
            $response['message'] = Yii::t('backend', 'Approve image successfully!');
        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }

        return $response;
    }

    public function actionDisapprove()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

//        if (!Yii::$app->user->can('admin') && !Yii::$app->user->can('ho')) {
//            $response = [
//                'error_code' => 403,
//                'message' => Yii::t('backend', 'Please contact admin to do this function')
//            ];
//            return $response;
//        }

        $response = [];

        $id = Yii::$app->request->post('id');
        $reason = Yii::$app->request->post('reason');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            if ($reason) {
                $model->note = $reason;
                $model->status = BtsPlanItemImage::STATUS_DISAPPROVED;
                $model->approved_at = date('Y-m-d H:i:s');
                $model->approved_by = Yii::$app->user->identity->getId();
                $model->save(false);

                // update item status
                BtsPlanItemBase::updateStatus($model->plan_id, $model->item_id);

                // Gui tin nhan cho partner
                $user = Yii::$app->user->identity;
                if ($user->user_type == 'ho') {
                    $plan = $model->plan;
                    $partner = $plan->partner;
                    $partnerNumbers = explode(',', $partner->msisdn_warning);

                    if (count($partnerNumbers)) {
                        $msg = SystemSettingBase::getConfigByKey('MT_HO_DISAPPROVE_IMAGE');
                        //HO {ho_user} disapproved an image in plan {plan_name} with note: {note}
                        $msg = Yii::t('backend', $msg, [
                            'ho_user' => $user->username,
                            'plan_name' => $plan->name,
                            'note' => $reason,
                        ]);


                        SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $partnerNumbers, $msg);
                    }

                }

                $response['error_code'] = 0;
                $response['message'] = Yii::t('backend', 'Disapprove image successfully!');
            } else {
                // Bat buoc nhap
                $response['error_code'] = 400;
                $response['message'] = Yii::t('backend', 'Reason is required!');
            }

        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }

        return $response;
    }

    public function actionDelete()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [
            'error_code' => 0,
            'message' => Yii::t('backend', 'Delete image successfully!')
        ];

        $id = Yii::$app->request->post('id');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            if ($model->status == BtsPlanItemImage::STATUS_APPROVED) {
                // Ko cho xoa anh da duoc duyet
                $response['error_code'] = 403;
                $response['message'] = Yii::t('backend', 'You cannot delete a approved image!');
                return $response;
            }

            $planId = $model->plan_id;
            $itemId = $model->item_id;

            $filePath = str_replace('/' . basename(Yii::$app->params['media_path']), '', Yii::$app->params['media_path']) . $model->image_path;
            $model->delete();

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            // update item status
            BtsPlanItemBase::updateStatus($planId, $itemId);

            $response['error_code'] = 0;
            $response['message'] = Yii::t('backend', 'Delete image successfully!');
        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }

        return $response;
    }

    /**
     * Show thong tin cua image
     * @param $id
     * @return string
     */
    public function actionInfo($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('/bts-plan/itemImageInfo', [
            'model' => BtsPlanItemImage::findOne($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Xu ly upload anh item
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionUpload()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $maxFileSize = Yii::$app->params['image_upload_size'] * 1024; //Kb
        if (Yii::$app->request->isPost) {
            $planId = Yii::$app->request->post('plan_id');
            $itemId = Yii::$app->request->post('item_id');
            $fileId = Yii::$app->request->post('fileId');

            $plan = BtsPlan::findOne($planId);
            $item = BtsItem::findOne($itemId);
            if (!$plan || !$item) {
                $res = ['error' => Yii::t('backend', 'Plan or item not found!')];
                return $res;
            }

            // check so luong image
            $planItem = BtsPlanItemBase::findOne([
                'plan_id' => $planId,
                'item_id' => $itemId,
            ]);
            $countImages = BtsPlanItemImageBase::find()
                ->where([
                    'plan_id' => $planId,
                    'item_id' => $itemId,
                ])
                ->count();
            if ($countImages >= $planItem->image_num) {
                $res = ['error' => Yii::t('backend', 'You cannot upload more image!', [
                    'total' => $planItem->image_num
                ])];
                return $res;
            }

            $res = [];
            $initialPreview = [];
            $initialPreviewConfig = [];
            $images = UploadedFile::getInstancesByName("image_upload");



            if (count($images) > 0) {
                foreach ($images as $key => $image) {
                    $validator = new ImageValidator();
                    if (!$validator->validate($image)) {
                        $res = ['error' => Yii::t('backend', 'Invalid image!')];
                        return ($res);
                    }

                    if ($image->size > $maxFileSize * 1024) {
                        $res = ['error' => Yii::t('backend', 'The maximum size of the image cannot exceed 2M')];
                        return ($res);
                    }
                    if (!in_array(strtolower($image->extension), array('gif', 'jpg', 'jpeg', 'png'))) {
                        $res = ['error' => Yii::t('backend', 'Please upload a standard image file, support gif, jpg, png and jpeg.')];
                        return ($res);
                    }
                    $dir = '/plan/'. date('ymd', strtotime($plan->created_at)). '/'. $planId. '/'. $itemId. '/';

                    $fileNameClean = preg_replace('/[^A-Za-z0-9\-]/', '', $image->baseName); // Removes special chars.
                    $fileNameClean = Inflector::camel2id($fileNameClean). '-'. time();
                    $filename = $fileNameClean . '.' . $image->getExtension();

                    $savePath = Yii::$app->params['media_path']. $dir;
                    // If the folder does not exist, create a new folder
                    if (!file_exists($savePath)) {
                        FileHelper::createDirectory($savePath);
                    }

                    $file = $savePath . $filename;

                    if ($image->saveAs($file)) {
                        // resize image neu qua to
                        if ($image->size > 1.5*1024*1024) {
                            Image::thumbnail($file, 1200, null)->save($file);
                        }

                        $imgpath = '/'. basename(Yii::$app->params['media_path']). $dir . $filename;
                        // Luu db
                        $imgObj = new BtsPlanItemImage();
                        $imgObj->plan_id = $planId;
                        $imgObj->item_id = $itemId;
                        $imgObj->image_path = $imgpath;
                        $imgObj->status = BtsPlanItemImage::STATUS_DRAFT;
                        $imgObj->save(false);

                        // update item status
                        BtsPlanItemBase::updateStatus($planId, $itemId);

                        $partnerUser = Yii::$app->user->identity;
                        if ($partnerUser->user_type == 'partner') {
                            $partner = $partnerUser->partner;
                            $hoNumbers = explode(',', SystemSettingBase::getConfigByKey('HO_PHONE_NUMBER'));
                            if (count($hoNumbers)) {
                                // Gui tin nhan cho HO
                                $msg = SystemSettingBase::getConfigByKey('MT_PARTNER_UPLOAD_NEW_IMAGE');
                                // Partner {partner_name}: user {partner_user} has just uploaded a new image in plan {plan_name}
                                $msg = Yii::t('backend', $msg, [
                                    'partner_name' => ($partner)? $partner->name: '',
                                    'partner_user' => $partnerUser->username,
                                    'plan_name' => $plan->name,
                                ]);

                                SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $hoNumbers, $msg);
                            }



                        }

                        $config = [
                            'caption' => $filename,
                            'width' => '120px',
                            'url' => Url::to(['bts-plan-item-image/delete', 'id' => $imgObj->id]), // server delete action
                            'key' => $imgObj->id,
                            'extra' => ['filename' => $filename]
                        ];
                        array_push($initialPreviewConfig, $config);

                        $res = [
                            "initialPreview" => $initialPreview,
                            "initialPreviewConfig" => $initialPreviewConfig,
                            "imgfile" => "<input name='image[]' id='" . $imgObj->id . "' type='hidden' value='" . $imgpath . "'/>",
                            'filename' => $filename,
                            'imagePath' => $imgObj->getImagePathUrl(),
                            'image_id' => $imgObj->id,
                            'file_id' => $fileId,
                        ];
                    }
                }
            }
            return $res;
        }
    }
}
