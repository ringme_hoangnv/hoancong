<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class SystemSettingBase extends \common\models\db\SystemSettingDB {

    public static function getAllConfig() {
        $cache = Yii::$app->cache;
        $key = 'system_setting_all';

        $data = $cache->get($key);

        if (!$data) {
            $settings =  self::find()
                ->select('config_key, config_value')
                ->asArray()
                ->all()
            ;

            if (!empty($settings)) {
                $data = ArrayHelper::map($settings, 'config_key', 'config_value');
            } else {
                $data = array();
            }

            $cache->set($key, $data, CACHE_TIMEOUT);
        }

        return $data;
    }

    public static function getConfigByKey($configKey) {

        if ($configKey == null) {
            return null;
        } else {
            $allConfig = self::getAllConfig();
            return (isset($allConfig[$configKey]))? $allConfig[$configKey]: null;
        }
    }
}