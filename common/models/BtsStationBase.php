<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use backend\models\User;

class BtsStationBase extends \common\models\db\BtsStationDB
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'bts_code' => Yii::t('backend', 'Bts Code'),
            'bts_name' => Yii::t('backend', 'Bts Name'),
            'branch_id' => Yii::t('backend', 'Branch'),
            'status' => Yii::t('backend', 'Status'),
            'direction' => Yii::t('backend', 'Direction'),
            'latlong' => Yii::t('backend', 'Latlong'),
            'bts_type' => Yii::t('backend', 'Bts Type'),
            'frequency_band' => Yii::t('backend', 'Frequency Band'),
            'antenna_num' => Yii::t('backend', 'Antenna Num'),
            'rru_num' => Yii::t('backend', 'Rru Num'),
            'tower_height' => Yii::t('backend', 'Tower Height'),
            'tower_type' => Yii::t('backend', 'Tower Type'),
            'anchorage_num' => Yii::t('backend', 'Anchorage Num'),
            'shelter_type' => Yii::t('backend', 'Shelter Type'),
            'ground_system_num' => Yii::t('backend', 'Ground System Num'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    public function getBranch()
    {
        return $this->hasOne(BranchBase::className(), ['id' => 'branch_id']);
    }
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}