<?php

namespace common\models;

use backend\models\User;
use common\helpers\Helpers;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class BtsItemBase extends \common\models\db\BtsItemDB
{

    public function getImageUploadNumberByPlan($plan) {
        $baseNum = $this->image_num;
        $uploadImageNum = $baseNum;
        $group = $this->group;

        switch ($group->calculate_image_method) {
            case 'pozos':
            case 'postes':
            case 'ret':
            case 'retenida':
            case 'vientos':
            case 'cuerpos':
                if ($group->calculate_image_method == 'ret') {
                    $fieldName = 'retenida_num';
                } else {
                    $fieldName = $group->calculate_image_method. '_num';
                }

                $uploadImageNum = $baseNum * $plan->$fieldName;
                break;

            case 'panel_solar':
                // 06-09-12-15-18	21-24	27-36
                // 4	6	8
                $fieldName = $group->calculate_image_method. '_num';
                $panelSolarNum = $plan->$fieldName;
                if ($panelSolarNum >= 6 && $panelSolarNum <= 18 ) {
                    $uploadImageNum = $baseNum * 4;
                } elseif ($panelSolarNum >= 21 && $panelSolarNum <= 24) {
                    $uploadImageNum = $baseNum * 6;
                } elseif ($panelSolarNum >= 27 && $panelSolarNum <= 36) {
                    $uploadImageNum = $baseNum * 8;
                }
                break;
            case 'base':
                // ko can xu ly
                break;
            default:

        }

        return $uploadImageNum;
    }

    public static function getActiveItemsArrByCate($cateId) {
        $return = [];
        $data = self::find()
            ->select('id, image_path, name')
            ->where(['category_id' => $cateId])
            ->andWhere(['status' => 1])
            ->orderBy('name ASC')
            ->asArray()
            ->all();
        if (count($data)) {
            $data = array_values($data);
            $return = $data;
        }

        return $return;
    }

    public static function listActiveItemsByCate($cateId) {
        return self::find()
            ->where(['category_id' => $cateId])
            ->andWhere(['status' => 1])
            ->orderBy('name ASC')

            ->all();
    }

    public function getImagePathUrl() {
        return Helpers::getMediaUrl($this->image_path);
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'category_id' => Yii::t('backend', 'Category'),
            'group_id' => Yii::t('backend', 'Item Group'),
            'image_path' => Yii::t('backend', 'Avatar'),
            'status' => Yii::t('backend', 'Status'),
            'note' => Yii::t('backend', 'Note'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
    public function getCategory()
    {
        return $this->hasOne(ItemCategoryBase::className(), ['id' => 'category_id']);
    }
    public function getGroup()
    {
        return $this->hasOne(ItemGroupBase::className(), ['id' => 'group_id']);
    }
    public function getCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


}