<?php

namespace common\models;

use backend\models\BtsItem;
use backend\models\BtsPlan;
use backend\models\User;
use common\helpers\Helpers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class BtsPlanItemImageBase extends \common\models\db\BtsPlanItemImageDB {

    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DISAPPROVED = 2;

    public function getThumbUrl($w = 200, $h = 200) {

        if ($this->image_path) {
            $pathInfo = pathinfo($this->image_path);


            $cacheDir = Yii::getAlias('@webroot'). '/cache/'. $pathInfo['dirname'];
            $absThumbPath = '/cache'. $pathInfo['dirname']. '/'. $pathInfo['filename']. "-$w-". ($h? $h: 'auto'). '.'. $pathInfo['extension'];
            $fullThumbPath = Yii::getAlias('@webroot'). '/'. $absThumbPath;
                if (file_exists($fullThumbPath)) {
                return $absThumbPath;
            } elseif (file_exists(Yii::getAlias('@webroot'). '/'. $this->image_path)) {
                FileHelper::createDirectory($cacheDir , $mode = 0775, $recursive = true);

                try {

                    Image::thumbnail(Yii::getAlias('@webroot'). '/'. $this->image_path, $w, $h)
                        ->save($fullThumbPath, ['quality' => 90]);

                    return $absThumbPath;

                } catch (\Exception $e) {
                    Yii::error('Loi khi tao anh thumb'. $e);
                    return '';
                }
            } else {
                return '';
            }

        }


        return $this->image_path;
    }

    public function getStatusName() {
        $name = $this->status;
        switch ($this->status) {
            case 0:
                $name = Yii::t('backend', 'Draft');
                break;
            case 1:
                $name = Yii::t('backend', 'Approved');
                break;
            case 2:
                $name = Yii::t('backend', 'Disapproved');
                break;
        }
        return $name;
    }

    public function getImagePathUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->image_path);
        } else {
            return Helpers::getMediaUrl($this->getThumbUrl($w, $h));
        }

    }

    public function getLocalImageUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getLocalMediaUrl($this->image_path);
        } else {
            return Helpers::getLocalMediaUrl($this->getThumbUrl($w, $h));
        }
    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getApprovedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'approved_by']);
    }

    public function getItem()
    {
        return $this->hasOne(BtsItem::className(), ['id' => 'item_id']);
    }
    public function getPlan()
    {
        return $this->hasOne(BtsPlan::className(), ['id' => 'plan_id']);
    }
}