<?php

namespace common\models;

use backend\models\ItemCategory;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class ItemGroupBase extends \common\models\db\ItemGroupDB
{

    public static function getGroupListByCate($cateId) {
        return self::getGroupByCateQuery($cateId)
            ->all();
    }
    public static function getGroupByCateQuery($cateId) {
        return self::find()
            ->where(['category_id' => $cateId])
            ->orderBy('NAME ASC');
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(ItemCategory::className(), ['id' => 'category_id']);
    }
}