<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bts_item".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_path
 * @property integer $category_id
 * @property integer $group_id
 * @property integer $status
 * @property string $note
 * @property integer $image_num
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property BtsPlanItemDB[] $btsPlanItems
 * @property BtsPlanDB[] $plans
 */
class BtsItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bts_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'group_id'], 'required'],
            [['category_id', 'group_id', 'image_num', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'image_path'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['note'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'group_id' => Yii::t('backend', 'Group ID'),
            'status' => Yii::t('backend', 'Status'),
            'note' => Yii::t('backend', 'Note'),
            'image_num' => Yii::t('backend', 'Image Num'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtsPlanItems()
    {
        return $this->hasMany(BtsPlanItemDB::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(BtsPlanDB::className(), ['id' => 'plan_id'])->viaTable('bts_plan_item', ['item_id' => 'id']);
    }
}
