<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "api_login_auth".
 *
 * @property string $msisdn
 * @property string $token
 * @property string $expired_at
 * @property string $created_at
 * @property string $client_useragent
 * @property string $client_os
 */
class ApiLoginAuthDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_login_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msisdn', 'token'], 'required'],
            [['expired_at', 'created_at'], 'safe'],
            [['msisdn', 'token', 'client_useragent'], 'string', 'max' => 255],
            [['client_os'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'msisdn' => Yii::t('backend', 'Msisdn'),
            'token' => Yii::t('backend', 'Token'),
            'expired_at' => Yii::t('backend', 'Expired At'),
            'created_at' => Yii::t('backend', 'Created At'),
            'client_useragent' => Yii::t('backend', 'Client Useragent'),
            'client_os' => Yii::t('backend', 'Client Os'),
        ];
    }
}
