<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bts_plan".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $is_exported
 * @property integer $status
 * @property string $start_at
 * @property string $end_at
 * @property integer $partner_id
 * @property string $bts_code
 * @property integer $item_category_id
 * @property integer $postes_num
 * @property integer $ret_num
 * @property integer $retenida_num
 * @property integer $panel_solar_num
 * @property integer $vientos_num
 * @property integer $cuerpos_num
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $approved_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BtsStationDB $btsCode
 * @property PartnerDB $partner
 * @property BtsPlanItemDB[] $btsPlanItems
 * @property BtsItemDB[] $items
 */
class BtsPlanDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bts_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_at', 'end_at', 'bts_code', 'item_category_id', 'ret_num', 'retenida_num', 'panel_solar_num', 'vientos_num', 'cuerpos_num', 'created_by', 'updated_by'], 'required'],
            [['start_at', 'end_at', 'created_at', 'updated_at'], 'safe'],
            [['partner_id', 'item_category_id', 'postes_num', 'retenida_num', 'panel_solar_num', 'vientos_num', 'cuerpos_num', 'created_by', 'updated_by', 'approved_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['is_exported', 'status'], 'string', 'max' => 1],
            [['bts_code'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'is_exported' => Yii::t('backend', 'Is Exported'),
            'status' => Yii::t('backend', 'Status'),
            'start_at' => Yii::t('backend', 'Start At'),
            'end_at' => Yii::t('backend', 'End At'),
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'bts_code' => Yii::t('backend', 'Bts Code'),
            'item_category_id' => Yii::t('backend', 'Item Category ID'),
            'postes_num' => Yii::t('backend', 'Postes Num'),
            'ret_num' => Yii::t('backend', 'Ret Num'),
            'retenida_num' => Yii::t('backend', 'Retenida Num'),
            'panel_solar_num' => Yii::t('backend', 'Panel Solar Num'),
            'vientos_num' => Yii::t('backend', 'Vientos Num'),
            'cuerpos_num' => Yii::t('backend', 'Cuerpos Num'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'approved_by' => Yii::t('backend', 'Approved By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtsCode()
    {
        return $this->hasOne(BtsStationDB::className(), ['bts_code' => 'bts_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(PartnerDB::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtsPlanItems()
    {
        return $this->hasMany(BtsPlanItemDB::className(), ['plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(BtsItemDB::className(), ['id' => 'item_id'])->viaTable('bts_plan_item', ['plan_id' => 'id']);
    }
}
