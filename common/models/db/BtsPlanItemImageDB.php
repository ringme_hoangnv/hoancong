<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bts_plan_item_image".
 *
 * @property integer $id
 * @property integer $plan_id
 * @property integer $item_id
 * @property string $image_path
 * @property integer $status
 * @property string $note
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $approved_at
 * @property integer $approved_by
 */
class BtsPlanItemImageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bts_plan_item_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'item_id', 'created_by', 'updated_by', 'approved_by'], 'integer'],
            [['plan_id', 'item_id', 'image_path'], 'required'],
            [['created_at', 'updated_at', 'approved_at'], 'safe'],
            [['image_path'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['note'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'plan_id' => Yii::t('backend', 'Plan ID'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'status' => Yii::t('backend', 'Status'),
            'note' => Yii::t('backend', 'Note'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'approved_at' => Yii::t('backend', 'Approved At'),
            'approved_by' => Yii::t('backend', 'Approved By'),
        ];
    }
}
