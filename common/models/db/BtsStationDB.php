<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bts_station".
 *
 * @property string $bts_code
 * @property string $bts_name
 * @property integer $branch_id
 * @property integer $status
 * @property string $direction
 * @property string $latlong
 * @property string $bts_type
 * @property integer $frequency_band
 * @property integer $antenna_num
 * @property integer $rru_num
 * @property double $tower_height
 * @property string $tower_type
 * @property integer $anchorage_num
 * @property string $shelter_type
 * @property integer $ground_system_num
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property BtsPlanDB[] $btsPlans
 */
class BtsStationDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bts_station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bts_code', 'bts_name', 'direction'], 'required'],
            [['frequency_band', 'antenna_num', 'rru_num', 'anchorage_num', 'ground_system_num'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['bts_code'], 'string', 'max' => 200],
            [['bts_name', 'direction'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['latlong'], 'string', 'max' => 100],
            [['bts_type', 'tower_type', 'shelter_type'], 'string', 'max' => 20],
            [['tower_height'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bts_code' => 'Bts Code',
            'bts_name' => 'Bts Name',
            'status' => 'Status',
            'direction' => 'Direction',
            'latlong' => 'Latlong',
            'bts_type' => 'Bts Type',
            'frequency_band' => 'Frequency Band',
            'antenna_num' => 'Antenna Num',
            'rru_num' => 'Rru Num',
            'tower_height' => 'Tower Height',
            'tower_type' => 'Tower Type',
            'anchorage_num' => 'Anchorage Num',
            'shelter_type' => 'Shelter Type',
            'ground_system_num' => 'Ground System Num',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtsPlans()
    {
        return $this->hasMany(BtsPlanDB::className(), ['bts_code' => 'bts_code']);
    }
}
