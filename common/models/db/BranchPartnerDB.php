<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "branch_partner".
 *
 * @property integer $branch_id
 * @property integer $partner_id
 * @property string $created_at
 */
class BranchPartnerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'branch_partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id', 'partner_id'], 'required'],
            [['branch_id', 'partner_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'branch_id' => 'Branch ID',
            'partner_id' => 'Partner ID',
            'created_at' => 'Created At',
        ];
    }
}
