<?php

namespace common\models\db;

use Yii;
class CronMsg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cron_msg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id','item_id','number_manager_local'],'integer'],
            [['sent_to','msg_local_manager'],'string'],
            [['sent_at','safe']]
        ];
    }
    public function checkTimeWork($time_now)
    {
        $time_now = strtotime($time_now);
        if($time_now >= strtotime('1:00:00') && $time_now < strtotime('9:00:00')) {
            return 1;
        }
        elseif(strtotime('9:00:00' )<= $time_now && $time_now <= strtotime('21:00:00')) {
            return 2;
        }
        return 3;
    }

}