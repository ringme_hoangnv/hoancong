<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "bts_plan_item".
 *
 * @property integer $plan_id
 * @property integer $item_id
 * @property integer $status
 *
 * @property BtsItemDB $item
 * @property BtsPlanDB $plan
 */
class BtsPlanItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bts_plan_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'item_id'], 'required'],
            [['plan_id', 'item_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'item_id' => 'Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(BtsItemDB::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(BtsPlanDB::className(), ['id' => 'plan_id']);
    }
}
