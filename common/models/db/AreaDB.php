<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property string $type
 * @property string $area_code
 * @property string $parent_code
 * @property string $cen_code
 * @property string $province
 * @property string $district
 * @property string $precinct
 * @property string $street_block
 * @property string $street
 * @property string $name
 * @property string $full_name
 * @property integer $order_no
 * @property string $pstn_code
 * @property string $provice_reference
 * @property integer $shop_owner_id
 * @property string $area_type
 */
class AreaDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_code'], 'required'],
            [['order_no', 'shop_owner_id'], 'integer'],
            [['type', 'cen_code', 'province', 'district', 'precinct'], 'string', 'max' => 10],
            [['area_code', 'parent_code', 'pstn_code'], 'string', 'max' => 15],
            [['street_block', 'street', 'name', 'full_name'], 'string', 'max' => 255],
            [['provice_reference', 'area_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('backend', 'Type'),
            'area_code' => Yii::t('backend', 'Area Code'),
            'parent_code' => Yii::t('backend', 'Parent Code'),
            'cen_code' => Yii::t('backend', 'Cen Code'),
            'province' => Yii::t('backend', 'Province'),
            'district' => Yii::t('backend', 'District'),
            'precinct' => Yii::t('backend', 'Precinct'),
            'street_block' => Yii::t('backend', 'Street Block'),
            'street' => Yii::t('backend', 'Street'),
            'name' => Yii::t('backend', 'Name'),
            'full_name' => Yii::t('backend', 'Full Name'),
            'order_no' => Yii::t('backend', 'Order No'),
            'pstn_code' => Yii::t('backend', 'Pstn Code'),
            'provice_reference' => Yii::t('backend', 'Provice Reference'),
            'shop_owner_id' => Yii::t('backend', 'Shop Owner ID'),
            'area_type' => Yii::t('backend', 'Area Type'),
        ];
    }
}
