<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

class AuthorBase extends \common\models\db\AuthorDB {

    public function getWebDetailUrl() {
        return Url::to(['author/detail', 'author_slug' => $this->slug, 'author_id' => $this->id]);
    }
}