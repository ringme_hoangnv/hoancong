<?php

namespace common\models;

use backend\models\Branch;
use backend\models\BtsPlan;
use backend\models\User;
use Yii;
use yii\helpers\ArrayHelper;

class SystemSettingBase extends \common\models\db\SystemSettingDB {

    public static function getAllConfig() {
        $cache = Yii::$app->cache;
        $key = 'system_setting_all';

        $data = $cache->get($key);

        if (!$data) {
            $settings =  self::find()
                ->select('config_key, config_value')
                ->asArray()
                ->all()
            ;

            if (!empty($settings)) {
                $data = ArrayHelper::map($settings, 'config_key', 'config_value');
            } else {
                $data = array();
            }

            $cache->set($key, $data, CACHE_TIMEOUT);
        }

        return $data;
    }

    public static function getConfigByKey($configKey) {

        if ($configKey == null) {
            return null;
        } else {
            $allConfig = self::getAllConfig();
            return (isset($allConfig[$configKey]))? $allConfig[$configKey]: null;
        }
    }

    public static function getNumberQlVung($planid) {
        $plan = BtsPlan::find()->where(['id' => $planid ])->one();
        $user_create = User::find()->where(['id' => $plan->created_by])->one();
        // lay so cua nhan vien quan ly vung
        if($user_create->user_type == 'ho' && $user_create->ho_type == 'Nhân viên quản lí vùng'){
            return $user_create->msisdn;
        }
        elseif ($user_create->user_type == 'branch') {
            $branch = Branch::find()->where(['id' => $user_create->branch_id ])->one();
            $ho = User::find()->where(['id' => $branch->ho_user_id])->one();
            return $ho->msisdn;
        }
    }


}