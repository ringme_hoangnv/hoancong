<?php

namespace common\models;

use backend\models\BtsPlan;
use backend\models\BtsPlanItemImage;
use Yii;

class BtsPlanItemBase extends \common\models\db\BtsPlanItemDB
{
    // 0: chua upload du image, 1: da upload du & cho duyet, 2: co image bi tu choi 3: da duyet
    const STATUS_NOT_YET_UPLOADED = 0;
    const STATUS_UPLOADED_WAIT_APPROVE = 1;
    const STATUS_HAVE_DISAPPROVED = 2;
    const STATUS_APPROVED = 3;

    public static function updateStatus($planId, $itemId) {
        $planItem = self::findOne([
            'plan_id' => $planId,
            'item_id' => $itemId
        ]);
        if ($planItem) {
            $imageQuery = BtsPlanItemImageBase::find()
                ->where([
                    'plan_id' => $planId,
                    'item_id' => $itemId,
                ])
                ;
            $totalDisapproveImageQuery = clone $imageQuery;
            $totalApproveImageQuery = clone $imageQuery;

            $totalImage = $imageQuery->count();
            $totalDisapproveImage = $totalDisapproveImageQuery->andWhere(['status' => BtsPlanItemImageBase::STATUS_DISAPPROVED])->count();
            $totalApproveImage = $totalApproveImageQuery->andWhere(['status' => BtsPlanItemImageBase::STATUS_APPROVED])->count();
            if ($totalImage == 0) {
                $planItem->status = self::STATUS_NOT_YET_UPLOADED;
            } else {
                if ($totalImage < $planItem->image_num) {
                    $planItem->status = self::STATUS_NOT_YET_UPLOADED;
                } else {
                    // so anh = yeu cau
                    if ($totalDisapproveImage == 0 && $totalApproveImage == 0) {
                        $planItem->status = self::STATUS_UPLOADED_WAIT_APPROVE;
                    } elseif ($totalDisapproveImage > 0) {
                        $planItem->status = self::STATUS_HAVE_DISAPPROVED;
                    } elseif ($totalImage == $totalApproveImage) {
                        if ($totalImage == $planItem->image_num) {
                            $planItem->status = self::STATUS_APPROVED;
                        } else
                            $planItem->status = self::STATUS_NOT_YET_UPLOADED;

                    } else {
                        $planItem->status = self::STATUS_UPLOADED_WAIT_APPROVE;
                    }
                }

            }

            $planItem->save();

            return $planItem;
        }
        return null;
    }

    public function rules()
    {
        return [
            [['plan_id', 'item_id'], 'required'],
            [['plan_id', 'item_id', 'status', 'image_num'], 'integer']
        ];
    }
}