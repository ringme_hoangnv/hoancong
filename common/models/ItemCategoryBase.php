<?php

namespace common\models;

use backend\models\ItemCategoryType;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class ItemCategoryBase extends \common\models\db\ItemCategoryDB
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getCategoryType()
    {
        return $this->hasOne(ItemCategoryType::className(), ['id' => 'category_type_id']);
    }
}