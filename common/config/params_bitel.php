<?php
return [
    // Ten he thong
    'system_name' => 'Video CMS',
    'short_system_name' => 'VCS',

    'default_content_lang' => 'en',
    'content_languages' => [
        //'vi' => Yii::t('backend', 'Tiếng Việt'),
        //'jp' => Yii::t('backend', 'Tiếng Nhật'),
        'en' => Yii::t('backend', 'Tiếng Anh'),
    ],

    'convert_video_api' => 'http://10.121.46.28:8189/encoder-service/v1/convert/video',
    'update_cache_api' => [
        'url' => 'http://10.121.46.28:8080/video-service/v1/cache/update-cate',
        'security' => 'c82738b558cbe563ef604cbeff75dd58',
    ],

    'country_code' => '51',
    'phonenumber_pattern' => '/^(\+51|51|0|)([0-9]{8,9})$/',
    'deeplink_pattern' => '/^([a-zA-Z])+[a-zA-Z0-9]:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}(\.[a-z]{2,4}\b)?([-a-zA-Z0-9@:%_\+.~#?&\/=]*)$/',



    'adv_channel' => [
        'all' => 'Toàn bộ',
        'web' => 'Web',
        'wap' => 'Wap',
    ],
    'adv_position' => [
        'slideshow' => 'Slideshow',
//        'head_line' => 'Head line',
//        'popup' => 'Popup',
//        'left_banner' => 'Banner bên trái',
//        'right_banner' => 'Banner bên phải',
    ],

    ### Cau hinh lien quan toi media va upload file ###
    'media_path' => '/u01/app/video_gosol/apps/video_upload',

    'media_url_cms' => 'http://10.121.46.28:8080',
    'media_url_user' => 'http://10.121.46.28:8080',
    'media_url_vcs' => 'http://10.121.46.28:8080',
    'media_url' => 'http://10.121.46.28:8080',

    'image_upload_size' => '2', // Dung luong upload file anh tinh theo MB
    'current_media_folder' => 'medias', // Ten folder se luu cac file media upload len o thu muc web
    // Ten folder luu anh dai dien cua tin tuc, neu ko cau hinh thi he thong se lay dua theo ten model tuong ung
    // Cau hinh dong theo format, $tenModel_image_folder, vi du: post_image_folder
    'advertisment_image_folder' => 'hot-image',
    'category_image_folder' => 'category',
    'channel_image_folder' => 'channel',
    'banner_image_folder' => 'banner',
    'video_image_folder' => 'video-images',

    'no_image' => '/img/no-image-square.jpg',
    'no_image_16_9' => '/img/no-image-16x9.jpg',
    'banner_locations' => [
        '0' => 'HOME_SLIDE'
    ],

    // Cau hinh google captcha
    'recaptcha_secret' => '6LdUptESAAAAAOYpLj5TbQBPzwzKyCCJj9PKY7ZQ',
    'recaptcha_site_key' => '6LdUptESAAAAAHvd3-ZoYINx2_Aem5hWAqc4weps',

    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
