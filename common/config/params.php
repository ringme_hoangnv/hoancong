<?php
Yii::setAlias('@anyname', realpath(dirname(__FILE__).'/../../'));
return [
    // Ten he thong
    'system_name' => 'Acceptance Test support',
    'short_system_name' => 'ATS',

    'sms_shortcode' => 9005,

    'default_content_lang' => 'en',
    'content_languages' => [
        'en' => Yii::t('backend', 'English'),
        'es' => Yii::t('backend', 'Spainish'),
    ],

    'rabbitmq_config' => [
        'host' => '103.143.206.63',
        'port' => 5672,
        'user' => 'admin',
        'password' => 'admin@123',
        'mt_queue_name' => 'test',
    ],

    'partner_ruc_pattern' => '/^[0-9_]+$/',
    'bts_code_pattern' => '/^[A-Za-z0-9_]+$/',
    'item_group_calculate_image_method' => [
        'base' => 'BASE - Mặc định = 1',
        'postes' => 'POSTES - Số cột bê tông',
        'ret' => 'RET-Bộ níu cột',
        'retenida' => 'Retenida',
        'panel_solar' => 'PANEL SOLAR - Số tấm pin',
        'vientos' => 'VIENTOS - Số móng co',
        'cuerpos' => 'CUERPOS',
    ],
    //  minishelter, mini 20U, không dùng nhà, C02, C04, C06, nhà xây X06
    'bts_type_of_shelter' => [
        'Minishelter' => 'Minishelter',
        '20 U' => 'mini 20U',
        'no_shelter' => 'No shelter',
        'C02' => 'C02',
        'C04' => 'C04',
        'C06' => 'C06',
        'X06' => 'nhà xây X06',
        'Minisite' => 'Minisite',
        'RRU' => 'RRU',
        'W600(RRU)' => 'W600(RRU)',
        'trx' => 'trx'

    ],
    //300x300, 400x400, 600x600, 1000x1000, 1800x1800, poste fibra, cột nhựa, cột chống cứng
    'bts_type_of_tower' => [
        '300x300' => '300x300',
        '400x400' => '400x400',
        '600x600' => '600x600',
        '1000x1000' => '1000x1000',
        '1800x1800' => '1800x1800',
        'poste_fibra' => 'poste fibra',
        'plastic_column' => 'Plastic column', // cot nhua
        'hard_column' => 'Hard column',
        'poste_concreto' => 'Poste de concreto',
        'torre_rentada' => 'Torre Rentada',
        'd410' => 'D410',
        'monoposte' => 'Monoposte',
    ],
    // BTS full, mini site, RRU Extenidad, TRX
    'bts_types' => [
        'full' => 'BTS full',
        'mini_site' => 'Mini Site',
        'rru_extenidad' => 'RRU Extenidad',
        'trx' => 'TRX',
    ],
    //  900 MHZ, 1900??? hay cho nhập, 2600
    'bts_frequency_bands' => [
        '900' => '900 Mhz',
        '1900' => '1900 Mhz',
        '2600' => '2600 Mhz'
    ],

    'update_cache_api' => [
        'url' => 'http://10.121.46.28:7289/video-service/v1/cache/update-cate',
        'security' => 'c82738b558cbe563ef604cbeff75dd58',
    ],
    'update_media_api' => [
        'url' => 'http://10.121.46.28:8189/encoder-service/v1/convert/media/approved',
        'security' => 'c82738b558cbe563ef604cbeff75dd58',
    ],

    'country_code' => '51',
    'phonenumber_pattern' => '/^(\+51|51|0|)([0-9]{9})$/',
    'phonenum_list_pattern' => '/^\s*(\+51|51|0|)([0-9]{9})(\,\s*(\+51|51|0|)([0-9]{9})(\s*))*$/',
    'deeplink_pattern' => '/^([a-zA-Z])+[a-zA-Z0-9]:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}(\.[a-z]{2,4}\b)?([-a-zA-Z0-9@:%_\+.~#?&\/=]*)$/',

    'adv_channel' => [
        'all' => 'Toàn bộ',
        'web' => 'Web',
        'wap' => 'Wap',
    ],
    'adv_position' => [
        'slideshow' => 'Slideshow',
//        'head_line' => 'Head line',
//        'popup' => 'Popup',
//        'left_banner' => 'Banner bên trái',
//        'right_banner' => 'Banner bên phải',
    ],

    ### Cau hinh lien quan toi media va upload file ###
//    'media_path' => '/home/superapp/apps/bts/backend/web/medias',
    'media_path' => '/home/hoangnguyen/Workspace/cms_ringme/cms_qlhc/backend/web/medias',
    //'ftp_upload_path' => '/home/superapp/apps/bts/backend/web/medias',
//    'media_path' => '/Volumes/data/projects/peru/bts/backend/web/medias',
//    'media_path' => Yii::getAlias('@anyname'),
    'ftp_upload_path' => '/Volumes/data/projects/peru/bts/backend/web/medias',

    'media_url' => '',
    'local_media_url' => '',

    'image_upload_size' => '10', // Dung luong upload file anh tinh theo MB
    // Ten folder luu anh dai dien cua tin tuc, neu ko cau hinh thi he thong se lay dua theo ten model tuong ung
    // Cau hinh dong theo format, $tenModel_image_folder, vi du: post_image_folder
    'advertisment_image_folder' => 'hot-image',
    'category_image_folder' => 'category',
    'channel_image_folder' => 'channel',
    'banner_image_folder' => 'banner',
    'video_image_folder' => 'video-images',

    'no_image' => '/img/no-image-square.jpg',
    'no_image_16_9' => '/img/no-image-16x9.jpg',
    'banner_locations' => [
        '0' => 'HOME_SLIDE'
    ],

    // Cau hinh google captcha
    'recaptcha_secret' => '6LdUptESAAAAAOYpLj5TbQBPzwzKyCCJj9PKY7ZQ',
    'recaptcha_site_key' => '6LdUptESAAAAAHvd3-ZoYINx2_Aem5hWAqc4weps',

    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
