<?php

return [
    'login_background_img' => [
        #'/img/bg2.jpg',
        #'/img/bg3.jpg',
        #'/img/bg4.jpg',
    ],
    'page_sizes' => [
        10 => 10,
        20 => 20,
        50 => 50,
#        100 => 100,
    ],
    'upload_folder' => 'uploads',

    'is_active' => [
        '0' => 'Chưa kích hoạt',
        '1' => 'Kích hoạt',
    ],


    'report_max_interval' => 60,

    'adminEmail' => 'admin@example.com',
];
