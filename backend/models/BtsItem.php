<?php

namespace backend\models;

use Yii;

class BtsItem extends \common\models\BtsItemBase {

    public function rules()
    {
        return [
            [['name', 'category_id', 'group_id', 'image_num'], 'required'],
            [['category_id', 'created_by', 'updated_by', 'image_num'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'image_path'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['note'], 'string', 'max' => 2000],
            ['name', 'unique'],
        ];
    }
}