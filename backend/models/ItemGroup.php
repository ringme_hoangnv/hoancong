<?php

namespace backend\models;

use Yii;

class ItemGroup extends \common\models\ItemGroupBase {

    public function rules()
    {
        return [
            [['name', 'category_id', 'calculate_image_method'], 'required'],
            [['name',], 'unique'],
            [['category_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['description'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'category_id' => Yii::t('backend', 'Category'),
            'status' => Yii::t('backend', 'Status'),
            'description' => Yii::t('backend', 'Description'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}