<?php

namespace backend\models;

use Yii;

class Area extends \common\models\AreaBase {
    /**
     * select * from area where province is not null and district is null and precinct is null;--ListProvince
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getProvinceList() {
        return self::find()
            ->where('province IS NOT NULL')
            ->andWhere('district IS NULL')
            ->andWhere('precinct IS NULL')
            ->orderBy('order_no ASC, name asc')
            ->all();
    }

    /**
     * select * from area where province = '11' and district is not null and precinct is null;--List District
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getDistrictList($province) {
        return self::getDistrictQuery($province)
            ->all();
    }
    public static function getDistrictQuery($province) {
        return self::find()
            ->where(['province' => $province])
            ->andWhere('district IS NOT NULL')
            ->andWhere('precinct IS NULL')
            ->orderBy('order_no ASC, name asc')
            ;
    }

    /**
     * select * from area where province = '01' and district = '01' and precinct is not null;--List Precinct
     * @param $province
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPrecinctList($province, $district) {
        return self::getPrecinctQuery($province, $district)
            ->all();
    }
    public static function getPrecinctQuery($province, $district) {
        return self::find()
            ->where(['province' => $province])
            ->andWhere(['district' => $district])
            ->andWhere('precinct IS NOT NULL')
            ->orderBy('order_no ASC, name asc')
            ;
    }
}