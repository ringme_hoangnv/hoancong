<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class Branch extends \common\models\BranchBase {

    public $selected_partners  = [];

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at', 'updated_at', 'selected_partners'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1000],
            [['ho_user_id'],'integer']
        ];
    }

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_partners = ArrayHelper::map($this->getPartnerIds(), 'partner_id', 'partner_id');

        }

        return parent::loadDefaultValues($skipIfSet);
    }

    public function save($runValidation = true, $attributeNames = NULL) {


        if(!$this->isNewRecord) {
            $currentPartners = $this->getPartnerIds();

            if (count($currentPartners)) {
                foreach ($currentPartners as $index => $partner) {

                    // Neu truoc do da chon --> xoa bot di
                    $exists = false;
                    if (is_array($this->selected_partners) && count($this->selected_partners)) {
                        foreach ($this->selected_partners as $selectedId) {
                            if($selectedId == $partner->partner_id) {
                                $exists = true;
                                break;
                            }
                        }

                        // neu bo chon --> xoa di
                        if($exists === false) {
                            Yii::info ( "DELETE BRANCH_ID: " . $this->id . " PARTNER_ID: " . $partner->partner_id, "log_debug" );
                            BranchPartner::deleteAll([
                                'branch_id' => $this->id,
                                'partner_id' => $partner->partner_id,
                            ]);
                        }
                    } else {
                        Yii::info ( "DELETE BRANCH_ID : " . $this->id, "log_debug" );
                        BranchPartner::deleteAll([
                            'branch_id' => $this->id,
                        ]);
                    }

                }
            }

            // them nhung partner moi
            if (is_array($this->selected_partners) && count($this->selected_partners)) {
                foreach ($this->selected_partners as $item) {
                    // tag co san
                    $exists = false;

                    foreach ($currentPartners as $partner) {
                        if($partner->partner_id == $item) {
                            $exists = true;
                            break;
                        }
                    }

                    // neu post chua co tag nay thi gan tag nay vao post
                    if($exists === false) {
                        $tag = new BranchPartner();
                        $tag->branch_id = $this->id;
                        $tag->partner_id = $item;
                        $tag->save();
                    }

                }
            }


            return parent::save($runValidation, $attributeNames);

        } else {
            $return = parent::save($runValidation, $attributeNames);
            if (is_array($this->selected_partners) && count($this->selected_partners)) {
                foreach ($this->selected_partners as $item) {
                    // tag co san
                    if ($item) {
                        $tag = new BranchPartner();
                        $tag->branch_id = $this->id;
                        $tag->partner_id = $item;
                        $tag->save();
                    }
                }
            }

            return $return;
        }
    }

    public function getPartnerIds()
    {
        return BranchPartner::find()
            ->select('partner_id')
            ->where(
                ['branch_id' => $this->id]
            )
            ->all();
    }

    public function getPartners() {
        return $this->hasMany(Partner::className(), ['id' => 'partner_id'])
            ->viaTable('branch_partner', ['branch_id' => 'id'])
            ->orderBy('name asc')
            ;
    }

    public static function getBranchArray() {
        $itemArr = self::find()
            ->select('id, name')
            ->asArray()
            ->orderBy('name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'name');

        } else {
            return array();
        }
    }
}