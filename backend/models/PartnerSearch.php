<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Partner;
use yii\helpers\ArrayHelper;

/**
 * PartnerSearch represents the model behind the search form about `backend\models\Partner`.
 */
class PartnerSearch extends Partner
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'ruc', 'director', 'director_dni', 'address', 'description', 'status', 'created_at', 'updated_at'], 'safe'],
            [['name', 'ruc', 'director', 'director_dni', 'address', 'description', 'status', 'created_at', 'updated_at'], 'trim'],
            [['name', 'ruc', 'director', 'director_dni', 'address', 'description', 'status', 'created_at', 'updated_at'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['name' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (Yii::$app->user->identity->user_type == 'branch') {
            // Chi show cac partner dc gan voi branch do
            $branchId = Yii::$app->user->identity->branch_id;
            if ($branchId) {
                $partnerIds = ArrayHelper::map(Partner::getPartnersArrByBranch($branchId), 'id', 'id');
                $query->andWhere(['id' => $partnerIds]);
            }
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ruc', $this->ruc])
            ->andFilterWhere(['like', 'director', $this->director])
            ->andFilterWhere(['like', 'director_dni', $this->director_dni])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['=', 'status', $this->status]);

        return $dataProvider;
    }
}
