<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BtsStation;

/**
 * BtsStationSearch represents the model behind the search form about `backend\models\BtsStation`.
 */
class BtsStationSearch extends BtsStation
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_id','bts_code', 'bts_name', 'status', 'direction', 'latlong', 'bts_type', 'tower_height', 'tower_type', 'shelter_type', 'created_at', 'updated_at'], 'safe'],
            [['frequency_band', 'antenna_num', 'rru_num', 'anchorage_num', 'ground_system_num'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BtsStation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (Yii::$app->user->identity->user_type == 'branch') {
            // Chi show cac ban ghi dc gan voi branch do
            $query->andWhere(['branch_id' => Yii::$app->user->identity->branch_id]);

        }

        $query->andFilterWhere([
            'frequency_band' => $this->frequency_band,
            'antenna_num' => $this->antenna_num,
            'rru_num' => $this->rru_num,
            'anchorage_num' => $this->anchorage_num,
            'ground_system_num' => $this->ground_system_num,
        ]);

        $query->andFilterWhere(['like', 'bts_code', $this->bts_code])

            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'direction', $this->direction])
            ->andFilterWhere(['like', 'latlong', $this->latlong])
            ->andFilterWhere(['like', 'bts_type', $this->bts_type])
            ->andFilterWhere(['like', 'tower_height', $this->tower_height])
            ->andFilterWhere(['like', 'tower_type', $this->tower_type])
            ->andFilterWhere(['like', 'shelter_type', $this->shelter_type]);

        return $dataProvider;
    }
}
