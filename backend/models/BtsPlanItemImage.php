<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class BtsPlanItemImage extends \common\models\BtsPlanItemImageBase {



    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['plan_id', 'item_id', 'created_by', 'updated_by', 'approved_by'], 'integer'],
            [['plan_id', 'item_id', 'image_path'], 'required'],
            [['id', 'created_at', 'updated_at', 'approved_at'], 'safe'],
            [['image_path'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            [['note'], 'string', 'max' => 1000]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'plan_id' => Yii::t('backend', 'Plan'),
            'item_id' => Yii::t('backend', 'Item'),
            'image_path' => Yii::t('backend', 'Image'),
            'status' => Yii::t('backend', 'Status'),
            'note' => Yii::t('backend', 'Note'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'approved_at' => Yii::t('backend', 'Approved At'),
            'approved_by' => Yii::t('backend', 'Approved By'),
        ];
    }
}