<?php

namespace backend\models;

use Yii;

class BtsStation extends \common\models\BtsStationBase {

    public $lat;
    public $long;

    public function rules()
    {
        return [
            [['bts_code', 'direction', 'branch_id', 'bts_type', 'anchorage_num'], 'required'],
            [['frequency_band', 'antenna_num', 'rru_num', 'anchorage_num', 'ground_system_num', 'tower_height'], 'integer', 'min' => 0  ],
            [['created_at', 'updated_at', 'bts_name'], 'safe'],
            [['bts_code'], 'string', 'max' => 200],
            [['bts_name', 'direction'], 'string', 'max' => 255],
            [['status', 'branch_id'], 'integer'],
            [['latlong'], 'string',],
            [['bts_type', 'tower_type', 'shelter_type'], 'string', 'max' => 50],
            //[['tower_height'], 'string', 'max' => 10],
            [['bts_code'], 'unique'],
            //[['bts_name'], 'unique'],
            [['bts_code'], 'match', 'pattern' => Yii::$app->params['bts_code_pattern'],
                //'message' => Yii::t('backend', 'Bts code only allows character')
            ],
            ['anchorage_num', 'integer', 'min' => 1],

            [['province_id', 'district_id', 'precinct_id', 'address_text'], 'safe'],
            [['province_id', 'district_id', 'precinct_id', 'address_text'], 'string'],

            [['lat', 'long',], 'number'],
            ['direction','required','when' => function($model) {
                return $model->direction = null;
            }]

        ];
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues();

        if ($this->latlong && strpos($this->latlong, ',') !== false) {
            $latLongArr = explode(',', $this->latlong);
            if (!empty($latLongArr)) {
                $this->lat = $latLongArr[0];
                $this->long = $latLongArr[1];
            }
        }

        return $this;
    }

    public function beforeSave($insert)
    {
        $this->bts_code = strtoupper($this->bts_code);
        if ($this->lat && $this->long) {
            $this->latlong = $this->lat. ','. $this->long;
        }
        return parent::beforeSave($insert);
    }

}