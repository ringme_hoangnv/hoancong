$(document).ready(function(){
    function showPartnerBranchHo(val) {
        if(val == 'branch') {
            $('#user-branch').show();
            $('#user-partner').hide();
            $('.field-user-ho_type').hide();
            $('.field-user-branch_control').hide();
        } else if (val == 'partner') {
            $('#user-partner').show();
            $('#user-branch').hide();
            $('.field-user-ho_type').hide();
            $('.field-user-branch_control').hide();
        } else if (val == 'ho'){
            $('.field-user-ho_type').show();
            $('#user-partner').hide();
            $('#user-branch').hide();
            $('.field-user-branch_control').hide();
        }else {
            $('#user-branch').hide();
            $('#user-partner').hide();
            $('.field-user-ho_type').hide();
            $('.field-user-branch_control').hide();
        }
    }
    function showBranchControl(val) {
        if(val == 'Nhân viên quản lí vùng' ) {
            $('.field-user-branch_control').show();
        }
        else{
            $('.field-user-branch_control').hide();
        }
    }
    var radios = $('input:radio[name="User[user_type]"]');
    if(radios.is(':checked') === false) {
        $('#user-branch').hide();
        $('#user-partner').hide();
        $('.field-user-ho_type').hide();
        $('.field-user-branch_control').hide();
    } else {
        showPartnerBranchHo($('input:radio[name="User[user_type]"]:checked').val());
    }
    radios.change(function(){
        showPartnerBranchHo($(this).val());
        $('#user-ho_type').val('');
    });
    $('#user-ho_type').change(function() {
        showBranchControl($(this).val());
    });
    showBranchControl($('#user-ho_type').val());
    // $(window).load(function () {
    //     showBranchControl($('#user-ho_type').val());
    // })
});