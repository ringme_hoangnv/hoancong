<?php

namespace backend\controllers;

use backend\models\BranchPartner;
use backend\models\BtsItem;
use Yii;
use backend\models\Partner;
use backend\models\PartnerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;

/**
 * PartnerController implements the CRUD actions for Partner model.
 */
class PartnerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new Partner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partner();
        $model->status = 1;
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate

        if ($model->load($form_values) && $model->save()) {

            if (Yii::$app->user->identity->user_type == 'branch') {
                $branchPartner = new BranchPartner();
                $branchPartner->branch_id = Yii::$app->user->identity->branch_id;
                $branchPartner->partner_id = $model->id;
                $branchPartner->created_at = date('Y-m-d H:i:s');
                $branchPartner->save();
            }

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Partner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        if ($model->load($form_values) && $model->save()) {

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Edit "{object}" successfully!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['index']);
//                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Partner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->delete();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        // Neu user la brach thi chi cho update partner cua ho
        if (Yii::$app->user->identity->user_type == 'branch') {
            $check = BranchPartner::find()
                ->where([
                    'branch_id' => Yii::$app->user->identity->branch_id,
                    'partner_id' => $id
                ])
                ->one();
            if (!$check) {
                throw new NotFoundHttpException('The requested page does not exist.');
                //Yii::$app->session->setFlash('info', Yii::t('backend', 'You cannot update this partner info!'));
                //return $this->redirect(['index']);
            }
        }

        if (($model = Partner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetPartnersByBranch()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $params = Yii::$app->request->post('depdrop_all_params');
        $data = [];
        if (isset($params['branch-id']) && $params['branch-id']) {

            $data = Partner::getPartnersArrByBranch($params['branch-id']);

            $data = array_values($data);
            //$data = Helpers::array_change_key_case_recursive($data, CASE_LOWER);

        } else {
            // get all partner
            $data = Partner::find()
                ->select('id, name')
                ->orderBy('name ASC')
                ->asArray()
                ->all();

            $data = array_values($data);
        }


        return ['output'=> $data, 'selected'=> Yii::$app->request->get('selected')];
    }
}
