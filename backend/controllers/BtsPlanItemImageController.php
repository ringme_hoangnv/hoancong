<?php

namespace backend\controllers;

use backend\models\BtsItem;
use backend\models\BtsPlanItem;
use backend\models\BtsPlanItemImage;
use backend\models\ItemCategory;
use backend\models\User;
use common\core\SuperAppApiGw;
use common\models\BtsPlanItemBase;
use common\models\BtsPlanItemImageBase;
use common\models\db\CronMsg;
use common\models\SystemSettingBase;
use Yii;
use backend\models\BtsPlan;
use backend\models\BtsPlanSearch;
use yii\base\BaseObject;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\validators\ImageValidator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use yii\web\UploadedFile;


class BtsPlanItemImageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionApprove()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

//        if (!Yii::$app->user->can('admin') || !Yii::$app->user->can('ho')) {
//            $response = [
//                'error_code' => 403,
//                'message' => Yii::t('backend', 'Please contact admin to do this function')
//            ];
//            return $response;
//        }

        $response = [];

        $id = Yii::$app->request->post('id');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            $model->status = BtsPlanItemImage::STATUS_APPROVED;
            $model->approved_by = Yii::$app->user->identity->getId();
            $model->approved_at = date('Y-m-d H:i:s');
            $model->note = null;
            $model->save(false);

            // update item status
            BtsPlanItemBase::updateStatus($model->plan_id, $model->item_id);

            $response['error_code'] = 0;
            $response['message'] = Yii::t('backend', 'Approve image successfully!');
        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }
        $this->actionCheckItem($model);
        return $response;
    }

    public function actionCheckItem($model)
    {
        $plan_id = $model->plan_id;
        $item_id = $model->item_id;
        $soluonganhdaxem = BtsPlanItemImage::find()
            ->andWhere([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
            ])
            ->andWhere(['<>', 'status', '0'])->count();
        $item = BtsPlanItem::findOne([
            'plan_id' => $plan_id,
            'item_id' => $item_id
        ]);
        $item1 = BtsItem::findOne($item_id);
        $plan = BtsPlan::findOne($plan_id);
        $soluonganhyeucau = $item->image_num;
        $category = ItemCategory::findOne($plan->item_category_id);
        if ($soluonganhyeucau == (int)$soluonganhdaxem) {

            //LORXXXX  - CATEGORIAXXX - FOTO PARARRAYOS Y TAPA TORRE  CARGA FOTO {datetime}  {soluonganhpheduyet}/{tongsoanh} APROBADO  {tongsoanhtuchoi}/{tongsoanh} OBSERVACIÓN FECHA {datetimepheduyet}
            $msg = "{bts_code}- {category}- {item}  CARGA FOTO {datetime}  {soluonganhpheduyet}/{tongsoanh} APROBADO  {tongsoanhtuchoi}/{tongsoanh} OBSERVACIÓN FECHA {datetimepheduyet}";
            $soluonganhpheduyet = BtsPlanItemImage::find()->where([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
                'status' => 1
            ])->count();
            $soluonganhtuchoi = BtsPlanItemImage::find()->where([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
                'status' => 2
            ])->count();
            $msg = \Yii::t('backend', $msg, [
                'datetime' => date("Y-m-d H:i:s"),
                'soluonganhpheduyet' => $soluonganhpheduyet,
                'tongsoanh' => $soluonganhyeucau,
                'tongsoanhtuchoi' => $soluonganhtuchoi,
                'datetimepheduyet' => date("Y-m-d H:i:s"),
                'bts_code' => ($plan) ? $plan->bts_code : '',
                'category' => $category->name,
                'item' => $item1->name,
            ]);
            $partner = User::findOne(['id' => $model->created_by]);
            if ($partner->msisdn) {
                SuperAppApiGw::sendMt(\Yii::$app->params['sms_shortcode'], $partner->msisdn, $msg);
            }
        }
    }

    public function actionDisapprove()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

//        if (!Yii::$app->user->can('admin') && !Yii::$app->user->can('ho')) {
//            $response = [
//                'error_code' => 403,
//                'message' => Yii::t('backend', 'Please contact admin to do this function')
//            ];
//            return $response;
//        }

        $response = [];

        $id = Yii::$app->request->post('id');
        $reason = Yii::$app->request->post('reason');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            if ($reason) {
                $model->note = $reason;
                $model->status = BtsPlanItemImage::STATUS_DISAPPROVED;
                $model->approved_at = date('Y-m-d H:i:s');
                $model->approved_by = Yii::$app->user->identity->getId();
                $model->save(false);

                // update item status
                BtsPlanItemBase::updateStatus($model->plan_id, $model->item_id);

                // Gui tin nhan cho partner
//                $user = Yii::$app->user->identity;
//                if ($user->user_type == 'ho') {
//                    $plan = $model->plan;
//                    $partner = $plan->partner;
//                    $partnerNumbers = explode(',', $partner->msisdn_warning);
//
//                    if (count($partnerNumbers)) {
//                        $msg = SystemSettingBase::getConfigByKey('MT_HO_DISAPPROVE_IMAGE');
//                        //HO {ho_user} disapproved an image in plan {plan_name} with note: {note}
//                        $msg = Yii::t('backend', $msg, [
//                            'ho_user' => $user->username,
//                            'plan_name' => $plan->name,
//                            'note' => $reason,
//                        ]);
//                        SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $partnerNumbers, $msg);
//                    }
//                }

                $response['error_code'] = 0;
                $response['message'] = Yii::t('backend', 'Disapprove image successfully!');
            } else {
                // Bat buoc nhap
                $response['error_code'] = 400;
                $response['message'] = Yii::t('backend', 'Reason is required!');
            }

        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }
        $this->actionCheckItem($model);

        return $response;
    }

    public function actionDelete()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [
            'error_code' => 0,
            'message' => Yii::t('backend', 'Delete image successfully!')
        ];

        $id = Yii::$app->request->post('id');
        $model = BtsPlanItemImage::findOne($id);

        if ($model) {
            if ($model->status == BtsPlanItemImage::STATUS_APPROVED) {
                // Ko cho xoa anh da duoc duyet
                $response['error_code'] = 403;
                $response['message'] = Yii::t('backend', 'You cannot delete a approved image!');
                return $response;
            }

            $planId = $model->plan_id;
            $itemId = $model->item_id;

            $filePath = str_replace('/' . basename(Yii::$app->params['media_path']), '', Yii::$app->params['media_path']) . $model->image_path;
            $model->delete();

            if (file_exists($filePath)) {
                unlink($filePath);
            }

            // update item status
            BtsPlanItemBase::updateStatus($planId, $itemId);

            $response['error_code'] = 0;
            $response['message'] = Yii::t('backend', 'Delete image successfully!');
        } else {
            $response['error_code'] = 404;
            $response['message'] = Yii::t('backend', 'Image not found!');
        }

        return $response;
    }

    /**
     * Show thong tin cua image
     * @param $id
     * @return string
     */
    public function actionInfo($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('/bts-plan/itemImageInfo', [
            'model' => BtsPlanItemImage::findOne($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Xu ly upload anh item
     * @return array
     * @throws \yii\base\Exception
     */

    public function actionUpload()
    {
        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $maxFileSize = Yii::$app->params['image_upload_size'] * 1024; //Kb
        if (Yii::$app->request->isPost) {
            $planId = Yii::$app->request->post('plan_id');
            $itemId = Yii::$app->request->post('item_id');
            $fileId = Yii::$app->request->post('fileId');

            $plan = BtsPlan::findOne($planId);
            $item = BtsItem::findOne($itemId);
            if (!$plan || !$item) {
                $res = ['error' => Yii::t('backend', 'Plan or item not found!')];
                return $res;
            }

            // check so luong image
            $planItem = BtsPlanItemBase::findOne([
                'plan_id' => $planId,
                'item_id' => $itemId,
            ]);
            $countImages = BtsPlanItemImageBase::find()
                ->where([
                    'plan_id' => $planId,
                    'item_id' => $itemId,
                ])
                ->count();
            if ($countImages >= $planItem->image_num) {
                $res = ['error' => Yii::t('backend', 'You cannot upload more image!', [
                    'total' => $planItem->image_num
                ])];
                return $res;
            }

            $res = [];
            $initialPreview = [];
            $initialPreviewConfig = [];
            $images = UploadedFile::getInstancesByName("image_upload");


            // upload image to db
            if (count($images) > 0) {
                foreach ($images as $key => $image) {
                    $validator = new ImageValidator();
                    if (!$validator->validate($image)) {
                        $res = ['error' => Yii::t('backend', 'Invalid image!')];
                        return ($res);
                    }

                    if ($image->size > $maxFileSize * 1024) {
                        $res = ['error' => Yii::t('backend', 'The maximum size of the image cannot exceed 2M')];
                        return ($res);
                    }
                    if (!in_array(strtolower($image->extension), array('gif', 'jpg', 'jpeg', 'png'))) {
                        $res = ['error' => Yii::t('backend', 'Please upload a standard image file, support gif, jpg, png and jpeg.')];
                        return ($res);
                    }
                    $dir = 'medias/plan/' . date('ymd', strtotime($plan->created_at)) . '/' . $planId . '/' . $itemId . '/';

                    $fileNameClean = preg_replace('/[^A-Za-z0-9\-]/', '', $image->baseName); // Removes special chars.
                    $fileNameClean = Inflector::camel2id($fileNameClean) . '-' . time();
                    $filename = $fileNameClean . '.' . $image->getExtension();

                    $savePath = $dir;
                    // If the folder does not exist, create a new folder
                    if (!file_exists($savePath)) {
                        FileHelper::createDirectory($savePath);
                    }

                    $file = $savePath . $filename;


                    if ($image->saveAs($file)) {
                        // resize image neu qua to
                        if ($image->size > 1.5 * 1024 * 1024) {
                            Image::thumbnail($file, 1200, null)->save($file);
                        }

                        $imgpath = '/' . $dir . $filename;
                        // Luu anh
                        $imgObj = new BtsPlanItemImage();
                        $imgObj->plan_id = $planId;
                        $imgObj->item_id = $itemId;
                        $imgObj->image_path = $imgpath;
                        $imgObj->status = BtsPlanItemImage::STATUS_DRAFT;
                        $imgObj->save(false);
                        // update item status
                        BtsPlanItemBase::updateStatus($planId, $itemId);

                        $config = [
                            'caption' => $filename,
                            'width' => '120px',
                            'url' => Url::to(['bts-plan-item-image/delete', 'id' => $imgObj->id]), // server delete action
                            'key' => $imgObj->id,
                            'extra' => ['filename' => $filename]
                        ];
                        array_push($initialPreviewConfig, $config);

                        $res = [
                            "initialPreview" => $initialPreview,
                            "initialPreviewConfig" => $initialPreviewConfig,
                            "imgfile" => "<input name='image[]' id='" . $imgObj->id . "' type='hidden' value='" . $imgpath . "'/>",
                            'filename' => $filename,
                            'imagePath' => $imgObj->getImagePathUrl(),
                            'image_id' => $imgObj->id,
                            'file_id' => $fileId,
                        ];
                    }
                }
            }
            $this->actionCheckSlAnh($planId, $itemId, $plan, $item);
            return $res;
        }
    }


    public function actionCheckSlAnh($planId, $itemId, $plan, $item)
    {
        // check so luong image
        $planItem = BtsPlanItemBase::findOne([ //so luong anh can
            'plan_id' => $planId,
            'item_id' => $itemId,
        ]);
        $soluonganhyeucau = $planItem->image_num;

        $soluonganhdadang = BtsPlanItemImageBase::find() //so luong anh da dang
        ->where([
            'plan_id' => $planId,
            'item_id' => $itemId,
            'status' => 0
        ])->count();

        //check xem so luong anh dang len da du chua de gui tin
        $soluonganhtuchoi  = BtsPlanItemImageBase::find() //so luong anh da dang
        ->where([
            'plan_id' => $planId,
            'item_id' => $itemId,
            'status' => 2
        ])->count();
        $partnerUser = Yii::$app->user->identity;
        if ( $soluonganhdadang == $soluonganhyeucau ||  $soluonganhdadang = $soluonganhtuchoi) {
            $category = ItemCategory::findOne($plan->item_category_id);
            $hoNumbers = explode(',', SystemSettingBase::getNumberQlVung($planId));
            if (count($hoNumbers)) {
                // Gui tin nhan cho HO
                $msg = SystemSettingBase::getConfigByKey('SENT_MSG_LOCAL_MANAGEMENT');
                // {bts_code}- {category}- {item} CARGA FOTO {datetime} ({soluonganhupload}/{soluonganhyeucau})
                $msg = Yii::t('backend', $msg, [
                    'bts_code' => ($plan) ? $plan->bts_code : '',
                    'category' => $category->name,
                    'item' => $item->name,
                    'datetime' => date('Y-m-d H:i:s'),
                    'soluonganhupload' => $soluonganhdadang,
                    'soluonganhyeucau' => ($soluonganhyeucau = $soluonganhdadang) ? $soluonganhyeucau : $soluonganhtuchoi,
                ]);
                //check thoi gian xem co dang trong gio lam viec khong
                $time_now = date('Y-m-d H:i:s');
                if (CronMsg::instance()->checkTimeWork($time_now) == 2) {
                    SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $hoNumbers[0], $msg);
                    //tao crontab de gui nhac nho
                    $time_sent = date('Y-m-d H:i:s', strtotime('+3 hours', strtotime($time_now)));
                    if(CronMsg::instance()->checkTimeWork($time_sent) == 3) {
                        $time_sent = date('Y-m-d H:i:s', strtotime("+1 days", strtotime('09:00:00')));
                    }
                    $cron_msg = new CronMsg();
                    $cron_msg->plan_id = $planId;
                    $cron_msg->item_id = $itemId;
                    $cron_msg->sent_to = 'DEPUTY';
                    $cron_msg->sent_at = $time_sent;
                    $cron_msg->msg_local_manager = $msg;
                    $cron_msg->save(false);
                }
                elseif (CronMsg::instance()->checkTimeWork($time_now) == 3) {
                    //tao crontab de gui nhac nho
                    $time_sent = date('Y-m-d H:i:s', strtotime("+1 days", strtotime('09:00:00')));
                    $cron_msg = new CronMsg();
                    $cron_msg->plan_id = $planId;
                    $cron_msg->item_id = $itemId;
                    $cron_msg->sent_to = 'MANAGER_LOCAL';
                    $cron_msg->sent_at = $time_sent;
                    $cron_msg->msg_local_manager = $msg;
                    $cron_msg->number_manager_local = $hoNumbers[0];
                    $cron_msg->save(false);
                } else {
                    //tao crontab de gui nhac nho
                    $cron_msg = new CronMsg();
                    $cron_msg->plan_id = $planId;
                    $cron_msg->item_id = $itemId;
                    $cron_msg->sent_to = 'MANAGER_LOCAL';
                    $cron_msg->sent_at = date('Y-m-d H:i:s', strtotime('09:00:00'));
                    $cron_msg->msg_local_manager = $msg;
                    $cron_msg->number_manager_local = $hoNumbers[0];
                    $cron_msg->save(false);
                }
            }
        }
    }
}