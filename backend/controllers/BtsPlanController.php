<?php

namespace backend\controllers;

use backend\models\BtsItem;
use backend\models\BtsPlanItem;
use backend\models\BtsPlanItemImage;
use backend\models\BtsStation;
use kartik\mpdf\Pdf;
use Yii;
use backend\models\BtsPlan;
use backend\models\BtsPlanSearch;
use yii\base\BaseObject;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use yii\web\UploadedFile;

/**
 * BtsPlanController implements the CRUD actions for BtsPlan model.
 */
class BtsPlanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BtsPlan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BtsPlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BtsPlan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new BtsPlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BtsPlan();
        $model->setScenario(BtsPlan::SCENARIO_CREATE);
        $model->is_exported = 0;
        $model->status = 1;
        $model->postes_num = 1;
        $model->ret_num = 1;
        $model->retenida_num = 1;
        $model->panel_solar_num = 1;
        $model->vientos_num = 1;
        $model->cuerpos_num = 1;
        $model->pozos_num = 1;
        $model->start_at = date('Y-m-d H:i:s');

        $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
//        $imageFieldName = 'image_path';
//        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
        if ($model->load($form_values)) {
            $model->file_path = UploadedFile::getInstance($model, 'file_path'); //upload file
            $model->upload();
//            $model->file_path &&
            if ($model->save(false)) {
                // Lay ra cac item cua category
                $cateId = $model->item_category_id;
                $itemList = BtsItem::find()
                    ->where(['category_id' => $cateId])
                    ->all();
                foreach ($itemList as $item) {
                    // Tinh toan so luong anh
                    $imageNum = $item->getImageUploadNumberByPlan($model);
                    $planItem = new BtsPlanItem();
                    $planItem->plan_id = $model->id;
                    $planItem->item_id = $item->id;
                    $planItem->image_num = $imageNum;
                    $planItem->save();


                }
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

                // bo sung redirect
                if (Yii::$app->request->post('save_and_back')) {
                    return $this->redirect(['index']);
                } elseif (Yii::$app->request->post('save_and_add')) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
//            Yii::$app->session->setFlash('info', Yii::t('backend', 'Input File can not be blank'));
//            return $this->render('create', [
//                'model' => $model,
//            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing BtsPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));
        $file_path = $model->file_path;
        $model->loadDefaultValues();
//        $form_values = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post())) {
            $model->file_path = UploadedFile::getInstance($model, 'file_path'); //upload file
            $model->upload();
            if(!$model->file_path) {
                $model->file_path = $file_path;
            }
            if ($model->save(false)) {
                // Lay ra cac item cua category
                $cateId = $model->item_category_id;
                // Xoa het item cu
                BtsPlanItem::deleteAll([
                    'plan_id' => $model->id
                ]);

                $itemList = BtsItem::find()
                    ->where(['category_id' => $cateId])
                    ->all();
                foreach ($itemList as $item) {
                    // Tinh toan so luong anh
                    $imageNum = $item->getImageUploadNumberByPlan($model);
                    $planItem = new BtsPlanItem();
                    $planItem->plan_id = $model->id;
                    $planItem->item_id = $item->id;
                    $planItem->image_num = $imageNum;
                    $planItem->save();

                }

                Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

                // bo sung redirect
                if (Yii::$app->request->post('save_and_back')) {
                    return $this->redirect(['index']);
                } elseif (Yii::$app->request->post('save_and_add')) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['index']);
                }
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BtsPlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if (Yii::$app->user->can('super-admin') || Yii::$app->user->can('admin')) {
            $model = $this->findModel($id);
            $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));
            Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
            $model->delete();
            Yii::$app->db->createCommand('set foreign_key_checks=1')->execute();
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('backend', 'Please contact admin to do this function'));
            return $this->redirect(['index']);
        }


        /*
        * ko cho xoa plan khoi db
        // Xoa image vat ly
        FileHelper::removeDirectory(Yii::getAlias('@backend_web'). '/medias/plan/'.date('ymd', strtotime($model->created_at)).'/'. $model->id);
//        $images = BtsPlanItemImage::findAll(['plan_id' => $id]);
//        if (count($images)) {
//            foreach ($images as $img) {
//                $fullImgPath = Yii::getAlias('@backend_web'). $img->image_path;
//
//                if (file_exists($fullImgPath)) {
//                    unlink($fullImgPath);
//                }
//            }
//        }

        // Xoa cac item/image lien quan
        BtsPlanItemImage::deleteAll(['plan_id' => $id]);
        BtsPlanItem::deleteAll(['plan_id' => $id]);

        $model->delete();
        */

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the BtsPlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BtsPlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = BtsPlan::findOne($id)) !== null) {
            // Neu user la branch thi chi cho update ban ghi cua user tao ra
            if (Yii::$app->user->identity->user_type == 'branch') {
                if (Yii::$app->user->getId() != $model->created_by) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            }

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionUpdateItemImages($id)
    {
        $model = $this->findModel($id);
        $items = $model->items;

        return $this->render('items', [
            'plan' => $model,
            'items' => $items
        ]);

    }

    public function actionExport($id)
    {
        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        $this->layout = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

        $plan = $this->findModel($id);

        // Neu la partner --> chi cho export khi da duoc duyet het
        if (Yii::$app->user->identity->user_type == 'partner') {
            // neu da export thi ko cho export nua
            if ($plan->is_exported) {

                Yii::$app->session->setFlash('warning', Yii::t('backend', 'You have already exported!'));
                return $this->redirect(['bts-plan/update-item-images', 'id' => $id]);
            }

            // neu chua duyet het cac  item
            $checkApprove = BtsPlanItem::find()
                ->where(['!=', 'status', BtsPlanItem::STATUS_APPROVED])
                ->andWhere([
                    'plan_id' => $plan->id,
                ])
                ->count();

            if ($checkApprove > 0) {
                Yii::$app->session->setFlash('warning', Yii::t('backend', 'Your items have not approved yet!'));
                return $this->redirect(['bts-plan/update-item-images', 'id' => $id]);
            }

        }


        $images = BtsPlanItemImage::find()
            ->alias('a')
            ->joinWith('item i')
            ->where([
                'a.plan_id' => $id
            ])
            ->orderBy('i.name asc, a.updated_at desc')
            ->all();

        // get your HTML raw content without any layouts or scripts
        if ($images == null) {
            Yii::$app->session->setFlash('info', Yii::t('backend', 'Please upload photos to perform this function!!!'));
            return $this->redirect(['bts-plan/update-item-images', 'id' => $id]);
        }
        $content = $this->renderPartial('_export', [
            'plan' => $plan,
            'images' => $images
        ]);
        //echo $content;die;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            //'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssFile' => '@bower/bootstrap/dist/css/bootstrap.css',
            // any css to be embedded if required
            'cssInline' => 'table tr td{border:1px solid #000;padding: 3px;}',
            // set mPDF properties on the fly
            'options' => ['title' => $plan->name . ' report'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => [$plan->name . ' report'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        if (Yii::$app->user->identity->user_type == 'partner') {
            $plan->is_exported = 1;
            $plan->save(false);
        }

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionGetbts()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $bts_code = explode(":", $data['bts_code']);
            $bts_code = $bts_code[0];
            $modelBts = BtsStation::find()->where(['bts_code' => $bts_code])->one();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'tower_type' => $modelBts->tower_type,
                'shelter_type' => $modelBts->shelter_type,
                'anchorage_num' => $modelBts->anchorage_num,
                'antenna_num' => $modelBts->antenna_num,
                'rru_num' => $modelBts->rru_num,
                'tower_height' => $modelBts->tower_height,
                'ground_system_num' => $modelBts->ground_system_num
            ];


        }
    }
}
