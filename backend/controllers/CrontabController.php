<?php

namespace backend\controllers;

use backend\models\BtsPlanItemImage;
use backend\models\User;
use common\core\SuperAppApiGw;
use common\models\BtsPlanItemBase;
use common\models\db\CronMsg;
use common\models\SystemSettingBase;
use yii\console\Controller;


class CrontabController extends Controller
{
    // gui tin nhan nhac nho
    public function actionSentAgain()
    {
        $timecurrent = date("Y-m-d H:i:s");
        $time_5 = date('Y-m-d H:i:s',strtotime('-5 minutes', strtotime($timecurrent))); // thoi gian now -5
        // lay danh sach cac crontab can gui
        $listCronTab = CronMsg::find()->where(['between', 'sent_at', $time_5, $timecurrent])->all();
        // gui tung tin
        foreach ($listCronTab as $cronTab) {
            $planId = $cronTab->plan_id;
            $itemId = $cronTab->item_id;
            $soluonganhchuaduocpheduyet = BtsPlanItemImage::find()->where([
                'plan_id' => $planId,
                'item_id' => $itemId,
                'status' => 0
            ])
                ->count();
            $planItem = BtsPlanItemBase::findOne([ //so luong anh yeu cau
                'plan_id' => $planId,
                'item_id' => $itemId,
            ]);
//            $soluonganhyeucau = $planItem->image_num;
            $timeold = $cronTab->sent_at;
            //check xem da phe duyet chua
            if ($soluonganhchuaduocpheduyet != 0) {
                //gui den manager local
                if ($cronTab->sent_to == 'MANAGER_LOCAL') {
                    SuperAppApiGw::sendMt(\Yii::$app->params['sms_shortcode'], (string)$cronTab->number_manager_local, $cronTab->msg_local_manager);
                    $time_sent = date('Y-m-d H:i:s', strtotime('+3 hours', strtotime($timeold)));
                    if(CronMsg::instance()->checkTimeWork($time_sent) == 3) {
                        $time_sent = date('Y-m-d H:i:s', strtotime("+1 days", strtotime('09:00:00')));
                    }
                    $cronTab->sent_to = 'DEPUTY';
                    $cronTab->sent_at = $time_sent;
                    $cronTab->save(false);
                } // gui tin den pho phong
                elseif ($cronTab->sent_to == "DEPUTY") {
                    $msg = SystemSettingBase::getConfigByKey('SENT_MSG_DEPUTY');
                    // {bts_code}-{category}- {item} CARGA FOTO {datetime} (soluonganhupload/soluonganhyeucau) 3 HORAS NO REVISION {soluonganhchuaduocpheduyet}/{soluonganhyeucau}
                    $msg = \Yii::t('backend', $msg, [
                        'soluonganhchuaduocpheduyet' => $soluonganhchuaduocpheduyet,
                        'soluonganhyeucau' => $planItem->image_num,
                    ]);
                    $msg = $cronTab->msg_local_manager . $msg;
                    $deputys = User::findAll([
                        'ho_type' => 'Phó phòng'
                    ]);
                    // gui den tat ca cac pho phong
                    foreach ($deputys as $deputy) {
                        SuperAppApiGw::sendMt(\Yii::$app->params['sms_shortcode'], $deputy->msisdn, $msg);
                    }
                    // cap nhat lai thoi gian de gui den truong phong

                    $cronTab->sent_to = 'MANAGER';
                    $cronTab->sent_at = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($timeold)));
                    $cronTab->save(false);
                } // gui den truong phong
                else {
                    $msg = SystemSettingBase::getConfigByKey('SENT_MSG_MANAGER');
                    // {bts_code}-{category}- {item} CARGA FOTO {datetime} (soluonganhupload/soluonganhyeucau) 24 HORAS NO REVISION {soluonganhchuaduocpheduyet}/{soluonganhyeucau}
                    $msg = \Yii::t('backend', $msg, [
                        'soluonganhchuaduocpheduyet' => $soluonganhchuaduocpheduyet,
                        'soluonganhyeucau' => $planItem->image_num,
                    ]);
                    $msg = $cronTab->msg_local_manager . $msg;
                    $managers = User::findAll([
                        'ho_type' => 'Trưởng phòng'
                    ]);
                    foreach ($managers as $manager) {
                        SuperAppApiGw::sendMt(\Yii::$app->params['sms_shortcode'], $manager->msisdn, $msg);
                    }
                    $cronTab->sent_at = date("Y-m-d H:i:s", strtotime('+24 hours', strtotime($timeold)));
                    $cronTab->save(false);
                }
            }
        }
    }

}
?>

