<?php

use yii\bootstrap\Html;
use awesome\backend\form\AwsActiveForm;
use \yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use \backend\models\Branch;
use \backend\models\Partner;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered user-form">

        <div class="portlet-body">
            <div class="form-body row">
                <div class="col-md-6">
                    <div class="col-md-10">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-10">
                        <?= $form->field($model, 'new_password')->passwordInput() ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-10">
                        <?php
                        echo $form->field($model, 'fullname')->textInput(['maxlength' => 255]);
                        echo $form->field($model, 'msisdn')->textInput(['maxlength' => 15]);
                        ?>
                        <br/>
                        <?php echo $form->field($model, 'status', ['options' => ['class' => 'status-checkbox']])->checkbox([
                            'label' => Yii::t('backend', 'Active/Inactive')
                        ]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-10">
                        <?= $form->field($model, 'user_type')->radioList(\backend\models\User::getUserTypeArr(), [
                            'id' => 'user-type'
                        ])->label("User Type", array('class' => 'label-usertype')) ?>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="col-md-10">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-10">
                        <?= $form->field($model, 're_password')->passwordInput() ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-10">
                        <?= $form->field($model, 'address')->textarea(['maxlength' => 255, 'rows' => 4]); ?>
                    </div>

                    <div class="clearfix"></div>

                    <div class="index-show">
                        <div class="col-md-10" id="user-branch">
                            <?php
                            if (Yii::$app->user->identity->user_type == 'branch') {
                                $branchList = Branch::find()->where(['id' => Yii::$app->user->identity->branch_id])->all();
                            } else {
                                $branchList = Branch::find()->all();
                            }
                            ?>
                            <?= $form->field($model, 'branch_id')->widget(Select2::classname(), [
                                'data' => \yii\helpers\ArrayHelper::map($branchList, 'id', 'name'),
                                'size' => Select2::MEDIUM,
                                'options' => [
                                    'id' => 'branch-id',
                                    'placeholder' => Yii::t('backend', 'Choose a branch')
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend', 'Branch')); ?>
                        </div>
                        <div class="col-md-10" id="user-partner">
                            <?php
                            if (($model->branch_id)) {
                                $partners = Partner::getPartnersByBranch($model->branch_id);
                            } else if (Yii::$app->user->identity->user_type == 'branch') {
                                $partners = Partner::getPartnersByBranch(Yii::$app->user->identity->branch_id);
                            } else {
                                $partners = Partner::find()->all();
                            }
                            ?>
                            <?= $form->field($model, 'partner_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($partners, 'id', 'name'),
                                'size' => Select2::MEDIUM,
                                'options' => [
                                    'id' => 'partner-id',
                                    'placeholder' => Yii::t('backend', 'Choose a partner'),
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend', 'Partner')); ?>
                        </div>

                        <div class="col-md-10">
                            <?= $form->field($model, 'ho_type')->dropDownList([
                                'Nhân viên quản lí vùng' => ' Area Management Staff',
                                'Trưởng phòng' => 'Manager',
                                'Phó phòng' => 'Deputy',
                            ],
                                [
                                    'prompt' => Yii::t('backend', '-Select-')
                                ]) ?>
                            <div id="ho-branch">
                                <?php $branchList = Branch::find()->where(['ho_user_id' => [null, $model->id]])->all() ?>
                                <?= $form->field($model, 'branch_control')->widget(Select2::classname(), [
                                    'data' => \yii\helpers\ArrayHelper::map($branchList, 'id', 'name'),
                                    'size' => Select2::MEDIUM,
                                    'options' => [
                                        'placeholder' => Yii::t('backend', 'Choose...')
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'multiple' => true,
                                    ],
                                    'addon' => [
                                        'prepend' => [
                                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                                        ]
                                    ],
                                ])->label(Yii::t('backend', 'Branch Control')); ?>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="clearfix"></div>

            </div>
        </div>

        <div class="portlet-title">
            <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
                <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
            &nbsp;&nbsp;&nbsp;
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>

        </div>
    </div>

<?php ActiveForm::end(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<?php
$this->registerJsFile("/js/user.js");
$this->registerCssFile("/css/user.css");
?>

