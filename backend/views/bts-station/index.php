<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BtsStationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'BTS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-station-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>

                <div class="">
                    <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => Yii::t('backend', 'Bts')
                    ]),
                        ['create'], ['class' => 'btn btn-info btn-sm']) ?>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],
                            'bts_type',

                            [
                                'attribute' => 'bts_code',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a(Html::encode($model->bts_code), ['bts-station/update', 'id' => $model->bts_code,]);
                                },
                            ],

                            [
                                'attribute' => 'branch_id',
                                'value' => function ($object) {

                                    return ($object->branch) ? $object->branch->name : null;
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],

                            //'direction',
                            //'latlong',

                            'frequency_band',
                            // 'antenna_num',
                            // 'rru_num',
                            // 'tower_height',
                            // 'tower_type',
                            // 'anchorage_num',
                            // 'shelter_type',
                            // 'ground_system_num',
                            'created_at',
                            'updated_at',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['style' => 'width: 110px;', 'class' => 'head-actions'],
                                'contentOptions' => ['class' => 'row-actions'],
                                'template' => (Yii::$app->user->identity->user_type == 'super-admin' || Yii::$app->user->identity->user_type == 'admin') ? '{view}{update}{delete}' : '{view}{update}',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->bts_code], [
                                            'class' => '',
                                            'data-target' => '#detail-modal',
                                            'data-toggle' => "modal"
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>