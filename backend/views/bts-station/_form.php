<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsStation */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered bts-station-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'bts_code')->textInput(['maxlength' => 200]) ?>

            </div>


            <div class="clearfix"></div>
            <div class="col-md-6">
                <?php $branchData = (Yii::$app->user->identity->user_type == 'branch') ? \backend\models\Branch::find()->where(['id' => Yii::$app->user->identity->branch_id])->all() : \backend\models\Branch::find()->all() ?>
                <?= $form->field($model, 'branch_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map($branchData, 'id', 'name'),
                    'size' => \kartik\widgets\Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a branch')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">

                <?= $form->field($model, 'bts_type')->dropDownList(
                    Yii::$app->params['bts_types'],
                    ['prompt' => Yii::t('backend', 'Choose a bts type')]
                ) ?>


            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <br/>
                <?= $form->field($model, 'status')->checkbox() ?>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-6">
                <?= $form->field($model, 'lat')->textInput() ?>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-6">
                <?= $form->field($model, 'long')->textInput() ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'direction')->textInput([
                    'maxlength' => 255,
                    'id' => 'address',
                    //'rows' => 3,
                    'data-target' => '#addr-modal',
                    'data-toggle' => 'modal',
                    'readonly' => true,
                    'class' => 'form-control address',
                ]) ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'frequency_band')->dropDownList(
                    Yii::$app->params['bts_frequency_bands']
                ) ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'tower_type')->dropDownList(Yii::$app->params['bts_type_of_tower']) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'shelter_type')->dropDownList(Yii::$app->params['bts_type_of_shelter']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'anchorage_num')->textInput(['type' => 'number']) ?>

            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'antenna_num')->textInput(['type' => 'number']) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'rru_num')->textInput(['type' => 'number']) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'tower_height')->textInput(['type' => 'number']) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'ground_system_num')->textInput(['type' => 'number']) ?>
            </div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>
<div id="addr-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?= Yii::t('backend', 'Address') ?></h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'province_id')->widget(Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getProvinceList(), 'province', 'name'),
                            'size' => Select2::MEDIUM,
                            'options' => [
                                'placeholder' => Yii::t('backend', 'Choose a province'),
                                'id' => 'province-id',
                                'none-text' => Yii::t('backend', 'Choose a province'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'addon' => [
//                                'prepend' => [
//                                    'content' => '<i class="glyphicon glyphicon-search"></i>'
//                                ]
                            ],
                        ])->label(Yii::t('backend', 'Province')); ?>

                        <?php

                        echo $form->field($model, 'district_id')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getDistrictList($model->province_id), 'district', 'name'),
                            'options' => ['id' => 'district-id', 'none-text' => Yii::t('backend', 'Choose a district')],
                            'pluginOptions' => [
                                'loading' => false,
                                'depends' => ['province-id'],
                                'placeholder' => Yii::t('backend', 'Choose a district'),
                                'url' => \yii\helpers\Url::to(['area/get-district', 'selected' => $model->district_id]),
                            ]
                        ])->label(Yii::t('backend', 'District'));
                        ?>


                        <?php

                        echo $form->field($model, 'precinct_id')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getPrecinctList($model->province_id, $model->district_id), 'precinct', 'name'),
                            'options' => ['id' => 'precinct-id', 'none-text' => Yii::t('backend', 'Choose a precinct')],
                            'pluginOptions' => [
                                'loading' => false,
                                'depends' => ['province-id', 'district-id'],
                                'placeholder' => Yii::t('backend', 'Choose a precinct'),
                                'url' => \yii\helpers\Url::to(['area/get-precinct', 'selected' => $model->precinct_id]),
                            ]
                        ])->label(Yii::t('backend', 'Precinct'));
                        ?>
                        <?= $form->field($model, 'address_text')->textInput(['id' => 'address-text', 'maxlength' => 255])->label(Yii::t('backend', 'Address')) ?>

                        <div id="addr-display" class="alert alert-info text-center" role="alert"></div>

                        <button class="btn btn-primary" type="button"
                                id="save-addr"><?= Yii::t('backend', 'Accept') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="click-fake"></div>
<?php ActiveForm::end(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#btsstation-bts_type').change(function () {
            if ($('#btsstation-bts_type').val() == 'rru_extenidad') {
                $('#btsstation-shelter_type').val('no_shelter');
            }
        })
        $('.address').change(function () {
            alert('hoang');
            if ($('#address').val() != '') {
                $('.click-fake').click();
            }
        })
    });
</script>