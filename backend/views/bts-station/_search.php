<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsStationSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default bts-station-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'bts_code') ?>
        </div>

        <div class="col-md-3">
            <?=
            $form->field($model, 'status')->dropDownList(
                (isset(Yii::$app->params['status'])) ? Yii::$app->params['status'] : [
                    0 => Yii::t('backend', 'Inactive'),
                    1 => Yii::t('backend', 'Active'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );
            ?>

        </div>

        <div class="col-md-3">
            <?php echo $form->field($model, 'bts_type')->dropDownList(
                Yii::$app->params['bts_types'],
                ['prompt' => Yii::t('backend', 'All')]
            ) ?>

        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
