<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsStation */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'BTS',
]) . ' ' . $model->bts_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'BTS'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->bts_code, 'url' => ['view', 'id' => $model->bts_code]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update') . ' ' . $model->bts_code;
?>
<div class="row bts-station-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
