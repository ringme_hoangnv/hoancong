<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsStation */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'BTS',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'BTS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-station-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
