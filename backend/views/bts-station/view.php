<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsStation */

$this->title = $model->bts_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Stations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-station-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->bts_code],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->bts_code], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'bts_type',
                        'bts_code',

                        [
                            'attribute' => 'branch_id',
                            'value' => function ($object) {

                                return ($object->branch)? $object->branch->name: null;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],

                        'direction',
                        'latlong',

                        'frequency_band',
                        'antenna_num',
                        'rru_num',
                        'tower_height',
                        'tower_type',
                        'anchorage_num',
                        'shelter_type',
                        'ground_system_num',
                        'created_at',
                        'updated_at',
                        [
                            'attribute' => 'created_by',
                            'value' => function($model) {
                                return ($model->created)? $model->created->username: null;
                            }
                        ],
                        [
                            'attribute' => 'updated_by',
                            'value' => function($model) {
                                return ($model->updated)? $model->updated->username: null;
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
