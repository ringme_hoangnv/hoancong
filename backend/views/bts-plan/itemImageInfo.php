<div class="row bts-plan-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-body">
                <?= \yii\widgets\DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'plan_id',
                            'value' => function($model) {
                                return ($model->plan)? $model->plan->name: null;
                            }
                        ],
                        [
                            'attribute' => 'item_id',
                            'value' => function($model) {
                                return ($model->item)? $model->item->name: null;
                            }
                        ],
                        [
                            'attribute' => 'image_path',
                            'value' => function ($model) {
                                return $model->getImagePathUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            //'headerOptions' => ['style' => 'width:120px'],
                        ],

                        [
                            'attribute' => 'status',
                            'value' => function($model) {
                                return $model->getStatusName();
                            }
                        ],
                        'note',

                        [
                            'attribute' => 'created_by',
                            'value' => function($model) {
                                return ($model->createdBy)? $model->createdBy->username: null;
                            }
                        ],
                        [
                            'attribute' => 'updated_by',
                            'value' => function($model) {
                                return ($model->updatedBy)? $model->updatedBy->username: null;
                            }
                        ],
                        'created_at',
                        'updated_at',
                        'approved_at',

                        [
                            'attribute' => 'approved_by',
                            'value' => function($model) {
                                return ($model->approvedBy)? $model->approvedBy->username: null;
                            }
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
