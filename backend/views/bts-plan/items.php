<?php


\backend\assets\ImageGalleryAsset::register($this);

use yii\helpers\Url;
use backend\models\BtsPlanItemImage;
use kartik\file\FileInput;

//use kartik\icons\FontAwesomeAsset;
//FontAwesomeAsset::register($this);


$this->title = Yii::t('backend', 'Update item images');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <br/>
    <div id="plan-info" class="panel panel-default">
        <div class="panel-body row">
            <h3 class="col-md-12" style="
                margin-top: 0;
            "><?= $plan->name; ?></h3>
            <div class="col-md-6"><b><?= Yii::t('backend', 'Partner') ?></b>:
                <?php
                $partner = $plan->partner;
                echo ($partner) ? $partner->name : '';
                ?>
            </div>

            <div class="col-md-6">
                <b><?= Yii::t('backend', 'Bts') ?></b>: <?= ($plan->btsCode) ? $plan->btsCode->bts_code . ' (' . $plan->bts_code . ')' : ''; ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <b><?= Yii::t('backend', 'Item cate') ?></b>: <?= ($plan->itemCategory) ? $plan->itemCategory->name : ''; ?>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6"><b><?= Yii::t('backend', 'Start time') ?></b>: <?= $plan->start_at; ?></div>
            <div class="col-md-6"><b><?= Yii::t('backend', 'End time') ?></b>: <?= $plan->end_at; ?></div>
            <div class="clearfix"></div>
            <?php if ($plan->status == 1): ?>
                <div class="col-md-12">
                    <a target="_blank" style="margin-top: 15px" href="<?= Url::to(['bts-plan/export', 'id' => $plan->id]) ?>"
                       class="btn btn-primary fa fa-export"><?= Yii::t('backend', 'Export') ?></a>
                </div>
            <?php endif; ?>
        </div>
    </div>


    <div class="panel-group" id="accordion">

        <?php if (count($items)): ?>
            <?php foreach ($items as $index => $item): ?>
                <?php

                $planItem = \backend\models\BtsPlanItem::findOne([
                    'plan_id' => $plan->id,
                    'item_id' => $item->id
                ]);
                $imageList = \backend\models\BtsPlanItemImage::find()
                    ->where([
                        'plan_id' => $plan->id,
                        'item_id' => $item->id,
                    ])
                    ->orderBy('updated_at desc')
                    ->all();
                $minImageCount = ($planItem->image_num - count($imageList));
                if ($minImageCount < 0) {
                    $minImageCount = 0;
                }

                ?>

                <div class="panel panel-default item itemstatus-<?= $planItem->status ?>">
                    <div class="panel-heading" style="position: relative">
                        <div class="row">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $item->id ?>">
                                    <i class="glyphicon glyphicon-menu-down"></i>
                                    <?= $item->name ?></a>
                            </h4>
                            <div class="upload-act" >
                                <b><?= Yii::t('backend', 'Total images') ?>:</b> <span id="ti-<?= $item->id ?>"><?= count($imageList) ?></span> / <?= $planItem->image_num; ?>
                                <?php if (Yii::$app->user->can('super-admin') || Yii::$app->user->identity->user_type == 'partner'): ?>
                                    <button type="button" class="btn-sm btn btn-default" data-toggle="modal"
                                            data-target="#ul-modal-<?= $item->id ?>"><?= Yii::t('backend', 'Upload Image') ?></button>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                    <?php if (Yii::$app->user->can('super-admin') || Yii::$app->user->identity->user_type == 'partner'): ?>
                        <div id="ul-modal-<?= $item->id ?>" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?= Yii::t('backend', 'Upload images for item: {item_name}', [
                                                'item_name' => $item->name
                                            ]) ?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <?php
                                        echo FileInput::widget([
                                            'name' => 'image_upload[]',
                                            'bsVersion' => '3.x',
                                            'options' => [
                                                'id' => 'image-upload-' . $plan->id . '-' . $item->id,
                                                'class' => 'image-upload-field',
                                                'multiple' => true,
                                                'accept' => 'image/*'
                                            ],
                                            'pluginOptions' => [
                                                'uploadUrl' => Url::to(['/bts-plan-item-image/upload']),
                                                'uploadExtraData' => [
                                                    'item_id' => $item->id,
                                                    'plan_id' => $plan->id,
                                                ],
                                                'minFileCount' => $minImageCount,
                                                'maxFileCount' => $planItem->image_num,
                                                'maxFileSize' => (Yii::$app->params['image_upload_size']*1024), // Kb
                                                //'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                                                //                                    'showPreview' => false,
                                                //                                    'showCaption' => true,
                                                'showRemove' => false,
                                                //                                    'showUpload' => false,
                                            ]
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div id="collapse-<?= $item->id; ?>"
                         class="panel-collapse collapse <?php // ($index == 0) ? 'in' : ''; ?>">
                        <div class="item-info ">
                            <div class="col-md-6">
                                <b><?= Yii::t('backend', 'Item Group') ?>:</b> <?= $item->group->name ?>
                            </div>
                            <div class="col-md-6">


                            </div>
                            <div class="col-md-12 item-note">
                                <b><?= Yii::t('backend', 'Description') ?>:</b>
                                <div><?= $item->note ?></div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div id="upload-images-<?= $item->id ?>" item-id="<?= $item->id ?>" class="panel-body upload-images">



                            <?php if (count($imageList)): ?>
                                <?php foreach ($imageList as $index => $image): ?>
                                    <?php
                                    $imageUrl = $image->getImagePathUrl();
                                    $thumbPath = $image->getImagePathUrl(200);

                                    ?>
                                    <div id="item-image-<?= $image->id; ?>"
                                         class="image-a imgstatus<?= $image->status ?> <?= ($image->status != 1)? 'not-approve': '' ?>"
                                         img-note="<?= \yii\helpers\Html::encode($image->note); ?>"
                                    >
                                        <img data-target="#big-img-modal"
                                             data-toggle="modal"
                                             data-src="<?= $imageUrl ?>" src="<?= $thumbPath ?>"
                                             onError="this.onerror=null;this.src='/img/no-image-square.jpg';"

                                        />
                                        <div id="img-note-<?= $image->id ?>" title="<?= \yii\helpers\Html::encode($image->note) ?>" class="img-note"><?= \yii\helpers\Html::encode($image->note) ?></div>
                                        <div class="image-actions" item-id="<?= $item->id ?>">
                                    <span title="<?= Yii::t('backend', 'Info') ?>"
                                          data-target="#info-modal"
                                          data-toggle="modal"
                                          href="<?= Url::to(['bts-plan-item-image/info', 'id' => $image->id]) ?>"
                                          img-id="<?= $image->id; ?>" class="image-action icon-info "></span>
                                            <?php if (Yii::$app->user->identity->user_type != 'partner'): ?>
                                                <span id="disapprove-img-<?= $image->id; ?>"
                                                      title="<?= Yii::t('backend', 'Disapprove') ?>"
                                                      data-toggle="modal"
                                                      data-target="#disapprove-modal"
                                                      img-id="<?= $image->id; ?>"
                                                      class="disapprove-img-<?= $image->id; ?> image-action icon-dislike btn-disapprove <?= ($image->status == BtsPlanItemImage::STATUS_DISAPPROVED) ? 'active' : '' ?>"></span>
                                                <span id="approve-img-<?= $image->id; ?>"
                                                      title="<?= Yii::t('backend', 'Approve') ?>"
                                                      ahref="<?= Url::to(['bts-plan-item-image/approve']) ?>"
                                                      img-id="<?= $image->id; ?>"
                                                      class="approve-img-<?= $image->id; ?> image-action icon-like <?= ($image->status == BtsPlanItemImage::STATUS_APPROVED) ? 'active' : '' ?>"></span>
                                            <?php endif; ?>
                                            <span title="<?= Yii::t('backend', 'Delete') ?>"
                                                  ahref="<?= Url::to(['bts-plan-item-image/delete']) ?>"
                                                  data-confirm="<?= Yii::t('backend', 'Are you sure you want to delete this image?') ?>"
                                                  img-id="<?= $image->id; ?>"
                                                  class="image-action glyphicon glyphicon-trash"></span>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>

                            <?php endif; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            <?php endforeach; ?>
            <div class="clearfix"></div>
        <?php endif; ?>
    </div>

    <div id="disapprove-modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                    <h4><b><?= Yii::t('backend', 'Disapprove image') ?></b></h4>
                    <form id="disapprove-form" method="post"
                          action="<?= Url::to(['bts-plan-item-image/disapprove']) ?>">
                    <textarea placeholder="<?= Yii::t('backend', 'Reason') ?>" id="reason" name="reason"
                              class="form-control" rows="4"></textarea>
                        <div id="error-msg" class="error font-red font-italic"></div>
                        <br/>
                        <button type="submit" class="btn btn-warning"><?= Yii::t('backend', 'Disapprove') ?></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal"><?= Yii::t('backend', 'Close') ?></button>
                        <input type="hidden" id="disapprove-image-id" name="id" value=""/>
                    </form>

                    <div class="clearfix"></div>
                </div>

            </div>

        </div>
    </div>
    <div id="info-modal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <div id="big-img-modal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img src="/img/no-image-square.jpg" id="imagepreview" style="max-width: 100%; max-height: 600px;">
                    <br />
                    <br />
                    <div id="img-note" class="alert alert-warning" role="alert"></div>
                    <div id="img-controls">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="image-template" type="text/x-handlebars-template">
    <div id="item-image-{{image_id}}" class="image-a imgstatus-0">
        <img data-target="#big-img-modal" data-toggle="modal" data-src="{{image_url}}" src="{{thumb_url}}"/>
        <div id="img-note-{{image_id}}" title="" class="img-note"></div>
        <div id="act-{{actid}}" class="image-actions" item-id="{{item_id}}" thumbid="{{actid}}">
            <span title="<?= Yii::t('backend', 'Info') ?>"
                  data-target="#info-modal"
                  data-toggle="modal"
                  href="<?= Url::to(['bts-plan-item-image/info?id={{image_id}}']) ?>"
                  img-id="{{image_id}}" class="image-action icon-info "></span>
            <?php if (Yii::$app->user->identity->user_type != 'partner'): ?>
                <span id="disapprove-img-{{image_id}}"
                      title="<?= Yii::t('backend', 'Disapprove') ?>"
                      data-toggle="modal"
                      data-target="#disapprove-modal"
                      img-id="{{image_id}}"
                      class="image-action icon-dislike btn-disapprove disapprove-img-{{image_id}}"></span>
                <span id="approve-img-{{image_id}}" title="<?= Yii::t('backend', 'Approve') ?>"
                      ahref="<?= Url::to(['bts-plan-item-image/approve']) ?>"
                      img-id="{{image_id}}"
                      class="image-action icon-like approve-img-{{image_id}}"></span>
            <?php endif; ?>
            <span id="del-img-{{image_id}}" title="<?= Yii::t('backend', 'Delete') ?>"
                  ahref="<?= Url::to(['bts-plan-item-image/delete']) ?>"
                  data-confirm="<?= Yii::t('backend', 'Are you sure you want to delete this image?') ?>"
                  img-id="{{image_id}}" class="image-action glyphicon glyphicon-trash"></span>
        </div>
    </div>
</script>
<?php
$this->registerJs(<<< EOT_JS_CODE
$(document).ready(function() {
    $(".item").on( "click", '.image-a img', function() {
        var src = $(this).attr('data-src');
        var note = $.trim($(this).parent().attr('img-note'));
       $('#imagepreview').attr('src', src);
       if (note) {
        $('#img-note').html(note);
        $('#img-note').show();
       } else {
        $('#img-note').hide();
       }

       $('#img-controls').html($('.image-actions', $(this).parent()).html());
    });


    $("#big-img-modal").on('shown.bs.modal', function(){

      });

});

EOT_JS_CODE
);

?>
<div class="clearfix"></div>
