<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BtsPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Bts Plans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-plan-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                <?php if (Yii::$app->user->can('admin') || Yii::$app->user->can('super-admin')
                    || Yii::$app->user->can('ho') || Yii::$app->user->can('branch')): ?>
                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                            'modelClass' => Yii::t('backend', 'Bts Plan')
                        ]),
                            ['create'], ['class' => 'btn btn-info btn-sm']) ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],


                            [
                                'attribute' => 'name',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if (Yii::$app->user->identity->user_type == 'partner') {
                                        return Html::a(Html::encode($model->name), ['bts-plan/update-item-images', 'id' => $model->id,]);
                                    }
                                    return Html::a(Html::encode($model->name), ['bts-plan/update', 'id' => $model->id,]);
                                },
                            ],
                            [
                                'attribute' => 'partner_id',
                                'value' => function ($model) {
                                    return ($model->partner) ? $model->partner->name : null;
                                },
                                'visible' => (Yii::$app->user->identity->user_type != 'partner') ? true : false,
                            ],

                            'bts_code',
                            [
                                'attribute' => 'item_category_id',
                                'value' => function ($model) {
                                    return ($model->itemCategory) ? $model->itemCategory->name : null;
                                }
                            ],
                            'description',

                            [
                                'attribute' => 'is_exported',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->is_exported == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_exported"></span>';
                                }
                            ],

                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],

                            'start_at',
                            'end_at',
                            // 'created_by',
                            // 'created_at',
                            // 'updated_at',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => [
                                    'style' => ((Yii::$app->user->identity->user_type == 'partner') ? 'width: 70px' : 'width: 115px;'),
                                    'class' => 'head-actions'
                                ],
                                'contentOptions' => ['class' => 'row-actions'],
                                //'template' => (Yii::$app->user->identity->user_type == 'partner')? '{image}{view}': '{image}{view}{update}{delete}',
                                'template' => (Yii::$app->user->identity->user_type == 'super-admin' || Yii::$app->user->identity->user_type == 'admin') ? '{image}{view}{update}{delete}' : '{image}{view}',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->id], [
                                            'class' => '',
                                            'data-target' => '#detail-modal',
                                            'data-toggle' => "modal"
                                        ]);
                                    },
                                    'image' => function ($url, $model) {
                                        return ($model->status == 1) ? Html::a('<span class="icon-picture"></span>', ['update-item-images', 'id' => $model->id]) : '';
                                    },
                                ]
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>