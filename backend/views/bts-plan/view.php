<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsPlan */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-plan-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'partner_id',
                            'value' => function($model) {
                                return ($model->partner)? $model->partner->name: null;
                            }
                        ],

                        'bts_code',
                        [
                            'attribute' => 'item_category_id',
                            'value' => function($model) {
                                return ($model->itemCategory)? $model->itemCategory->name: null;
                            }
                        ],
                        'name',
                        'description',
                        [
                            'attribute' => 'is_exported',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_exported == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],

                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        [
                            'format' => 'raw',
                            'label' => Yii::t('backend', 'File'),
                            'value' => function ($model) {
                                if ($model->file_path) {
                                    return '<a download data-pjax="0" href="' . $model->file_path . '">' . Yii::t('backend', 'Download') . '</a>';
                                }
                            }
                        ],

                        'start_at',
                        'end_at',

                        [
                            'attribute' => 'created_by',
                            'value' => function($model) {
                                return ($model->createdBy)? $model->createdBy->username: null;
                            }
                        ],
                        [
                            'attribute' => 'updated_by',
                            'value' => function($model) {
                                return ($model->updatedBy)? $model->updatedBy->username: null;
                            }
                        ],
//                        [
//                            'attribute' => 'updated_by',
//                            'value' => function($model) {
//                                return ($model->updatedBy)? $model->updatedBy->username: null;
//                            }
//                        ],
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
