<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use backend\models\Partner;
use backend\models\BtsStation;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsPlan */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered bts-plan-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

            </div>
            <div class="col-md-6">
                <br />
                <?= $form->field($model, 'status')->checkbox() ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'start_at', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>

            </div>

            <div class="col-md-6">
                <?=
                $form->field($model, 'end_at', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <?php
                if (Yii::$app->user->identity->user_type == 'branch') {
                    $partners = Partner::getPartnersByBranch(Yii::$app->user->identity->branch_id);
                } else {
                    $partners = Partner::find()->all();
                }
                ?>
                <?= $form->field($model, 'partner_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map($partners, 'id', 'name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a partner')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?php
                if (Yii::$app->user->identity->user_type == 'branch') {
                    $btsList = BtsStation::find()->where(['branch_id' => Yii::$app->user->identity->branch_id])->all();
                } else {
                    $btsList = BtsStation::find()->all();
                }
                ?>
                <?= $form->field($model, 'bts_code')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map($btsList, 'bts_code', 'bts_code'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a BTS')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'item_category_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'id' => 'item-cate-id',
                        'placeholder' => Yii::t('backend', 'Choose an item cate')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => 1000, 'rows' => 4]) ?>

            </div>
            <div class="clearfix"></div>

            <div class="col-md-12">
<?php

$format = <<< SCRIPT
function format(state) {
console.log(state)
    if (!state.id) return state.text; // optgroup
    src = state.image_path + '.png'
    return '<img class="flag" src="' + src + '"/>' + state.text;
}
SCRIPT;
                ?>
                <?php

                $escape = new \yii\web\JsExpression("function(m) { return m; }");
                $this->registerJs($format, \yii\web\View::POS_HEAD);

                 echo $form->field($model, 'selected_item_ids', [
                     'template' => '<div class="form-group"><label class="control-label">{label}</label>
                    <a style="margin-left: 50px" target="_blank" href="'. \yii\helpers\Url::to(['bts-item/index']). '">#'. Yii::t('backend', 'Items management'). '</a>
                    {input}</div>',
                 ])->widget(DepDrop::classname(), [

                    'type' => DepDrop::TYPE_SELECT2,
                    'select2Options' => ['pluginOptions' => [
                        'allowClear' => false,
                        'multiple' => true,
                        'placeholder' => Yii::t('backend', 'Choose items'),

//                        'templateResult' => new \yii\web\JsExpression('format'),
//                        'templateSelection' => new \yii\web\JsExpression('format'),
//                        'escapeMarkup' => $escape,
                    ]],
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\BtsItem::listActiveItemsByCate($model->item_category_id), 'id', 'name'),
                    'options'=>['id'=>'plan-item-ids'],
                    'pluginOptions'=>[
                        'depends'=>['item-cate-id'],

                        'url' => \yii\helpers\Url::to(['bts-item/get-active-items-by-cate', 'selected' => $model->selected_item_ids]),
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>
        <?php if (!$model->isNewRecord && $model->status == 1):  ?>
        &nbsp;&nbsp;&nbsp;
        <a href="<?= \yii\helpers\Url::to(['update-item-images', 'id' => $model->id]); ?>" class="btn btn-warning btn-sm">
            <i class="fa fa-picture-o"></i> <?= Yii::t('backend', 'Update item images') ?>                </a>
        <?php endif; ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
