<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use backend\models\Partner;
use backend\models\BtsStation;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsPlan */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered bts-plan-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
            </div>

            <div class="col-md-6">
                <?=
                 $form->field($model, 'file_path')->fileInput()
                ?>
                <?php if (!$model->isNewRecord  && $model->file_path): ?>
                    <a href="<?= $model->file_path; ?>" download><?= Yii::t('backend', 'Download') ?></a>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'start_at', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>

            </div>

            <div class="col-md-6">
                <?=
                $form->field($model, 'end_at', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <?php
                if (Yii::$app->user->identity->user_type == 'branch') {
                    $partners = Partner::getPartnersByBranch(Yii::$app->user->identity->branch_id);
                } else {
                    $partners = Partner::find()->all();
                }
                ?>
                <?= $form->field($model, 'partner_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map($partners, 'id', 'name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a partner')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?php
                if (Yii::$app->user->identity->user_type == 'branch') {
                    $btsList = BtsStation::find()->where(['branch_id' => Yii::$app->user->identity->branch_id])->all();
                } else {
                    $btsList = BtsStation::find()->all();
                }
                ?>
                <?= $form->field($model, 'bts_code')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map($btsList, 'bts_code', 'bts_code'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a BTS')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'item_category_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'id' => 'item-cate-id',
                        'placeholder' => Yii::t('backend', 'Choose an item cate')
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>

            <div class="col-md-6 info-bts-code ">

            </div>

        </div>

        <div class="clearfix"></div>

        <div class="col-md-6">
            <?= $form->field($model, 'postes_num')->textInput(['type' => 'number', 'min' => 1]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'retenida_num')->textInput(['type' => 'number', 'min' => 1]) ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <?= $form->field($model, 'panel_solar_num')->textInput(['type' => 'number', 'min' => 1]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'vientos_num')->textInput(['type' => 'number', 'min' => 1]) ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <?= $form->field($model, 'cuerpos_num')->textInput(['type' => 'number', 'min' => 1]) ?>
            <br/>
            <?= $form->field($model, 'status')->checkbox() ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'pozos_num')->textInput(['type' => 'number','min' =>1]) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'description')->textarea(['maxlength' => 1000, 'rows' => 4]) ?>
        </div>

    </div>
</div>
<div class="portlet-title ">


    <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
        <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
    &nbsp;&nbsp;&nbsp;
    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>
    <?php if (!$model->isNewRecord && $model->status == 1): ?>
        &nbsp;&nbsp;&nbsp;
        <a href="<?= \yii\helpers\Url::to(['update-item-images', 'id' => $model->id]); ?>"
           class="btn btn-warning btn-sm">
            <i class="fa fa-picture-o"></i> <?= Yii::t('backend', 'Update item images') ?>                </a>
    <?php endif; ?>
</div>
</div>

<?php ActiveForm::end(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        function getBts() {
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl . '/bts-plan/getbts' ?>',
                type: 'post',
                data: {
                    bts_code: $('#btsplan-bts_code').val(),
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
                },
                success: function (data) {
                    $(".info-bts-code").text('Tower Type : ' + data.tower_type + ";" +
                        ' Antenna Num : ' + data.antenna_num + ";" +
                        ' Shelter Type :  ' + data.shelter_type + ";" +
                        ' Ground System Num : ' + data.ground_system_num + ";" +
                        " Anchorage Num : " + data.anchorage_num + ";" +
                        ' Tower Height : ' + data.tower_height + ";" +
                        " Rru Num : " + data.rru_num
                    )
                }
            });
        }
        getBts();
        $('#btsplan-bts_code').change(function () {
            getBts();
        })

    });
</script>



