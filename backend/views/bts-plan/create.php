<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsPlan */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Bts Plan',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Plans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-plan-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
