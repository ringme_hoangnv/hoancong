<table class="table table-bordered">
    <tbody>
    <tr>
        <td width="200" height="100" class="text-left"><img src="<?= \common\helpers\Helpers::getLocalMediaUrl('/img/bitel-logo.png') ?>" width="200px" /></td>
        <td class="text-center">
            <h3 class="text-center text-uppercase"><?= Yii::t('backend', 'Report {item_cate}',[
                    'item_cate' => $plan->itemCategory->name,
                ]) ?>
            </h3>
        </td>
        <td width="200" class="text-center text-uppercase">
            <?= $plan->itemCategory->name; ?>
        </td>
    </tr>
    </tbody>
</table>

<table class="table table-bordered" style="border: none;">
    <tbody>
    <tr>
        <td class="text-uppercase" width="130">
            <?= Yii::t('backend', 'Contratista')?>:
        </td>
        <td class="">
            <?= ($plan->partner)? $plan->partner->name: ''; ?>
        </td>
        <td width="50" class="text-center text-uppercase " style="border-top: none;border-bottom: none;">

        </td>
        <td class="text-uppercase" width="130">
            <?= Yii::t('backend', 'Fecha')?>:
        </td>
        <td class="">
            <?php
            $lastApproveImage = \backend\models\BtsPlanItemImage::find()
                ->where(['plan_id' => $plan->id])
                ->orderBy('approved_at desc')
                ->one();
            echo ($lastApproveImage)? $lastApproveImage->approved_at: '';
            ?>
        </td>
    </tr>
    <tr>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Sup. contrata')?>:
        </td>
        <td class="">
            <?php
            $lastUploadImage = \backend\models\BtsPlanItemImage::find()
                ->where(['plan_id' => $plan->id])
                ->orderBy('created_at desc')
                ->one();
            $lastUploadUser = $lastUploadImage->createdBy;
            echo ($lastUploadUser)? $lastUploadUser->fullname: '';
            ?>
        </td>
        <td width="50" class="text-center text-uppercase " style="border-top: none;border-bottom: none;">

        </td>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Sup. bitel')?>:
        </td>
        <td class="">
            <?= ($lastApproveImage->approvedBy)? $lastApproveImage->approvedBy->fullname: ''; ?>
        </td>
    </tr>
    <tr>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'MTS fibra')?>:
        </td>
        <td class="">

        </td>
        <td width="50" class="text-center text-uppercase " style="border-top: none;border-bottom: none;">

        </td>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Qty postes')?>:
        </td>
        <td class="">
            <?= $plan->postes_num ?>
        </td>
    </tr>
    <tr>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Estación')?>:
        </td>
        <td class="">
            <?= $plan->bts_code; ?>
        </td>
        <td width="50" class="text-center text-uppercase " style="border-top: none;border-bottom: none;">

        </td>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Sucursal')?>:
        </td>
        <td class="">
            <?php
            $bts = $plan->btsCode;
            $branch = ($bts)? $bts->branch: null;
            ?>
            <?= ($branch)? $branch->name: '' ?>
        </td>
    </tr>
    <tr>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'Start time')?>:
        </td>
        <td class="">
            <?= $plan->start_at; ?>
        </td>
        <td width="50" class="text-center text-uppercase " style="border-top: none;border-bottom: none;">

        </td>
        <td class="text-uppercase">
            <?= Yii::t('backend', 'End time')?>:
        </td>
        <td class="">
            <?= $plan->end_at; ?>
        </td>
    </tr>
    </tbody>
</table>
<?php /*
<div class="container ">
    <table class="table table-no-bordered text-center text-uppercase">
        <tbody>
        <tr>
            <td style="border: 1px solid #000000;">
                <img src="/img/bitel-logo.png" width="400px" />
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">
                <?=  $plan->btsCode->bts_code; ?>
            </td>
        </tr>
        </tbody>

    </table>

</div>
 */ ?>
<br />
<div class="container ">
    <?php $totalImage = count($images); ?>
    <?php if ($totalImage): ?>
        <table class="table table-no-bordered">
            <tbody>
            <?php
            $index = 1;
            $currentItemId = null;

            ?>
            <?php foreach ($images as $idx => $image):?>
                <?php echo ($index % 3 == 1)? '<tr style="margin-bottom: 15px;border:none; ">': '' ?>
                <?php
//                if ($currentItemId != $image->item_id) {
//                    $index = 1;
//                    $currentItemId = $image->item_id;
//                }
                ?>
                <td title="<?= $index ?> <?= $index % 2 ?> <?= $idx. '=='. ($totalImage - 1) ?>" class="text-center" width="33%" style="border: 1px solid #000000 !important;vertical-align: top">
                    <table class="table table-bordered ">
                        <tbody>
                            <tr>
                                <td style="height: 170px;">
                                    <img src="<?= $image->getLocalImageUrl(200); ?>" style="<?= ($index < 6)? 'max-height: 150px;': 'max-height: 160px;'; ?> max-width: 180px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center text-uppercase">
                                    <?= $image->item->name. ' '. $index ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </td>
                <?php  echo ($index % 3 == 1 || $index % 3 == 2)? '<td style="border:none;width: 15px;"></td>': '' ?>

                <?php echo ($index % 3 == 0 )? '</tr>': '' ?>
                <?php $index = $index + 1; ?>
            <?php endforeach;?>
            </tbody>
        </table>
    <?php endif;?>


</div>