<?php
/* @var $this View */

use yii\web\View;
use backend\models\User;
use backend\models\TelesaleStorage;
use common\helpers\Helpers;
use backend\components\common\TelesaleSoapCaller;
use yii\helpers\Url;
use \backend\models\BookingHistory;

$this->title = Yii::$app->params['system_name'];
?>
    <div class="site-index">
        <?php
        $user = Yii::$app->user;
        echo (\Yii::$app->request->BaseUrl);
        ?>
        <br />
        <?php
        // var_dump(Yii::$app->language)?>
    </div>
    <h1>My First Google Map</h1>

    <div id="googleMap" style="width:100%;height:400px;"></div>
<?php //\backend\assets\HomepageAsset::register($this); ?>
<script>
    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(51.508742,-0.120850),
            zoom:5,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script>
