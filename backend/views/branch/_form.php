<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Branch */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered branch-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => 1000, 'rows' => 4]) ?>

            </div>
            <div class="col-md-12">

                <br />
                <?php echo $form->field($model, 'selected_partners', [
                    'template' => '<div class="form-group"><label class="control-label">{label}</label>
                    <a style="margin-left: 50px" target="_blank" href="'. \yii\helpers\Url::to(['partner/index']). '">#'. Yii::t('backend', 'Partner management'). '</a>
                    {input}</div>',
                ])->widget(Select2::classname(), [

                    'data' => \backend\models\Partner::getPartnerArray(),
                    //'maintainOrder' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'language' => Yii::$app->language,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose Partners'),
                        'multiple' => true,
                    ],
//                    'pluginOptions' => [
//                        'tags' => true,
//                        'tokenSeparators' => [','],
//                        'maximumInputLength' => 20
//                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
