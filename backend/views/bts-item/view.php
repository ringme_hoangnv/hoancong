<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsItem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-item-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
//                        [
//                            'attribute' => 'image_path',
//                            'value' => function ($model) {
//                                return $model->getImagePathUrl();
//                            },
//                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
//                            'headerOptions' => ['style' => 'width:120px'],
//                        ],
                        'name',
                        [
                            'attribute' => 'category_id',
                            'value' => function($model) {
                                return ($model->category)? $model->category->name: null;
                            }
                        ],
                        [
                            'attribute' => 'group_id',
                            'value' => function($model) {
                                return ($model->group)? $model->group->name: null;
                            }
                        ],
                        'image_num:integer',
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        'note',
                        'created_at',
                        'updated_at',
                        [
                            'attribute' => 'created_by',
                            'value' => function($model) {
                                return ($model->created)? $model->created->username: null;
                            }
                        ],
                        [
                            'attribute' => 'updated_by',
                            'value' => function($model) {
                                return ($model->updated)? $model->updated->username: null;
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
