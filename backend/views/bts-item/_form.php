<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsItem */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered bts-item-form">

        <div class="portlet-body">
            <div class="form-body row">
                <div class="col-md-6">
                    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                        'size' => Select2::MEDIUM,
                        'options' => [
                            'placeholder' => Yii::t('backend', 'Choose a category'),
                            'id' => 'category-id'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        'addon' => [
                            'prepend' => [
                                'content' => '<i class="glyphicon glyphicon-search"></i>'
                            ]
                        ],
                    ]); ?>
                </div>

                <div class="col-md-6">
                    <?php

                    echo $form->field($model, 'group_id')->widget(DepDrop::classname(), [
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                        'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemGroup::getGroupListByCate($model->category_id), 'id', 'name'),
                        'options' => ['id' => 'group-id'],
                        'pluginOptions' => [
                            'depends' => ['category-id'],
                            //'placeholder' => Yii::t('backend', 'Choose '),
                            'url' => \yii\helpers\Url::to(['item-category/get-item-group', 'selected' => $model->group_id]),
                        ]
                    ]);
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'image_num')->textInput(['type' => 'number','min' =>1]) ?>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-6">
                    <br/>
                    <?= $form->field($model, 'status')->checkbox() ?>

                    <?= $form->field($model, 'note')->textarea(['maxlength' => 2000, 'rows' => 4]) ?>
                </div>
                <?php /*
                <div class="col-md-6">

                    <?= $this->render('/common/_slim_image_field', [
                        'fieldName' => 'image_path',
                        'itemName' => 'item-image', // Vi du: adv, post, post-category de luu rieng tung folder
                        'fieldLabel' => Yii::t('backend', 'Image'),
                        'dataMinSize' => '0,0',
                        'dataSize' => '',
                        'dataForceSize' => '',
                        'dataRatio' => '',
                        'model' => $model,
                        'dataWillRemove' => 'imageWillChange',
                        'dataWillSave' => 'imageWillChange',
                        'dataMaxFileSize' => 0.3
                    ]) ?>
                </div>
                */ ?>
                <div class="clearfix"></div>

            </div>
        </div>
        <div class="portlet-title ">


            <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
                <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
            &nbsp;&nbsp;&nbsp;
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>