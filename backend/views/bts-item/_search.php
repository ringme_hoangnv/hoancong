<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\BtsItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default bts-item-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'name') ?>

        </div>
        <div class="col-md-3">

            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All category')
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'status')->dropDownList(
                (isset(Yii::$app->params['status'])) ? Yii::$app->params['status'] : [
                    0 => Yii::t('backend', 'Not yet ') . Yii::t('backend', 'Status'),
                    1 => Yii::t('backend', 'Status'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>
        <div class="col-md-3">
            <?=

            $form->field($model, 'created_at')->widget(\kartik\daterange\DateRangePicker::className(), [
                    'options' => ['class' => 'form-control daterange-field input-sm'],
                    'model' => $model,
                    'attribute' => 'created_at',
                    'convertFormat' => true,
                    'presetDropdown' => true,
                    'readonly' => true,
                    'pluginOptions' => [
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd/m/Y',
                        ]
                    ],
                    'pluginEvents' => [
                        'cancel.daterangepicker' => "function(ev, picker) {
                                            $(this).val('');
                                        }",
                        'apply.daterangepicker' => 'function(ev, picker) {
                                            if($(this).val() == "") {
                                                $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                                                picker.endDate.format(picker.locale.format)).trigger("change");
                                            }
                                        }',
                        'show.daterangepicker' => 'function(ev, picker) {
                                            picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                                            if($(this).val() == "") {
                                                picker.container.find(".ranges .active").removeClass("active");
                                            }
                                        }',
                    ]
                ]
            )
            ?>

        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
