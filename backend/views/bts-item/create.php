<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BtsItem */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Bts Item',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bts Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row bts-item-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
