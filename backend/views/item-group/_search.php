<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\ItemGroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default item-group-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'name') ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All')
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>

        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'status')->dropDownList(
                (isset(Yii::$app->params['status'])) ? Yii::$app->params['status'] : [
                    0 => Yii::t('backend', 'Deactivated') ,
                    1 => Yii::t('backend', 'Activated'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
