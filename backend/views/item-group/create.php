<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ItemGroup */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Item Group',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Item Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row item-group-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
