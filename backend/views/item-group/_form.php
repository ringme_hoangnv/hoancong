<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\ItemGroup */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered item-group-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">

                <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategory::find()->all(), 'id', 'name'),
                    'size' => Select2::MEDIUM,
//                    'options' => [
//                        'placeholder' => Yii::t('backend', '')
//                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

            </div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <?= $form->field($model, 'calculate_image_method')->dropDownList(
                    Yii::$app->params['item_group_calculate_image_method'], [

                ])->hint('The method will be used to calculate number of images that partner must upload for each item'); ?>

                <?= $form->field($model, 'status')->checkbox() ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => 500, 'rows' => 4,]) ?>

            </div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
