<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\ItemCategory */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered item-category-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">

                <?= $form->field($model, 'category_type_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\ItemCategoryType::find()->all(), 'id', 'name'),
                    'size' => Select2::MEDIUM,
//                    'options' => [
//                        'placeholder' => Yii::t('backend', '')
//                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <br />
                <?= $form->field($model, 'status')->checkbox() ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => 500, 'rows' => 4]) ?>

            </div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
