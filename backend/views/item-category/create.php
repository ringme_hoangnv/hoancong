<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ItemCategory */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Item Category',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Item Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row item-category-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
