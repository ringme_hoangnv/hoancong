<?php
use yii\helpers\Url;
use \yii\helpers\Html;
?>
<?php
$absParent = str_replace(Yii::$app->params['ftp_upload_path'], '', $parent);

$folderArr = explode ('/', $absParent);

?>
<?php if ($absParent):?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <?php
        if ($count = count($folderArr)) {
            $path = Yii::$app->params['ftp_upload_path'];
            foreach($folderArr as $index => $folder) {

                ?>

                <?php if(!$folder):?>
                    <li class="breadcrumb-item"><span class="bfolder" href="<?= Url::to(['file-manager/index', 'parent' => Yii::$app->params['ftp_upload_path']]) ?>"><i class="fa fa-home" ></i></span></li>
                <?php else: ?>
                    <?php
                    $path .= '/'.$folder;
                    ?>
                    <li class="breadcrumb-item <?= ($index == ($count-1))? 'active': '' ?>"><span class="<?= ($index == ($count-1))? '': 'bfolder' ?>" href="<?= Url::to(['file-manager/index', 'parent' => $path]) ?>"><?= basename($folder)?></span></li>
                <?php endif; ?>
                <?php
            }
        }

        ?>

    </ol>
</nav>
<?php endif; ?>

<?php if (count($folders)): ?>
    <?php foreach ($folders as $folder): ?>
        <?php $folderName = basename($folder); ?>
        <span class="folder fa fa-folder" folder-name="<?= $folderName ?>" href="<?= Url::to(['file-manager/index', 'parent' => $folder]) ?>" >
                <?= Html::encode($folderName); ?>
            </span>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (count($files)): ?>
    <?php foreach ($files as $file): ?>
        <span class="file fa fa-file" filepath="<?= str_replace(Yii::$app->params['ftp_upload_path'], '', $file) ?>" >
            <?= Html::encode(basename($file)); ?>
        </span>
    <?php endforeach; ?>
<?php endif; ?>