<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use api_public\modules\v1\models\Charge;
use api_public\modules\v1\models\GetNewPassForm;
use api_public\modules\v1\models\Prize;
use api_public\modules\v1\models\Subscriber;
use common\core\MpsConnector;
use common\helpers\Helpers;
use common\models\OneTimePasswordBase;
use Yii;
use yii\data\Pagination;

class AccountController extends ApiController
{


    /**
     * Doi mat khau sau khi dang nhap vao he thong
     * @return array
     */
    public function actionChangePassword()
    {
        $checkLogin = self::checkTokenFromHeader();
        if (is_array($checkLogin) && isset($checkLogin['errorCode'])) {
            return $checkLogin;
        }

        $webSub = $checkLogin;

        $requestData = json_decode(Yii::$app->request->getRawBody());

        $password = $requestData->password;
        $newPassword = $requestData->new_pass;
        $repeatPassword = $requestData->repeat_password;

        $message = '';
        $data = [];

        if (!$password || !$newPassword || !$repeatPassword) {
            $errCode = ApiResponseCode::FORM_INVALID;
            $message = Yii::t('api', 'Vui lòng nhập đủ thông tin!');
        } else if ($newPassword != $repeatPassword) {
            $errCode = ApiResponseCode::FORM_INVALID;
            $message = Yii::t('api', 'Nhập lại mật khẩu mới chưa khớp!');
        }

        if (!$webSub) {
            $errCode = ApiResponseCode::LOGIN_IS_REQUIRED;

        } else if (!$webSub->validatePassword($password)) {
            // Mat khau cu khong dung
            $errCode = ApiResponseCode::FORM_INVALID;
            $message = Yii::t('api', 'Mật khẩu cũ không đúng, vui lòng thử lại!');
        }

        if ($errCode != ApiResponseCode::SUCCESS) {
            return ApiHelper::formatResponse(
                $errCode,
                $data,
                $message
            );
        }

        // Xu ly luu pass
        $webSub->setPassword($newPassword);
        $webSub->save(false);

        $message = Yii::t('api', 'Đổi mật khẩu thành công!');

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            [],
            $message
        );
    }


    /**
     * Tra ve thong tin profile cua nguoi dung
     * @return array
     */
    public function actionProfile()
    {
        $checkLogin = self::checkTokenFromHeader();
//        if (is_array($checkLogin) && isset($checkLogin['errorCode'])) {
//            return $checkLogin;
//        }

        $webUser = $checkLogin;

        if (!$webUser) {
            return ApiHelper::formatResponse(
                ApiResponseCode::NO_DATA,
                [],
                Yii::t('api', 'Thuê bao không tồn tại trên hệ thống!')
            );
        }

        $data = [
            'msisdn' => $webUser->MSISDN,
            'username' => $webUser->USERNAME,
            'fullname' => $webUser->FULLNAME,
            'avatar' => $webUser->AVATAR,
//            'total_spin' => ($webUser->PAID_SPIN + $webUser->FREE_SPIN),
        ];
        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $data
        );
    }
}