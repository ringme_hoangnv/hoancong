<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use Yii;


/**
 * FortuneTelling controller
 * @return
 */
class FortuneTellingController extends ApiController
{
    public function actionFingerprint()
    {
        $type = Yii::$app->request->get('type', 'female');
        if (!in_array($type, ['male', 'female']))
            $type = 'female';

        $data = null;
        $errorCode = ApiResponseCode::NO_DATA;

        // ShowFull content
        $contentId = 'fingerprint';
        $contentStatusCode = ApiHelper::getViewContentStatusCode($contentId);
        $showFull = $contentStatusCode == 1 || $contentStatusCode == 2;

        switch ($contentStatusCode) {
            case 1:
                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                ];
                $errorCode = ApiResponseCode::SUCCESS;
                break;
            case 0:
                $registerMessage = '';

                $packages = \frontend\models\Pkg::getActiveSubPkg();
                foreach ($packages as $package){
                    $m1 = Yii::t('frontend', '{fee} Ks/{day} days', [
                    'fee' => number_format($package->PRICE),
                    'day' => $package->DAY_ADD,
                    ]); 
                    $m2 = Yii::t('frontend', 'FREE First week');
        
                    $registerMessage = $m1.'.'.$m2;
                }

                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                    'registerMessage' => $registerMessage,
                ];

                $errorCode = ApiResponseCode::PERMISSION_IS_REQUIRED;
                $message = $this->renderPartial('viewMoreContent', [
                    'contentStatusCode' => $contentStatusCode,
                    'contentId' => $contentId,
                ]);
                break;
            case 401:
                $data = null;
                $errorCode = ApiResponseCode::LOGIN_IS_REQUIRED;
                break;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionHandMole()
    {

        // ShowFull content
        $contentId = 'hand_mole';
        $contentStatusCode = ApiHelper::getViewContentStatusCode($contentId);
        $showFull = $contentStatusCode == 1 || $contentStatusCode == 2;


        $data = null;
        $errorCode = ApiResponseCode::NO_DATA;

        switch ($contentStatusCode) {
            case 1:
                $data = [
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                ];
                $errorCode = ApiResponseCode::SUCCESS;
                break;
            case 0:
                $registerMessage = '';

                $packages = \frontend\models\Pkg::getActiveSubPkg();
                foreach ($packages as $package){
                    $m1 = Yii::t('frontend', '{fee} Ks/{day} days', [
                    'fee' => number_format($package->PRICE),
                    'day' => $package->DAY_ADD,
                    ]); 
                    $m2 = Yii::t('frontend', 'FREE First week');
        
                    $registerMessage = $m1.'.'.$m2;
                }

                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                    'registerMessage' => $registerMessage,
                ];
                $errorCode = ApiResponseCode::PERMISSION_IS_REQUIRED;
                $message = $this->renderPartial('viewMoreContent', [
                    'contentStatusCode' => $contentStatusCode,
                    'contentId' => $contentId,
                ]);
                break;
            case 401:
                $data = null;
                $errorCode = ApiResponseCode::LOGIN_IS_REQUIRED;
                break;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionFacialMole()
    {

        // ShowFull content
        $contentStatusCode = ApiHelper::getViewContentStatusCode('facial_mole');
        $showFull = $contentStatusCode == 1 || $contentStatusCode == 2;


        $data = null;
        $errorCode = ApiResponseCode::NO_DATA;

        switch ($contentStatusCode) {
            case 1:
                $data = [
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                ];
                $errorCode = ApiResponseCode::SUCCESS;
                break;
            case 0:
                $registerMessage = '';

                $packages = \frontend\models\Pkg::getActiveSubPkg();
                foreach ($packages as $package){
                    $m1 = Yii::t('frontend', '{fee} Ks/{day} days', [
                    'fee' => number_format($package->PRICE),
                    'day' => $package->DAY_ADD,
                    ]); 
                    $m2 = Yii::t('frontend', 'FREE First week');
        
                    $registerMessage = $m1.'.'.$m2;
                }

                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                    'registerMessage' => $registerMessage,
                ];
                $errorCode = ApiResponseCode::PERMISSION_IS_REQUIRED;
                $message = $this->renderPartial('viewMoreContent', [
                    'contentStatusCode' => $contentStatusCode,
                    'contentId' => $contentId,
                ]);
                break;
            case 401:
                $data = null;
                $errorCode = ApiResponseCode::LOGIN_IS_REQUIRED;
                break;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionFemaleBodyMole()
    {

        // ShowFull content
        $contentStatusCode = ApiHelper::getViewContentStatusCode('female_mole_body');
        $showFull = $contentStatusCode == 1 || $contentStatusCode == 2;


        $data = null;
        $errorCode = ApiResponseCode::NO_DATA;
        switch ($contentStatusCode) {
            case 1:
                $data = [
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                ];
                $errorCode = ApiResponseCode::SUCCESS;
                break;
            case 0:
                $registerMessage = '';

                $packages = \frontend\models\Pkg::getActiveSubPkg();
                foreach ($packages as $package){
                    $m1 = Yii::t('frontend', '{fee} Ks/{day} days', [
                    'fee' => number_format($package->PRICE),
                    'day' => $package->DAY_ADD,
                    ]); 
                    $m2 = Yii::t('frontend', 'FREE First week');
        
                    $registerMessage = $m1.'.'.$m2;
                }

                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                    'registerMessage' => $registerMessage,
                ];
                $errorCode = ApiResponseCode::PERMISSION_IS_REQUIRED;
                $message = $this->renderPartial('viewMoreContent', [
                    'contentStatusCode' => $contentStatusCode,
                    'contentId' => $contentId,
                ]);
                break;
            case 401:
                $data = null;
                $errorCode = ApiResponseCode::LOGIN_IS_REQUIRED;
                break;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionMaleBodyMole()
    {

        // ShowFull content
        $contentId = 'male_mole_body';
        $contentStatusCode = ApiHelper::getViewContentStatusCode($contentId);
        $showFull = $contentStatusCode == 1 || $contentStatusCode == 2;


        $data = null;
        $errorCode = ApiResponseCode::NO_DATA;
        switch ($contentStatusCode) {
            case 1:
                $data = [
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                ];
                $errorCode = ApiResponseCode::SUCCESS;
                break;
            case 0:
                $registerMessage = '';

                $packages = \frontend\models\Pkg::getActiveSubPkg();
                foreach ($packages as $package){
                    $m1 = Yii::t('frontend', '{fee} Ks/{day} days', [
                    'fee' => number_format($package->PRICE),
                    'day' => $package->DAY_ADD,
                    ]); 
                    $m2 = Yii::t('frontend', 'FREE First week');
        
                    $registerMessage = $m1.'.'.$m2;
                }

                $data = [
                    'type' => $type,
                    'showFull' => $showFull,
                    'contentStatusCode' => $contentStatusCode,
                    'registerMessage' => $registerMessage,
                ];
                $errorCode = ApiResponseCode::PERMISSION_IS_REQUIRED;
                $message = $this->renderPartial('viewMoreContent', [
                    'contentStatusCode' => $contentStatusCode,
                    'contentId' => $contentId,
                ]);
                break;
            case 401:
                $data = null;
                $errorCode = ApiResponseCode::LOGIN_IS_REQUIRED;
                break;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }
}