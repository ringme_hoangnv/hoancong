<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use api_public\modules\v1\models\ApiLoginAuth;
use common\core\MpsConnector;
use common\helpers\Helpers;
use common\models\ServiceSubscriberBase;
use frontend\models\Charge;
use frontend\models\GetOtpForm;
use frontend\models\LoginForm;
use frontend\models\OneTimePassword;
use frontend\models\PasswordResetRequestForm;
use frontend\models\Pkg;
use frontend\models\ResetPasswordForm;
use frontend\models\ServiceSubscriber;
use frontend\models\SignupForm;
use frontend\models\Subscriber;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

class SubscriberController extends ApiController
{
    public function actions()
    {

        return [
            'fast-login' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new SignupForm();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $msisdn = Helpers::convertMsisdn($model->username);

                $sub = new Subscriber();
                $sub->MSISDN = $msisdn;
                $sub->USERNAME = $msisdn;
                $sub->REG_TIME = date('Y-m-d H:i:s');
                $sub->LAST_LOGIN_TIME = date('Y-m-d H:i:s');
                $sub->STATUS = 1;

                $sub->LANGUAGE = Yii::$app->language;
                $sub->setPassword($model->password);
                $sub->generatePasswordResetToken();
                $sub->save();

                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Sign up successfully!'));
                Yii::$app->session->set('msisdn', $sub->MSISDN);
                Yii::$app->user->login($sub);
                return $this->goHome();
            }
        }

        return $this->render('signUp', [
            'model' => $model
        ]);
    }

    public function actionIndex()
    {
//        if (Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
        $errorCode = ApiResponseCode::SUCCESS;
        $message = "";

        $token = trim(Yii::$app->request->headers->get('token'));
        $loginAuth = ApiLoginAuth::findOne(['TOKEN' => $token]);

//        $msisdn = ApiHelper::convertMsisdn(Yii::$app->session->get('msisdn', null));

        $serviceSub = ServiceSubscriber::findOne(['MSISDN' => $loginAuth->MSISDN]);
        $packageSubList = Pkg::getActiveSubPkg();

        $label = Yii::t('frontend', 'Register {service_name} service to view horoscope and forturn telling content and get your fortune every day', [
            'service_name' => Yii::$app->params['service_name']
        ]);

        if (!$serviceSub || !$serviceSub->isActivatingService()) {
            $modalTitle = Yii::t('frontend', 'Đăng ký dịch vụ');
            $btnName = Yii::t('frontend', 'Đăng ký');

            $packageData = [];
            foreach ($packageSubList as $package) {
                $text = Yii::t('frontend', '{package_name} (Fee: {pkg_fee} {money_unit}/{per_day} days)', [
                    'package_name' => $package->NAME,
                    'pkg_fee' => $package->PRICE,
                    'per_day' => $package->DAY_ADD,
                    'money_unit' => Yii::$app->params['money_unit'],
                    'pkg_id' => $package->PKG_ID,
                ]);

                $packageData[] = [
                    'package_name' => $package->NAME,
                    'pkg_fee' => $package->PRICE,
                    'per_day' => $package->DAY_ADD,
                    'money_unit' => Yii::$app->params['money_unit'],
                    'pkg_id' => $package->PKG_ID,
                    'text' => $text
                ];
            }

            $data = [
                "isSub" => "false",
                "label" => $label,
                "title" => $modalTitle,
                "btnName" => $btnName,
                "packageData" => $packageData,
                "ads" => Yii::t('frontend', 'FREE First week'),
            ];
        } else {
            $modalTitle = Yii::t('frontend', 'Hủy dịch vụ');
            $btnName = Yii::t('frontend', 'Hủy dịch vụ');

            $data = [
                "isSub" => "true",
                "label" => $label,
                "title" => $modalTitle,
                "btnName" => $btnName,
                "serviceName" => Yii::t('frontend', 'Bạn đang sử dụng dịch vụ {service_name}', [
                    'service_name' => Yii::$app->params['service_name']]),
                "registerDate" => date('d/m/Y', strtotime($serviceSub->REGISTER_DATE)),
                "expiredDate" => date('d/m/Y', strtotime($serviceSub->EXPIRE_DATE)),
            ];
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            //Yii::$app->session->setFlash('info', Yii::t('frontend', 'Bạn đang đăng nhập!'));
            return $this->goHome();
        }
        $this->layout = 'fullWidthLayout';
        $source = Yii::$app->request->get('source', Yii::$app->session->get('login_source', null));
        if ($source)
            Yii::$app->session->set('login_source', $source);

        $model = new LoginForm();
        $model->username = Yii::$app->request->get('msisdn', null);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            Yii::$app->session->set('rememberMe', $model->rememberMe);
            if ($model->login()) {
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Đăng nhập thành công!'));

                $sub = Yii::$app->user->identity->getServiceSubscriber();
                if (!$sub || ($sub && !$sub->isActivatingService())) {

                    return $this->redirect(Url::to(['subscriber/index']));
                }

                return $this->goBack();
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Lay lai mat khau
     * @return string|\yii\web\Response
     */
    public function actionResetPass($token)
    {
        $token = Yii::$app->request->get('token');
        if (!$token) {
            Yii::$app->session->setFlash('warning', Yii::t('frontend', 'Bạn không có quyền truy cập trang này'));
            return $this->goHome();
        } elseif (!Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('warning', Yii::t('frontend', 'Bạn đang đăng nhập! Vui lòng thoát ra để thực hiện tính năng này!'));
            return $this->goHome();
        }

        $user = Subscriber::findByResetPassToken($token);
        if (!$user) {
            Yii::$app->session->setFlash('warning', Yii::t('frontend', 'Bạn không có quyền truy cập trang này'));
            return $this->goHome();
        }

        $model = new ResetPasswordForm($token);
        $model->setUser($user);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {

                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Mật khẩu của bạn đã được khôi phục thành công!'));

                return $this->redirect(Url::to(['subscriber/login']));
            }
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $data = Yii::t('frontend', 'Đăng xuất thành công!');

        $errorCode = ApiResponseCode::SUCCESS;

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionChangePass()
    {
        //khanhlh
        $errorCode = ApiResponseCode::NO_DATA;
        $cache = Yii::$app->cache;
        $requestData = json_decode(Yii::$app->request->getRawBody());

        $oldPass = $requestData->old_password;
        $newPass = $requestData->new_pass;
        $password = $requestData->password;
        $usingOTP = $requestData->using_otp;

        $user = self::checkTokenFromHeader();

        $user->setPassword($password);
        $user->save();
        $data = Yii::t('frontend', 'Đổi mật khẩu thành công!');

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    /**
     * Trang quen mat khau
     */
    public function actionForgotPass()
    {

        $model = new PasswordResetRequestForm();
        $sendResult = null;
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $sendResult = $model->sendEmail();
                //Yii::$app->session->setFlash('success', Yii::t('frontend', 'Vui lòng kiểm tra email để khôi phục mật khẩu của bạn!'));

            }
        }

        return $this->render('forgotPass', [
            'model' => $model,
            'sendResult' => $sendResult,
        ]);
    }


    /**
     * Xu ly sau khi dang nhap MXH thanh cong
     * @param $client
     * @return \yii\web\Response
     */
    public function oAuthSuccess($client)
    {
        // get user data from client
        $attributes = $client->getUserAttributes();

        $oAuthId = null;
        $fullName = '';
        $email = '';
        $avartarUrl = '';
        if ($client instanceof \yii\authclient\clients\Google) {
            $oAuthId = $attributes['id'];
            $fullName = $attributes['displayName'];
            $email = $attributes['emails'][0]['value'];
            $avartarUrl = str_replace('sz=50', 'sz=200', $attributes['image']['url']);
        } elseif ($client instanceof \yii\authclient\clients\Facebook) {
            $oAuthId = $attributes['id'];
            $fullName = $attributes['name'];
            $email = $attributes['email'];
            $avartarUrl = $attributes['picture']['data']['url'];
        }

        // Dang nhap thanh cong, co id
        if ($oAuthId) {
            $subscriber = Subscriber::getByOauthId($oAuthId);

            if (!$subscriber) {
                // Chua co user
                $newSub = new Subscriber();
                $newSub->insertUserSocial($oAuthId, $fullName, $email, $avartarUrl);

                Yii::$app->user->login($newSub);
                Yii::$app->getSession()->set("oauth_id", $newSub->oauth_id);
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Đăng nhập thành công!'));

                return $this->goBack();
            } else {
                Yii::$app->user->login($subscriber);
                Yii::$app->getSession()->set("oauth_id", $subscriber->oauth_id);
                Yii::$app->session->setFlash('success', Yii::t('frontend', 'Đăng nhập thành công!'));
            }
        }

    }

    public function actionTransactionHistory()
    {

//        if (Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//        $this->layout = 'fullWidthLayout';

//        $msisdn = Yii::$app->user->identity->MSISDN;
        $webSub = ApiController::checkTokenFromHeader();

        $msisdn = $webSub->MSISDN;

        $currentPage = Yii::$app->request->get('page', 1);

        $startDate = date('Y-m-d', strtotime(str_replace('/', '-', Yii::$app->request->get('start_date', date('01/m/Y')))));
        $endDate = date('Y-m-d', strtotime(str_replace('/', '-', Yii::$app->request->get('end_date', date('d/m/Y')))));

        // $data is not found in cache, calculate it from scratch

        $query = Charge::find()
            ->alias('c')
            ->where(['c.MSISDN' => $msisdn])
            ->orderBy('c.CHARGE_ID DESC');

        if ($startDate && $endDate) {
            $query->andWhere([
                'BETWEEN', 'CHARGE_TIME', $startDate, $endDate
            ]);
        }

        $countQuery = clone $query;
        // get the total number of articles (but do not fetch the article data yet)
        $count = $countQuery->count();

        // create a pagination object with the total count
        $pages = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 10,
        ]);

        // limit the query using the pagination and retrieve the articles
        $listPager = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $data = [
            'listPager' => $listPager,
            'pages' => $pages,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];

        $data = $this->renderPartial('transactionHistory', $data);

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionGetOtp()
    {
        $this->layout = false;
        $isFinish = false;
        $message = '';
        $model = new GetOtpForm();
        $model->msisdn = Yii::$app->session->get('msisdn');
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                // Luu OTP
                $otpStr = OneTimePassword::insertNewOtp($model->msisdn);

                // Gui tin nhan SMS
//                $content = Yii::t('frontend', SystemSetting::getConfigByKey('SMS_GET_OTP_MSG'), [
                $content = Yii::t('frontend', '{otp} la mat khau cua quy khach de su dung tren wapsite {url} Mat khau se het han sau {expired_minute} phut', [
                    'otp' => $otpStr,
                    'expired_minute' => Yii::$app->params['otp_expired_minutes'],
                    'url' => Yii::$app->params['wapsite_url'],

                ]);
                $send = MpsConnector::sendMt(Yii::$app->params['sms_shortcode'], $model->msisdn, $content);

                // Ket thuc 
                $isFinish = true;
                $message = Yii::t('frontend', 'Mã OTP đã được gửi về số điện thoại của Quý Khách. Vui lòng kiểm tra tin nhắn SMS! Xin cảm ơn!');
            }
        }

        return $this->render('getOtp', [
            'getOtpModal' => $model,
            'isFinish' => $isFinish,
            'message' => $message,
        ]);
    }

    public function actionGetNewPass()
    {
        $this->layout = 'fullWidthLayout';
        $isFinish = false;
        $message = '';
        $model = new GetOtpForm();
        $model->msisdn = Yii::$app->session->get('msisdn', Yii::$app->request->get('msisdn', null));
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                //$newPass = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvxyz'), 0, 6);
                $newPass = substr(str_shuffle('0123456789'), 0, 6);

                // update pass
                $webSub = Subscriber::findOrCreateNew($model->msisdn);
                $webSub->setPassword($newPass);
                $webSub->save();

                // Gui tin nhan SMS
                $content = Yii::t('frontend', '{password} is your password to use on website {url}', [
                    'password' => $newPass,
                    'url' => Yii::$app->params['wapsite_url'],

                ]);
                $send = MpsConnector::sendMt(Yii::$app->params['sms_shortcode'], $model->msisdn, $content);

                // Ket thuc
                $isFinish = true;
                $message = Yii::t('frontend', 'The new password has already sent to your phone number. Please check your SMS. Thank you!');
                Yii::$app->session->setFlash('info', $message);
                return $this->redirect(['subscriber/login', 'msisdn' => $model->msisdn]);
            }
        }

        return $this->render('getNewPass', [
            'getOtpModal' => $model,
            'isFinish' => $isFinish,
            'message' => $message,
        ]);
    }

    public function actionBirthday()
    {
        $user = self::checkTokenFromHeader();

        $serviceSub = ServiceSubscriberBase::findOne([
            'MSISDN' => $user->MSISDN
        ]);

        if ($serviceSub->BIRTHDAY) {
            $bdtimestamp = strtotime($serviceSub->BIRTHDAY);
            $day = date('d', $bdtimestamp);
            $month = date('m', $bdtimestamp);
            $year = date('Y', $bdtimestamp);
            $hour = date('H', $bdtimestamp);
            $minute = date('i', $bdtimestamp);

            $errorCode = ApiResponseCode::SUCCESS;

            $data = [
                "day" => $day,
                "month" => $month,
                "year" => $year,
                "hour" => $hour,
                "minute" => $minute,
            ];
        } else {
            $errorCode = ApiResponseCode::NO_DATA;
            $data = null;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionUpdateProfile()
    {
        $requestData = json_decode(Yii::$app->request->getRawBody());

        $day = $requestData->day;
        $month = $requestData->month;
        $year = $requestData->year;

        $hour = $requestData->hour;
        $minute = $requestData->minute;

        if ($hour !== null)
            $hour = str_pad($hour, 2, "0", STR_PAD_LEFT);
        if ($minute !== null)
            $minute = str_pad($minute, 2, "0", STR_PAD_LEFT);

        $hourValid = true;

        $errorMsg = ApiResponseCode::NO_DATA;
        $data = null;

        if (!checkdate($month, $day, $year)) {
            $errorMsg = Yii::t('frontend', 'Ngày sinh không hợp lệ!');
        } elseif (!preg_match("/^(?:2[0-3]|[01][0-9]|[0-9]):[0-5][0-9]$/", $hour . ':' . $minute)) {
            $hourValid = false;
            $errorMsg = Yii::t('frontend', 'Giờ sinh không hợp lệ!');
        } else {
            $user = self::checkTokenFromHeader();

            $serviceSub = ServiceSubscriberBase::findOne([
                'MSISDN' => $user->MSISDN
            ]);
//            $user = Yii::$app->user->identity;
//            $serviceSub = $user->getServiceSubscriber();
            if (!$serviceSub) {
                $serviceSub = new ServiceSubscriber();
                $serviceSub->PKG_ID = null;
                $serviceSub->MSISDN = $user->MSISDN;
                $serviceSub->STATUS = '0';
            }

            $serviceSub->BIRTHDAY = $year . '-' . $month . '-' . $day;
            if ($hourValid) {
                $serviceSub->BIRTHDAY = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute;
            }
            $serviceSub->save(false);
            $data = $serviceSub->BIRTHDAY;

//            Yii::$app->session->setFlash('success', Yii::t('frontend', 'Cập nhật ngày sinh thành công!'));
//            return $this->redirect(Url::to(['subscriber/update-profile']));
        }
        $message = $errorMsg;

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $data,
            $message
        );
    }
}
