<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use api_public\modules\v1\models\BtsPlanSearch;
use backend\models\BtsPlan;
use backend\models\BtsPlanItem;
use backend\models\BtsPlanItemImage;
use Yii;

class PlanController extends ApiController
{
    public function actionList()
    {
        $errorCode = ApiResponseCode::SUCCESS;

        $searchModel = new BtsPlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->getPagination()->setPage(Yii::$app->request->get('page', 1) - 1);
        $planList = $dataProvider->getModels();

        return ApiHelper::formatResponse(
            $errorCode,
            $planList
        );
    }

    public function actionView()
    {


        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }

    public function actionApprove($plan_id)
    {
        if ($this->user->user_type != 'ho') {
            return ApiHelper::formatResponse(
                ApiResponseCode::PERMISSION_IS_REQUIRED
            );
        }

        $plan = BtsPlan::findOne([
            'id' => $plan_id
        ]);

        if ($plan) {

            $plan->status = 1; // Duyet plan
            if ($plan->save())
                $errorCode = ApiResponseCode::SUCCESS;
            else
                $errorCode = ApiResponseCode::SYSTEM_ERROR;
        }
        return ApiHelper::formatResponse(
            $errorCode
        );
    }

    public function actionDisapprove($plan_id)
    {
        if ($this->user->user_type != 'ho') {
            return ApiHelper::formatResponse(
                ApiResponseCode::PERMISSION_IS_REQUIRED
            );
        }

        $plan = BtsPlan::findOne([
            'id' => $plan_id
        ]);

        if ($plan) {

            $plan->status = 0; // Huy duyet plan
            if ($plan->save())
                $errorCode = ApiResponseCode::SUCCESS;
            else
                $errorCode = ApiResponseCode::SYSTEM_ERROR;
        }
        return ApiHelper::formatResponse(
            $errorCode
        );
    }

    /**
     * Lay ra cac item cua plan
     * @param $plan_id
     * @return array
     */
    public function actionItems($plan_id)
    {
        /*
        $planItems = BtsPlanItem::find()->all();
        foreach ($planItems as $pe) {
            BtsPlanItem::updateStatus($pe->plan_id, $pe->item_id);
        }
        die;
        */
        $errorCode = ApiResponseCode::SUCCESS;

        $plan = $this->findModel($plan_id);

        if ($plan) {
            $items = $plan->items;
            $data = [];
            foreach ($items as $item) {

                $imagesArr = [];
                $images = BtsPlanItemImage::find()
                    ->where([
                        'plan_id' => $plan_id,
                        'item_id' => $item->id,
                    ])
                    ->orderBy('updated_at DESC')
                    ->all();
                if (count($images)) {

                    foreach ($images as $image) {
                        $imagesArr[] = [
                            'id' => $image->id,
                            'image_path' => $image->getImagePathUrl(),
                            'plan_id' => $image->plan_id,
                            'item_id' => $image->item_id,
                            'status' => $image->status,
                            'created_at' => $image->created_at,
                            'updated_at' => $image->updated_at,
                            'approved_at' => $image->approved_at,
                            'approved_by' => ($image->approvedBy)? $image->approvedBy->username: null,
                        ];
                    }
                }

                $data[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'status' => $item->status,
                    'note' => $item->note,
                    'image_num' => $item->image_num,
                    'image_path' => $item->getImagePathUrl(),
                    'category_id' => $item->category_id,
                    'category_name' => ($item->category)? $item->category->name: null,
                    'images' => $imagesArr,
                ];
            }

            return ApiHelper::formatResponse(
                $errorCode,
                $data
            );
        } else {
            return ApiHelper::formatResponse(
                ApiResponseCode::NO_DATA
            );
        }

    }

    public function actionItemImages($plan_id)
    {
        $errorCode = ApiResponseCode::SUCCESS;
        $itemId = Yii::$app->request->get('item_id', null);

        $plan = $this->findModel($plan_id);

        if ($plan) {
            $images = BtsPlanItemImage::find()
                ->where([
                    'plan_id' => $plan_id,
                ])
                ->orderBy('item_id ASC, updated_at DESC')
                ;
            if ($itemId) {
                $images->andWhere(['item_id' => $itemId]);
            }
            $images = $images->all();
            $imagesArr = [];
            if (count($images)) {

                foreach ($images as $image) {
                    $imagesArr[] = [
                        'id' => $image->id,
                        'image_path' => $image->getImagePathUrl(),
                        'plan_id' => $image->plan_id,
                        'item_id' => $image->item_id,
                        'status' => $image->status,
                        'note' => $image->note,
                        'created_at' => $image->created_at,
                        'updated_at' => $image->updated_at,
                        'approved_at' => $image->approved_at,
                        'approved_by' => ($image->approvedBy)? $image->approvedBy->username: null,
                    ];
                }
            }

            return ApiHelper::formatResponse(
                $errorCode,
                $imagesArr
            );
        } else {
            return ApiHelper::formatResponse(
                ApiResponseCode::NO_DATA
            );
        }

    }


    protected function findModel($id)
    {
        $query = BtsPlan::find()
            ->where(['id' => $id])
            ;

        if ($this->user->user_type == 'branch') {
            $query->andWhere(['created_by' => $this->user->id]);
        } else if ($this->user->user_type == 'partner') {
            $query->andWhere(['partner_id' => $this->user->partner_id]);
        }

        $model = $query->one();

        return $model;
    }
}