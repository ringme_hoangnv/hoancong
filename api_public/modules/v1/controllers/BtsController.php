<?php
/**
 * Created by PhpStorm.
 * User: subob
 * Date: 8/13/21
 * Time: 20:52
 */

namespace api_public\modules\v1\controllers;


use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use backend\models\BtsStation;
use backend\models\ItemCategory;
use backend\models\ItemCategoryType;
use backend\models\ItemGroup;

class BtsController extends ApiController
{
    /**
     * List danh sach bts theo user login
     */
    public function actionList() {
        if ($this->user->user_type == 'partner') {
            // Lay ra cac BTS cua partner
            $btsList = BtsStation::find()
                ->select('bts.bts_code, bts.bts_name, p.item_category_id')
                ->alias('bts')
                ->leftJoin('bts_plan p', 'bts.bts_code = p.bts_code')
                ->where([
                    'p.partner_id' => $this->user->partner_id,
                    'bts.status' => 1,
                ])
                ->asArray()
                ->all();
        } elseif ($this->user->user_type == 'ho'){
            // Lay ra cac BTS cua ho
            $btsList = BtsStation::find()
                ->select('bts.bts_code, bts.bts_name, p.item_category_id')
                ->alias('bts')
                ->leftJoin('bts_plan p', 'bts.bts_code = p.bts_code')
                ->where([
                    'bts.status' => 1,
                ])
                ->asArray()
                ->all();
        }
        // user --> partner --> plan --> bts
        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $btsList
        );
    }


}