<?php
/**
 * Created by PhpStorm.
 * User: subob
 * Date: 8/13/21
 * Time: 20:52
 */

namespace api_public\modules\v1\controllers;


use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use backend\models\BtsPlan;
use backend\models\BtsPlanItem;
use backend\models\BtsStation;
use backend\models\ItemCategory;
use backend\models\ItemCategoryType;
use backend\models\ItemGroup;
use yii;

class ItemController extends ApiController
{
    public function actionSearch() {
        $btsCode = Yii::$app->request->get('bts_code', '');
        $categoryId = Yii::$app->request->get('category_id', null);
        $groupId = Yii::$app->request->get('group_id', null);

        $bts = BtsStation::findOne(['bts_code' => $btsCode]);

        if (!$bts) {
            return ApiHelper::formatResponse(
                ApiResponseCode::INVALID_PARAMS,
                [],
                Yii::t('backend', 'Bts code not found!')
            );
        }
        // Lay ra plan
        $planIds = BtsPlan::find()
            ->select('id')
            ->where([
                'bts_code' => $btsCode,
                'status' => 1,
                //['>', 'end_at', date('Y-m-d H:i:s')]
            ])
            ;

        if ($this->user->user_type == 'partner') {
            $planIds->andWhere(['partner_id' => $this->user->partner_id]);
        }
        $planIds = $planIds->all();
        $planIdsArr = [];
        if (count($planIds)) {
            $planIdsArr = yii\helpers\ArrayHelper::map($planIds, 'id', 'id');
        } else {

            return ApiHelper::formatResponse(
                ApiResponseCode::NO_DATA,
                [],
                Yii::t('backend', 'Items not found!!')
            );
        }

        $planItems = BtsPlanItem::find()
            ->alias('pi')
            ->select('pi.item_id, pi.plan_id,pi.status, pi.image_num, i.name, i.note, i.category_id, i.group_id, c.name as category_name, g.name as group_name')
            ->leftJoin('bts_item i', 'i.id = pi.item_id')
            ->leftJoin('item_group g', 'i.group_id = g.id')
            ->leftJoin('item_category c', 'i.category_id = c.id')
            ->where([
                'pi.plan_id' => $planIdsArr,
                'i.status' => 1,
            ]);
        $planItems->andFilterWhere(['i.category_id' => $categoryId])
            ->andFilterWhere(['i.group_id' => $groupId]);


        $planItems = $planItems->asArray()
            ->all();

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $planItems
        );
    }

    public function actionCategoryType() {
        $btsCode = Yii::$app->request->get('bts_code', '');

        // Lay ra cac category_id
        $cateIds = BtsPlan::find()
            ->select('item_category_id')
            ->where([
                'status' => 1,
                //['>', 'end_at', date('Y-m-d H:i:s')]
            ])
        ;
        $cateIds->andFilterWhere(['bts_code' => $btsCode]);

        if ($this->user->user_type == 'partner') {
            $cateIds->andWhere(['partner_id' => $this->user->partner_id]);
        }

        $cateIds = $cateIds->all();

        $list  = ItemCategoryType::find()
            ->alias('ct')
            ->leftJoin('item_category c', 'c.category_type_id = ct.id')
            ->select('ct.id, ct.name')
            ->andFilterWhere(['c.id' => yii\helpers\ArrayHelper::map($cateIds, 'item_category_id', 'item_category_id')])
            ->orderBy('ct.name asc')
            ->asArray()
            ->all();

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $list
        );
    }

    public function actionCategory() {

        $cateTypeId = Yii::$app->request->get('category_type_id', 0);
        $categoryList  = ItemCategory::find()
            ->alias('c')
            ->select('c.id, c.name, c.category_type_id, ct.name category_type_name')
            ->leftJoin('item_category_type ct', 'c.category_type_id = ct.id')
            ->orderBy('c.name asc')
            ;
        if ($cateTypeId) {
            $categoryList->andWhere(['c.category_type_id' => $cateTypeId]);
        }
        $categoryList = $categoryList
            ->asArray()
            ->all();

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $categoryList
        );
    }

    public function actionGroup() {
        $cateId = Yii::$app->request->get('category_id', 0);
        $list = ItemGroup::find()
            ->alias('g')
            ->select('g.id, g.name, g.category_id, c.name as category_name')
            ->leftJoin('item_category c', 'g.category_id = c.id')
            ->orderBy('g.name asc')
            ;

        if ($cateId) {
            $list->andWhere(['g.category_id' => $cateId]);
        }

        $list = $list
            ->asArray()
            ->all();
        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $list
        );
    }
}