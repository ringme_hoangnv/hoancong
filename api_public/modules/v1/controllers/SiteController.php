<?php


namespace api_public\modules\v1\controllers;


use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use Yii;

class SiteController extends ApiController
{
    public function actionTerms()
    {
        $errorCode = ApiResponseCode::SUCCESS;

        if (Yii::$app->language == 'en') {
            $data = $this->renderPartial('termsEn');
        } else {
            $data = $this->renderPartial('termsMm');
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }
}