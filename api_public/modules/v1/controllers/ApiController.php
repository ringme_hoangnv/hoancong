<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use api_public\modules\v1\models\ApiLoginAuth;
use api_public\modules\v1\models\LoginForm;
use api_public\modules\v1\models\Subscriber;
use backend\models\User;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class ApiController extends Controller
{
    private $subscriber;
    private $authorizationCode;
    protected $permission;
    protected $requiredAuth = false;
    protected $requiredPost = false;
    protected $permissionAction;
    public $currentLang = 'en';

    public $user= null;
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'common\components\captcha\BackendCaptcha',
                'transparent' => false,
                'foreColor' => 0x800080,
                'backColor' => 0xffffff,
                'minLength' => 2,
                'maxLength' => 3,
                'offset' => -2,
                'chars' => 'abcdefhjkmnpqrstuxyz2345678',
                'libfont' => [
                    0 => '@backend/web/css/fonts/captcha/vavobi.ttf',
                    1 => '@backend/web/css/fonts/captcha/momtype.ttf',
                    2 => '@backend/web/css/fonts/captcha/captcha4.ttf'
                ]
            ],
        ];
    }

    public function actionError()
    {
        return ApiHelper::errorResponse();
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function beforeAction($action)
    {
        $notAuthActions = ['authorization', 'captcha', 'translation', 'login', 'logout', 'error'];
        // validate authorization code
        Yii::$app->response->format = Response::FORMAT_JSON;
        $authorization_code = '';

        if (!in_array(Yii::$app->controller->action->id, $notAuthActions) && $this->requiredAuth && !$authorization_code) {
            echo json_encode(ApiHelper::formatResponse(
                ApiResponseCode::AUTHORIZATION_CODE_REQUIRED, []
            ));
            die;
        }

        $lang = trim(Yii::$app->request->headers->get('language'));
        $lang = ($lang && in_array($lang, Yii::$app->params['language_support'])) ? $lang : Yii::$app->params['default_language'];

        Yii::$app->session->set('language', $lang);
        Yii::$app->language = $lang;

        if (!in_array(Yii::$app->controller->action->id, $notAuthActions) && $this->requiredAuth) {
            if (!$apiClient) {
                echo json_encode(ApiHelper::formatResponse(
                    ApiResponseCode::AUTHORIZATION_CODE_REQUIRED, []
                ));
                die;
            }

            Yii::$app->session->set('api_client', $apiClient);
        }

        $this->authorizationCode = $authorization_code;

        // lay token tu header tuong auth_code
        // Truy van bang api_login_auth where token = ? de lay msisdn
        // Co msisdn --> lay ra subscriber o bang web_subscriber
        // set session Yii::$app->session->set('msisdn', $sub->MSISDN)
        //khanhlh

        $checkLogin = $this->checkTokenFromHeader();

        if ($checkLogin && $checkLogin->username) {
            Yii::$app->session->set('msisdn', $checkLogin->username);

        } else if (!in_array(Yii::$app->controller->action->id, $notAuthActions)) {
            echo json_encode(ApiHelper::formatResponse(
                ApiResponseCode::LOGIN_IS_REQUIRED, []
            ));
            die;
        }
        return parent::beforeAction($action);
    }

    public function actionChangeLanguage($lang = 'en')
    {
        $lang = Yii::$app->request->get('lang', 'en');
        if (in_array($lang, Yii::$app->params['language_support'])) {

            Yii::$app->session->set('language', $lang);
            Yii::$app->language = $lang;

            $errCode = ApiResponseCode::SUCCESS;

        } else {
            $errCode = ApiResponseCode::INVALID_PARAMS;
        }

        return ApiHelper::formatResponse(
            $errCode,
            []
        );
    }

    /**
     * Xu ly logout
     */
    public function actionLogout()
    {
        $token = trim(Yii::$app->request->get('token', null));
        $logoutAll = intval(Yii::$app->request->get('logout_all', 0));

        $apiAuth = ApiLoginAuth::findOne(['token' => $token]);
        $msisdn = $apiAuth->msisdn;

        if (!$token || !$apiAuth) {
            return ApiHelper::formatResponse(
                ApiResponseCode::INVALID_PARAMS
            );
        }
        if ($logoutAll == 1) {
            // Xoa token cu het han
            ApiLoginAuth::deleteAll(['msisdn' => $msisdn]);
        } else {
            $apiAuth->delete();
        }
        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            []
        );
    }

    /**
     * Ham xu ly login
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionLogin()
    {
        $requestData = json_decode(Yii::$app->request->getRawBody());

        $username = $requestData->username;
        $password = $requestData->password;
        $captcha = $requestData->captcha;

        $message = '';
        $data = [];
        $webUser = null;

        $loginForm = new LoginForm();
        $loginForm->load([
            'username' => $username,
            'password' => $password,
            'captcha' => $captcha,
        ]);


        if ($loginForm->validate()) {


            // Xoa token het han cho nhe DB
            //self::cleanOldToken();

            ApiLoginAuth::deleteAll(['OR', ['<', 'EXPIRED_AT', date('Y-m-d H:i:s')], ['msisdn' => $username]]);

            // Insert token moi
            $apiAuth = new ApiLoginAuth();
            $apiAuth->msisdn = $username;
            $apiAuth->token = Yii::$app->security->generateRandomString();
            $apiAuth->created_at = date('Y-m-d H:i:s');
            $apiAuth->expired_at = date('Y-m-d H:i:s', strtotime("+" . Yii::$app->params['api_token_expired_time'] . " minutes"));
            $apiAuth->client_useragent = Yii::$app->request->getUserAgent();
            $apiAuth->client_os = trim(Yii::$app->request->headers->get('client_os'));
            $apiAuth->save(false);

            $webUser = User::findOne(['username' => $username]);

            $data = [
                'token' => $apiAuth->token,
                'token_expired' => $apiAuth->expired_at,
                'user' => [

                    'username' => $webUser->username,
                    'fullname' => $webUser->fullname,
                    'user_type' => $webUser->user_type,
                    'partner_id' => $webUser->partner_id,
                    //'branch_id' => $webUser->branch_id,
                    //'avatar' => $webUser->avatar,
                ]
            ];

            return ApiHelper::formatResponse(
                ApiResponseCode::SUCCESS,
                $data
            );
        }

        return ApiHelper::formatResponse(
            ApiResponseCode::LOGIN_IS_REQUIRED,
            $loginForm->getErrors()
        );


    }

    /**
     * Refresh token khi het han
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRefreshToken()
    {
        $token = trim(Yii::$app->request->get('token', null));

        $apiAuth = ApiLoginAuth::find()
            ->where([
                'token' => $token,
            ])
            //->andWhere(['>', 'expired_at', date('Y-m-d H:i:s')])
            ->one();
        $msisdn = $apiAuth->msisdn;

        if (!$token || !$apiAuth) {
            return ApiHelper::formatResponse(
                ApiResponseCode::INVALID_PARAMS
            );
        }

        $webUser = User::findOne(['username' => $msisdn]);
        if (!$webUser) {
            return ApiHelper::formatResponse(
                ApiResponseCode::NO_DATA,
                [],
                Yii::t('api', 'Account not found!')
            );
        }

        // Tao token moi
        $apiAuth2 = new ApiLoginAuth();
        $apiAuth2->msisdn = $msisdn;
        $apiAuth2->token = sha1($msisdn . Yii::$app->security->generateRandomString());
        $apiAuth2->created_at = date('Y-m-d H:i:s');
        $apiAuth2->expired_at = date('Y-m-d H:i:s', strtotime("+" . Yii::$app->params['api_token_expired_time'] . " minutes"));
        $apiAuth2->client_useragent = Yii::$app->request->getUserAgent();
        $apiAuth2->client_os = trim(Yii::$app->request->headers->get('client_os'));
        $apiAuth2->save(false);

        // Xoa token cu
        $apiAuth->delete();
        // Xoa token het han cho nhe DB
        self::cleanOldToken();
        $data = [
            'token' => $apiAuth2->token,
            'token_expired' => $apiAuth2->expired_at,
            'user' => [

                'username' => $webUser->username,
                'fullname' => $webUser->fullname,
            ]
        ];


        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $data
        );
    }



    private static function cleanOldToken()
    {
        ApiLoginAuth::deleteAll(['AND', ['<', 'expired_at', date('Y-m-d H:i:s')]]);
    }

    /**
     * Kiem tra login, neu chua login --> tra ve ma loi
     * Neu da login --> tra ve WebSubscriber
     */
    public static function checkApiLogin()
    {
        if (Yii::$app->request->isPost) {
            $token = trim(Yii::$app->request->post('token', null));
        } else {
            $token = trim(Yii::$app->request->get('token', null));
        }

        $loginAuth = ApiLoginAuth::findOne(['token' => $token]);

        if (!$loginAuth) {
            return ApiHelper::formatResponse(
                ApiResponseCode::LOGIN_IS_REQUIRED);
        }

        $webSub = User::findOne([
            'username' => $loginAuth->msisdn
        ]);

        if (!$webSub) {
            return ApiHelper::formatResponse(
                ApiResponseCode::LOGIN_IS_REQUIRED
            );
        }
        return $webSub;
    }

    public function checkTokenFromHeader()
    {
        $token = trim(Yii::$app->request->headers->get('token'));

        $loginAuth = ApiLoginAuth::find()
            ->where([
            'token' => $token,
            ])
            ->andWhere([
                '>', 'expired_at', date('Y-m-d H:i:s'),
            ])
            ->andWhere('expired_at is not null')
            ->one() ;

        if (!$loginAuth) {
            return ApiHelper::formatResponse(
                ApiResponseCode::LOGIN_IS_REQUIRED);
        }

        $webSub = User::findOne([
            'username' => $loginAuth->msisdn
        ]);

        if (!$webSub) {
            return ApiHelper::formatResponse(
                ApiResponseCode::LOGIN_IS_REQUIRED
            );
        }
        $this->user = $webSub;
        return $webSub;
    }
}
