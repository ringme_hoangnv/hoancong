<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use api_public\modules\v1\models\Advertisment;
use common\helpers\FengShuiHelper;
use Yii;

/**
 * Site controller
 */
class HomeController extends ApiController
{
    public function actionGetInfo()
    {
        $dateInfoArr = FengShuiHelper::getArrayDateInfo(date('d'), date('m'), date('Y'));

        $date = date('d');
        $month = Yii::t('frontend', FengShuiHelper::getMonthName((date('n') - 1)));
        $thu = Yii::t('frontend', $dateInfoArr['thu_en']);

        $time = date('H:i');
        $hour = (Yii::$app->language == 'vi') ? Yii::t('frontend', FengShuiHelper::getGioCanChi()) : Yii::t('frontend', FengShuiHelper::getGioConGiap());

        $dayColor = ($dateInfoArr['ngay_hoang_dao']) ? 'red' : '';

        $lunarDate = Yii::t('frontend', date('d', strtotime($dateInfoArr['output_am'])));
        $lunarMonth = Yii::t('frontend', date('m', strtotime($dateInfoArr['output_am'])));

        $canChiNgay = Yii::t('frontend', $dateInfoArr['can_chi_ngay']);
        $canChiThang = Yii::t('frontend', $dateInfoArr['can_chi_thang']);
        $canChiNam = Yii::t('frontend', $dateInfoArr['can_chi_nam']);

        $des = Yii::t('frontend', 'Giờ tốt để xuất hành, khởi sự, giao dịch');

        $hourName = [];
        $hourValue = [];

        foreach ($dateInfoArr['gio_hoang_dao_arr'] as $gioHd) {
            array_push($hourValue, Yii::t('frontend', $gioHd[1] . '-' . $gioHd[2]));
            array_push($hourName, Yii::t('frontend', $gioHd[0]));
        }

        $data = [
            'date' => $date,
            'month' => $month,
            'thu' => $thu,
            'time' => $time,
            'hour' => $hour,
            'des' => $des,

            'dayColor' => $dayColor,
            'lunarDate' => $lunarDate,
            'lunarMonth' => $lunarMonth,
            'canChiNgay' => $canChiNgay,
            'canChiThang' => $canChiThang,
            'canChiNam' => $canChiNam,

            'hourValue' => $hourValue,
            'hourName' => $hourName,
        ];

        return ApiHelper::formatResponse(
            ApiResponseCode::SUCCESS,
            $data,
            $message
        );
    }

    public function actionAds()
    {
        $errorCode = ApiResponseCode::NO_DATA;
        $data = null;

        $footerBanners = Advertisment::getActiveAdvByPosition('footer_banner', 1);

        if (count($footerBanners)) {
            $errorCode = ApiResponseCode::SUCCESS;

            foreach ($footerBanners as $banner) {
                $data[] = [
                    'link' => $banner->LINK,
                    'imagePath' => $banner->getImageUrl()
                ];
            }
        } else {
            $errorCode = ApiResponseCode::NO_DATA;
            $data = null;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $data,
            $message
        );
    }
}
