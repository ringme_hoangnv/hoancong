<?php
$this->title = Yii::t('frontend', '12 Zodiac Signs');
\frontend\assets\HomeAsset::register($this);
?>

<div id="cung-hoang-dao" class="service text-center">
    <div class="container">
        <!-- title -->
        <div class="row">
            <div class="col-10 offset-1">
                <div class="info">
                    <a href="#"><img src="/<?= \common\helpers\Helpers::getCurrentThemeName(); ?>/images/icon2.png" alt=""></a>
                </div>
            </div>
        </div>

        <p class="intro">
            <?= Yii::t('frontend', 'No matter what your Sun sign, we are all affected by the 12 signs of the zodiac, as the Sun and other planets cycle through the horoscope over the course of a year. Browse through the signs of astrology here to known how about today?'); ?>
        </p>

        <!-- 12 zodiacs -->
        <div class="zodiac12">
            <div class="row">
                <?php $i = 0; ?>
                <?php foreach(Yii::$app->params['zodiacal_signs'] as $index => $zodiac):?>
                    <?php $i ++; ?>
                    <div class="col-6">
                        <div class="zodiac zodiac_<?= $index; ?>" value="<?= $index; ?>">
                            <a href="javascript:void(0)"><img src="/<?= \common\helpers\Helpers::getCurrentThemeName(); ?>/images/12-<?= $i ?>.png" alt=""></a>
                            <br>
                            <h6><?= Yii::t('frontend', $zodiac['name'])?><br /><?= Yii::t('frontend', '('. $zodiac['birth_date'].')')?></h6>
                        </div>

                    </div>
                <?php endforeach;?>
            </div>
        </div> <!-- /12 zodiacs -->
    </div>
</div> <!-- /service -->
<div class="container text-center">
    <?= $this->render('@app/views/common/horoscopeLink');?>
</div>

<!--  Modal -->
<div class="modal fade" id="cung-modal" tabindex="-1" role="dialog" aria-labelledby="cung-modalLabel" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div id="modal-content" class="modal-content">

        </div>
    </div>
</div>
<?php if (Yii::$app->request->get('showp') && Yii::$app->user->isGuest): ?>
<?php
// data-backdrop="static"
$this->registerJs( <<< EOT_JS_CODE
    $('#cung-hoang-dao .zodiac:first').click();
EOT_JS_CODE
);
?>
<?php endif; ?>