<?php 
use common\helpers\FengShuiHelper;

$dateInfoArr1 = FengShuiHelper::getArrayDateInfo(15, $month1, $year1);
$dateInfoArr2 = FengShuiHelper::getArrayDateInfo($day2, $month2, $year2);

$momAge = ($dateInfoArr1['nam_am'] - $dateInfoArr2['nam_am']);
// echo '<pre>';
// print_r($dateInfoArr2);die;
// print_r($dateInfoArr22);die;


?>
<div class="container-fluid">
	
	<?= $this->render('showErrors', [
			'errorArr' => $errorArr, 
		])?>

	<?php if ($formValid && $view == 1): ?>
		<div class="main-info card ">
			<div class="card-body" style="background: url(/tuvi/images/gender.png) right no-repeat;background-size: contain;">
				- <span class="label"><?= Yii::t('frontend', 'Ngày sinh âm lịch của mẹ');?>:</span> <span class="highlight"><?= date('d/m/Y', strtotime($dateInfoArr2['output_am'])) ?></span> - <?= Yii::t('frontend', 'năm');?> <span class="highlight"><?= Yii::t('frontend', $dateInfoArr2['can_chi_nam']) ?></span>
				<br />
				<br />
				- <span class="label"><?= Yii::t('frontend', 'Tháng thụ thai');?>:</span> <span class="highlight"><?= ($month1) ?></span>
				<br />
				- <span class="label"><?= Yii::t('frontend', 'Tuổi âm lịch của mẹ lúc thụ thai');?>:</span> <span class="highlight"><?= $momAge ?></span>

				
					<br />
					- <?= Yii::t('frontend', 'Dự đoán bạn sẽ sinh bé');?>: 
					<span class="highlight">
						<?= ($showFull)? FengShuiHelper::getGenderFromChart($momAge, $month1): '???'; ?>
					</span>
					
			</div>
		</div>
		<br />
		<?php if(!$showFull): ?>
			<?= $this->render('@app/views/layouts/partials/viewMoreContent', [
				'contentStatusCode' => $contentStatusCode,
                'contentId' => 'gender_predict',
			])?>
			
		<?php else: ?>

			<span class="highlight">History of the chinese gender chart?</span>
			<br />
			<?= Yii::t('frontend', 'Người Trung Quốc cổ đại dự đoán giới tính của em bé dựa theo Kinh Dịch (I Ching), và dựa theo Ngũ hành, và Âm dương bát quái.'); ?>
			<br />
			<?= Yii::t('frontend', 'Truyền thuyết nói rằng, biểu đồ giới tính cổ của Trung Quốc được vua quan nhà Thanh sử dụng để lựa chọn sinh con trai cho dòng tộc. Một câu truyện khác nói rằng biểu đồ này được tìm thấy trong một ngôi mộ Hoàng Gia cổ đại.')?>
			<br />
			<?= Yii::t('frontend', 'Nhiều người cho rằng biểu đồ này chính xác tới 90%. Bạn có thể tham khảo thêm biểu đồ bên dưới để dự đoán giới tính của bé.');?>
			<br />
			<br />
			<img class="img img-fluid" width="100%" src="/tuvi/images/chinese-gender-chart.png" />
			<br />
		<?php endif; // show full ?>
	<?php endif; // form valid ?>
	
</div>
<br />