<div class="modal-header">
    <h5 class="modal-title" ><?= Yii::t('frontend', $signInfo['name']); ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="modal-body" class="modal-body">
    <?= nl2br($text); ?>
    <div class="clearfix"></div>
</div>
<div class="modal-footer" >
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('frontend', 'Đóng'); ?></button>

        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
</div>
