<?php 
use yii\helpers\Url;
use frontend\assets\LubanAsset;

LubanAsset::register($this);
/*
$arr1 = [
    'Tài'                       => 'Cung tốt',
    'Tài đức'                   => 'Có tài và có đức, luôn gặp may mắn, có người cứu giúp',
    'Bảo khố'                   => 'Của cải sung túc',
    'Lục hợp'                   => 'Gia đình hòa hợp',
    'Nghênh phú'                => 'Đón điều tốt lành, gặp nhiều may mắn, hạnh phúc',
    'Bệnh'                      => 'Cung xấu',
    'Thoát tài'                 => 'Mất tiền',
    'Công sự'                   => 'Tranh chấp, kiện tụng',
    'Lao chấp'                  => 'Vướng vòng lao lý, tù đày',
    'Cô quả'                    => 'Chịu phận cô đơn',
    'Ly'                        => 'Cung xấu',
    'Trường bệnh'               => 'Nhiều bệnh tật, thị phi',
    'Kiếp tài'                  => 'Hao tổn tài sản, dễ bị cướp bóc',
    'Quan quỷ'                  => 'Quan tụng, trong nhà nhiều chuyện kỳ quái',
    'Thất thoát'                => 'Bị mất mát',
    'Nghĩa'                     => 'Cung tốt',
    'Thêm dinh'                 => 'Thêm người, thêm của',
    'Ích lợi'                   => 'Gặp nhiều thuận lợi',
    'Quý tử'                    => 'Con cái thông minh, hiếu thảo',
    'Đại cát'                   => 'Trăm sự đều hay, may mắn thuận lợi',
    'Quan'                      => 'Cung tốt',
    'Thuận khoa'                => 'Thi cử, công danh thuận lợi',
    'Hoạch tài'                 => 'Được tiền của bất ngờ',
    'Tấn đức'                   => 'Làm ăn phát đạt, được quý nhân giúp',
    'Phú quý'                   => 'Giàu có danh vọng',
    'Kiếp'                      => 'Cung xấu',
    'Tử biệt'                   => 'Chết chóc',
    'Thoái khẩu'                => 'Mất người',
    'Ly hương'                  => 'Người thân xa quê',
    'Tài thất'                  => 'Làm ăn thất bại, của cải tiêu tan',
    'Hại'                       => 'Cung xấu',
    'Tai chí'                   => 'Dễ gặp tai họa',
    'Tử tuyệt'                  => 'Hao người, công việc thất bại, phá sản',
    'Bệnh lâm'                  => 'Dễ mắc bệnh',
    'Khẩu thiệt'                => 'Dễ gây thị phi',
    'Bản'                       => 'Cung tốt',
    'Tài chí'                   => 'Tiền của đến, công việc làm ăn thuận lợi',
    'Đăng khoa'                 => 'Thi cử đỗ đạt, công thành danh toại',
    'Tiến bảo'                  => 'Của cải, tài lộc gia tăng không ngừng',
    'Hưng vượng'                => 'Công việc hanh thông, làm ăn phát tài',
    'Quý nhân'                  => 'Cung tốt',
    'Quyền lộc'                 => 'Có quyền thế, có lộc',
    'Trung tín'                 => 'Được tín nhiệm, trung thành',
    'Tác quan'                  => 'Có chức vị, quan lộc',
    'Phát đạt'                  => 'Phát tài, phát lộc, thuận kinh doanh',
    'Thông minh'                => 'Thông thái, học hành thi cử tốt',
    'Hiểm họa'                  => 'Cung xấu',
    'Án thành'                  => 'Gia cảnh sẽ bị tán tài lộc',
    'Hỗn nhân'                  => 'Con cháu hư đốn',
    'Thất hiếu'                 => 'Con cháu bất hiếu',
    'Tai họa'                   => 'Gặp tai họa, đau ốm',
    'Thường bệnh'               => 'Thường xuyên bệnh tật',
    'Thiên tai'                 => 'Cung xấu',
    'Hoàn tử'                   => 'Con cái khó nuôi, hay tật bệnh',
    'Quan tài'                  => 'Quan lộc, tài lộc tổn hại',
    'Thân tàn'                  => 'Thân thể bệnh tật, dễ tai nạn',
    'Thất tài'                  => 'Dễ bị lừa về tiền bạc',
    'Hệ quả'                    => 'Vợ chồng bất hòa, con cái dễ tai nạn',
    'Thiên tài'                 => 'Cung tốt',
    'Thi thơ'                   => 'Tốt cho văn học, nghệ thuật, âm nhạc',
    'Văn học'                   => 'Tốt cho văn học, nghệ thuật, sáng tạo',
    'Thanh quý'                 => 'Thanh tao, sáng tạo, ít tật bệnh',
    'Tác lộc'                   => 'Thêm tài, thêm lộc, lưu tài',
    'Thiên lộc'                 => 'May mắn kinh doanh, buôn bán, lộc trời cho',
    'Nhân lộc'                  => 'Cung tốt',
    'Trí tồn'                   => 'Con cái học giỏi, trí tuệ thông minh',
    'Phú quý'                   => 'Công danh thành đạt, tài lộc tốt',
    'Tiến bảo'                  => 'Thêm tài, thêm lộc, kinh doanh lợi lạc',
    'Thập thiện'                => 'Tăng phúc, gia đạo bình an',
    'Văn chương'                => 'Khoa bảng, thi cử thuận lợi',
    'Cô độc'                    => 'Cung xấu',
    'Bạc nghịch'                => 'Hao người, vô vọng, con cái ngỗ nghịch',
    'Vô vọng'                   => 'Người ở xa khó về',
    'Ly tán'                    => 'Ly biệt',
    'Tửu thục'                  => 'Vướng vào tửu sắc, tật bệnh',
    'Dâm dục'                   => 'Vướng vào dâm dục, tật bệnh',
    'Thiên tặc'                 => 'Cung xấu',
    'Phong bệnh'                => 'Bệnh tật bất ngờ, sức khỏe yếu',
    'Chiêu ôn'                  => 'Bệnh tật, tai bay vạ gió',
    'Ôn tài'                    => 'Kiện tụng về tiền bạc',
    'Ngục tù'                   => 'Kiện tụng, tù ngục, thị phi',
    'Quan tài'                  => 'Bệnh nặng, tai ách',
    'Tể tướng'                  => 'Cung tốt',
    'Đại tài'                   => 'Phát tài nhanh, hanh thông tài lộc',
    'Thi thơ'                   => 'Con cái thành danh, thi cử đỗ đạt',
    'Hoạch tài'                 => 'Tài lộc tốt, thành công viên mãn',
    'Hiếu tử'                   => 'Con cái có hiếu, sinh quý tử',
    'Qúy nhân'                  => 'Quý nhân trợ giúp, may mắn trong công việc',
    'Đinh (丁)'                  => 'Cung tốt',
    'Phúc tinh'                 => 'Luôn gặp may mắn, khi có tai họa có người cứu giúp',
    'Cập đệ'                    => 'Thi cử đỗ đạt',
    'Tài vượng'                 => 'Được nhiều tiền của',
    'Đăng khoa'                 => 'Thi cử đỗ đạt, học hành tốt',
    'Hại (害)'                   => 'Cung xấu',
    'Khẩu thiệt'                => 'Mang họa vì lời nói',
    'Lâm bệnh'                  => 'Dễ mắc bệnh hiểm nghèo',
    'Tử tuyệt'                  => 'Người chết, công việc thất bại, phá sản',
    'Tai chí'                   => 'Tai họa ập đến',
    'Vượng (旺)'                 => 'Cung tốt',
    'Thiên đức'                 => 'Hưởng lộc từ trời',
    'Hỷ sự'                     => 'Gặp nhiều chuyện vui',
    'Tiến bảo'                  => 'Được của quý',
    'Nạp phúc'                  => 'Đón nhận phúc, gia tăng phúc lộc',
    'Khổ (苦)'                   => 'Cung xấu',
    'Thất thoát'                => 'Mất mát, đồ đạc bị thất lạc, người ly biệt',
    'Quan quỷ'                  => 'Chuyện xấu với chính quyền',
    'Kiếp tài'                  => 'Bị cướp của',
    'Vô tự'                     => 'Không con nối dõi',
    'Nghĩa (義)'                 => 'Cung tốt',
    'Đại cát'                   => 'Cát tường, cát lợi, may mắn',
    'Tài vượng'                 => 'Tài lộc tăng, kinh doanh buôn bán phát đạt',
    'Ích lợi'                   => 'Gặp nhiều thuận lợi',
    'Thiên khố'                 => 'Kho trời, hưởng phúc lộc, tiền tài',
    'Quan (官)'                  => 'Cung tốt',
    'Phú quý'                   => 'Giàu có',
    'Tiến bảo'                  => 'Của cải gia tăng',
    'Hoạnh tài'                 => 'Được của cải bất ngờ, không trong dự tính',
    'Thuận khoa'                => 'Thi cử thuận lợi',
    'Tử (死)'                    => 'Cung xấu',
    'Ly hương'                  => 'Xa cách quê hương',
    'Tử biệt'                   => 'Chia lìa chết chóc',
    'Thoái đinh'                => 'Con trai gặp nhiều bất lợi, đi xa hoặc tử biệt',
    'Thất tài'                  => 'Làm ăn thất bại, của cải tiêu tan',
    'Hưng (興)'                  => 'Cung tốt',
    'Đăng khoa'                 => 'Thi cử đỗ đạt, học hành tốt',
    'Quý tử'                    => 'Con hiền ngoan, có tài đức',
    'Thêm đinh'                 => 'Thêm con trai',
    'Hưng vượng'                => 'Làm ăn hưng thịnh',
    'Thất (失)'                  => 'Cung xấu',
    'Cô quả'                    => 'Chịu phận cô đơn',
    'Lao chấp'                  => 'Vướng vòng lao lý, tù tội',
    'Công sự'                   => 'Tranh chấp, thưa kiện ra chính quyền',
    'Thoái tài'                 => 'Hao tốn tiền của, làm ăn thua lỗ',
    'Tài (財)'                   => 'Cung tốt',
    'Nghênh phúc'               => 'Đón nhận phúc đến, gặp nhiều may mắn',
    'Lục hợp'                   => 'Gia đình hòa hợp (cha, mẹ, vợ, chồng, con, cháu)',
    'Tiến bảo'                  => 'Của cải gia tăng',
    'Tài đức'                   => 'Có tiền của và có đức',
];

$arr2 = [

];
*/
?>
<?= $this->render('@app/views/layouts/partials/_seo', [
    'title' => Yii::t('frontend', 'Fengshui Feng Shui Decoration Furniture Luban Lu Ban Ruler'),
    'description' => Yii::t('frontend', 'Khmer feng shui Luban ruler, How to use the Lu Ban ruler?'),
    'keywords' => 'khmer feng shui ruler, cambodian Luban ruler, feng shui luban ruler,feng shui calculator,feng shui mesurement',
]); ?>
<div class="box-title box-title2 container-fluid">
    <h2 class=""><?= Yii::t('frontend', 'Luban Ruler'); ?></h2>
</div>
<div class="" style="padding:0 5px;">
    <div class="luban-content">

        <div id="lobanOuter">
            <div id="abc"></div>
            <div class="loban-touch-left"></div>
            <div class="loban-touch-right"></div>
            <div id="sodoLoban"></div>
            <div id="container-sodo" ><input type="text" class="form-control" value="" name="sodo" id="sodo" /> mm (<?= Yii::t('frontend', 'nhập số');?>)</div>
            <div id="thanhdo" style="left: 491px;"></div>
            <p class="loban-note" style="display: none"><?= Yii::t('frontend', 'Kéo thước sang trái hoặc phải')?></p>
            <p class="loban-t loban-522"><strong><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 52.2cm:</strong> <?php // Yii::t('frontend', 'Khoảng thông thủy (cửa, cửa sổ...)');?></p>
            <p class="loban-t loban-429"><strong><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 42.9cm:</strong> <?php // Yii::t('frontend', 'Khối xây dựng (bếp, bệ, bậc...)');?></p>
            <p class="loban-t loban-388"><strong><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 38.8cm:</strong> <?php // Yii::t('frontend', 'Đồ nội thất (bàn thờ, tủ...)');?></p>
            <div id="loban-wrapper" >
                <div id="loban-scroller" >
                    <div id="pullRight" style="display: none;" >
                        <span class="pullRightIcon"></span><span class="pullRightLabel"><?= Yii::t('frontend', 'Kéo qua phải');?>...</span>
                    </div>
                    <ul id="loban-thelist">
                        <li>
                            <img src="/feng-shui-horoscope/thuoc522" nopin="nopin">
                            <img src="/feng-shui-horoscope/thuoc429" nopin="nopin">
                            <img src="/feng-shui-horoscope/thuoc388" nopin="nopin">
                        </li>
                    </ul>
                    <div id="pullLeft" style="display:none;" >
                        <span class="pullLeftIcon"></span><span class="pullLeftLabel"><?= Yii::t('frontend', 'Kéo qua trái');?>...</span>
                    </div>
                </div>
            
    		
    		
    		</div>
            <div class="clearfix"></div>
        </div>
        <div style="text-align: right; padding-top: 5px; display: none"><?= Yii::t('frontend', 'Đơn vị tính');?>: mm</div>
    </div>
</div>
<div class="container-fluid" style="color:#212529">
    <div class="box_huongdan">
        <div class="tdbox_hd">
            <h2><b><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 52.2cm</b>: <?= Yii::t('frontend', 'Khoảng thông thủy (cửa, cửa sổ...)');?></h2>
        </div>
        <div class="noidung_boxhd">
            <span id="nd522-text"></span>
        </div>
        <div class="tdbox_hd">
            <h2><b><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 42.9cm</b>: <?= Yii::t('frontend', 'Khối xây dựng (bếp, bệ, bậc...)');?></h2>
        </div>
        <div class="noidung_boxhd">
            <span id="nd429-text"></span>
        </div>
        <div class="tdbox_hd">
            <h2><b><?= Yii::t('frontend', 'Thước Lỗ Ban');?> 38.8cm</b>: <?= Yii::t('frontend', 'Đồ nội thất (bàn thờ, tủ...)');?></h2>
        </div>
        <div class="noidung_boxhd noidung_boxhd_last">
            <span id="nd388-text"></span>
        </div>
    </div>
    <div class="box_huongdan" style="margin-top: 30px">
        <div class="tdbox_hd">
            <h2><b><?= Yii::t('frontend', 'Thước Lỗ Ban');?>?</b></h2>
        </div>

        <div class="noidung_boxhd noidung_boxhd_last">
            <?php if (Yii::$app->language == 'en'): ?>
            <p>
                Lǔ Bān was a master of the Feng Shui and the father of the Chinese ancient carpenters. He was the inventor of the saw, chisel and many other carpentry tools, including the Lu Ban ruler. He is also the author of the 鲁班经 Lǔ Bān Jīng ‘Divine Carpenter’s Classic’, the Chinese building manual dating from the early 明 Míng dynasty.
            </p>
            <br />
            <p>
                Every Luban Ruler has a specific length divided into sections of equal auspicious (good luck) and non auspicious (bad luck) portions. Each section is then divided into more specific sub-sections. After the last section the pattern repeats.
            </p>
            <br />
            <p>
                There are more than 20 different types of rulers or set of measurements depending on the proposal of the object to be measured. The most common rulers are:
            </p>
            - 52 cm: "airflow" objects. Objects that connect two spaces like doors or windows.<br />
            - 42.9 cm: solid construction. It also corresponds to the Yang Houses (Living): a staircase step or a concrete kitchen.<br />
            - 38.8 cm: furniture and "land-related" measurement. It also corresponds to the Yin Houses (Death): coffins, altars or grave sites.<br />

            <?php elseif (Yii::$app->language == 'kh'): ?>
                <p>
                Luban គឺជាមេរបស់ហ្វាងសួរនិងជាឪពុករបស់ជាងឈើជនជាតិចិន។ គាត់ជាអ្នកបង្កើតឧបករណ៍ម៉ាសឧបករណ៍ម៉ាសនិងឧបករណ៍ជាងឈើដទៃទៀតដែលរាប់បញ្ចូលទាំងអ្នកគ្រប់គ្រង Luban ផងដែរ។ គាត់ក៏ជាអ្នកនិពន្ធសៀវភៅក្បាច់បុរាណដ៏ល្បីល្បាញរបស់ជនជាតិ Lǔ Bān Jīng ផងដែរ ដែលជាសៀវភៅសាងសង់អាគារចិនតាំងពីសម័យដើមរាជវង្សមីង។
                </p>
                <br />
                <p>
                    គ្រប់ បន្ទាត់ Luban មានប្រវែងជាក់លាក់មួយត្រូវបានបែងចែកជាផ្នែកមួយនៃសំណាងស្មើគ្នា (សំណាងល្អ) និងមិនមែនជាសំណាងអាក្រក់។ ផ្នែកនីមួយៗត្រូវបានបែងចែកជាផ្នែករងជាក់លាក់បន្ថែមទៀត។ បន្ទាប់ពីផ្នែកចុងក្រោយដែលលំនាំធ្វើម្តងទៀត។
                </p>
                <br />
                <p>
                    មានជាង 20 ប្រភេទផ្សេងគ្នានៃបន្ទាត់ឬសំណុំរង្វាស់អាស្រ័យលើសំណើរបស់វត្ថុដែលត្រូវបានវាស់។ ជាទូរទៅបំផុតគឺ:
                </p>
                - 52 សង់ទីម៉ែត្រ: វត្ថុ ""លំហូរខ្យល់"" ។ វត្ថុដែលតភ្ជាប់កន្លែងពីរដូចជាទ្វារឬបង្អួច។ <br />
                - 42,9 សង់ទីម៉ែត្រ: សំណង់រឹងមាំ។ វាក៏ត្រូវគ្នាទៅនឹងផ្ទះ Yang (ការរស់នៅ): ជណ្តើរមួយឬផ្ទះបាយបេតុង <br />
                - 38,8 សង់ទីម៉ែត្រ: គ្រឿងសង្ហារឹមនិង​ ការវាស់វែងទាក់ទងនឹងដី។ វាក៏ទាក់ទងទៅនឹងផ្ទះ Yin (មរណភាព): មឈូស, អាសនៈ ឬកន្លែងកប់ខ្មោច។ <br />
            <?php else:  ?>
                <p>     Lǔ Bān သည်  Feng Shui ၏သခင်ဖြစ်ပြီး တရုတ်ရှေးဟောင်းလက်သမားများ၏ ဖခင်ဖြစ်သည်။ သူသည် Lu Ban ပေတံအပါအဝင် လွှ၊ ဆောက်နှင့်  အခြားလက်သမားပစ္စည်းများ၏ တီထွင်သူဖြစ်သည်။ သူသည်  明 Míng မင်းဆက်အစောပိုင်းအချိန်မှစ၍ တရုတ်အဆောက်အအုံလက်မှုပညာဖြစ်သော 鲁班经 Lǔ Bān Jīng 'လှပသောလက်သမား၏ ဂန္တဝင်လက်ရာ' ၏လက်ရာရှင်ဖြစ်သည်။  </p> <br /> <p>
Luban ပေတံတိုင်းတွင် တူညီသည့်အတိတ်နိမိတ်ကောင်းသော (ကံကောင်ခြင်း)နှင့် အတိတ်နိမိတ်မကောင်းသော (ကံဆိုးခြင်း)အပိုင်းများဟူ၍ ပိုင်းခြားထားသည့် တိကျသောအတိုင်းအတာပါရှိသည်။ အပိုင်းတစ်ပိုင်းစီကို ပိုမိုတိကျသောအပိုင်းခွဲလေးများအဖြစ် ပိုင်းခြားသည်။ နောက်ဆုံးတစ်ပိုင်းပြီးလျှင် ပုံစံထပ်ဖြစ်သည်။
 </p> <br /> <p>
တိုင်းရမည့်အရာ၏ တောင်းဆိုချက်အပေါ်မူတည်၍ ပေတံအမျိုးအစား သို့မဟုတ် အတိုင်းအတာပုံစံ 
 ၂၀ ကျော် ရှိသည်။ အများဆုံးအသုံးပြုသောပေတံများမှာ-
   </p> - ၅၂ စင်တီမီတာ - “လေဝင်လေထွက်ရှိသည့်” အရာများဖြစ်သည်။ တံခါးများ သို့မဟုတ် ပြတင်းပေါက်များကဲ့သို့ နေရာလွတ်နှစ်နေရာကို ချိတ်ဆက်ပေးသောအရာများဖြစ်သည်။   <br /> - ၄၂.၉ စင်တီမီတာ အစိုင်အခဲတည်ဆောက်မှုဖြစ်သည်။ ၎င်းသည် Yang အိမ်များ(နေထိုင်ခြင်း)-လှေကားခြေနင်း သို့မဟုတ် ကွန်ကရစ်မီးဖိုချောင်တို့နှင့်လည်း သက်ဆိုင်သည်။<br /> - ၃၈.၈ စင်တီမီတာ- ပရိဘောဂများနှင့် “မြေနှင့်ဆိုင်သော”တိုင်းတာမှုဖြစ်သည်။ ၎င်းသည် Yin အိမ်မျာ(သေဆုံးခြင်း)-အခေါင်းများ၊ ယဇ်ပလ္လင်များ သို့မဟုတ် အုတ်ဂူနေရာများနှင့်လာ်း သက်ဆိုင်သည်။<br />
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var note522c = [
        "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>: (<?= Yii::t('frontend', 'Gia cảnh được khả quan, làm ăn phát đạt, bạn bè trung thành, con cái thông minh hiếu thảo')?>).",
        "<b><?= Yii::t('frontend', 'XẤU') ?></b>: (<?= Yii::t('frontend', 'Gia chủ sẽ bị tán tài lộc, trôi dạt tha phương, cuộc sống túng thiếu, gia đạo có người đau ốm, con cái dâm ô hư thân mất nết, bất trung bất hiếu')?>).",
        "<b><?= Yii::t('frontend', 'XẤU') ?></b>: (<?= Yii::t('frontend', 'Coi chừng ốm đau nặng, chết chóc, mất của, vợ chồng sống bất hòa, con cái gặp nạn');?>).",
        "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>: (<?= Yii::t('frontend', 'Chủ nhà luôn may mắn về tài lộc, năng tài đắc lợi, con cái được nhờ vả, hiếu thảo, gia đạo chí thọ, an vui')?>).",
        "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>: (<?= Yii::t('frontend', 'Chủ nhà luôn gặp sung túc, phúc lộc, nghề nghiệp luôn phát triển, năng tài đắc lợi, con cái thông minh, hiếu thảo')?>).",
        "<b><?= Yii::t('frontend', 'XẤU') ?></b>: (<?= Yii::t('frontend', 'Gia chủ hao người, hao của, biệt ly, con cái ngỗ nghịch, tửu sắc vô độ đến chết')?>).",
        "<b><?= Yii::t('frontend', 'XẤU') ?></b>: (<?= Yii::t('frontend', 'Coi chừng bệnh đến bất ngờ, hay bị tai bay vạ gió, kiện tụng, tù ngục, chết chóc')?>).",
        "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>: (<?= Yii::t('frontend', 'Gia chủ hanh thông mọi mặt, con cái tấn tài danh, sinh con quý tử, chủ nhà luôn may mắn bất ngờ')?>)."
        ],
        note429c = [
            "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>",
            "<b><?= Yii::t('frontend', 'XẤU') ?></b>",
            "<b><?= Yii::t('frontend', 'XẤU') ?></b>",
            "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>",
            "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>",
            "<b><?= Yii::t('frontend', 'XẤU') ?></b>",
            "<b><?= Yii::t('frontend', 'XẤU') ?></b>",
            "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>"
        ],
        note388c = ["<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>", "<b><?= Yii::t('frontend', 'XẤU') ?></b>", "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>", "<b><?= Yii::t('frontend', 'XẤU') ?></b>", "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>", "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>", "<b><?= Yii::t('frontend', 'XẤU') ?></b>", "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>", "<b><?= Yii::t('frontend', 'XẤU') ?></b>", "<span class='good'><?= Yii::t('frontend', 'TỐT') ?></span>"
        ];
</script>

<br />
<div class="container text-center">
    <?= $this->render('@app/views/common/horoscopeLink', ['showZodiac' => true]);?>
</div>