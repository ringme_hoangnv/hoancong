<?php
$this->title =  Yii::t('frontend', 'Burmese Zodiac Signs');
\frontend\assets\HomeAsset::register($this);
?>
<!-- service -->
<div id="cung-hoang-dao2" class="service text-center">
    <div class="container">
        <!-- title -->
        <div class="row">
            <div class="col-10 offset-1">
                <div class="info">
                    <a href="#"><img src="/<?= \common\helpers\Helpers::getCurrentThemeName(); ?>/images/icon1.png" alt=""></a>
                </div>
            </div>
        </div>

        <p class="intro"><?= Yii::t('frontend', 'Browse through the signs of astrology here to known how about today?'); ?></p>

        <!-- Burmese zodiacs -->
        <div class="burmese" id="myanmar_zodiac_signs">
            <div class="row">
                <?php $i = 0; ?>
                <?php foreach(Yii::$app->params['burmese_astrology_signs'] as $index => $zodiac):?>
                    <?php $i ++; ?>
                    <div class="col-6">
                        <div class="zodiac zodiac_<?= $index?>" value="<?= $index?>">
                            <a href="javascript:void(0);"><img src="/<?= \common\helpers\Helpers::getCurrentThemeName(); ?>/images/bz-<?= $i ?>.png" alt=""></a>
                            <br>
                            <h6><?= Yii::t('frontend', $zodiac['name'])?></h6>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div> <!-- /burmese -->

    </div>
</div> <!-- /service -->
<br />
<div class="container text-center">
    <?= $this->render('@app/views/common/horoscopeLink');?>
</div>
<!--  Modal -->
<div class="modal fade" id="cung-modal" tabindex="-1" role="dialog" aria-labelledby="cung-modalLabel" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div id="modal-content" class="modal-content">

        </div>
    </div>
</div>
<?php if (Yii::$app->request->get('showp') && Yii::$app->user->isGuest): ?>
<?php
// data-backdrop="static"
$this->registerJs( <<< EOT_JS_CODE
    $('#myanmar_zodiac_signs .zodiac:first').click();
EOT_JS_CODE
);
?>
<?php endif; ?>