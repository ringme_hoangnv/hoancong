<?php

use common\helpers\FengShuiHelper;

$dateInfoArr1 = FengShuiHelper::getArrayDateInfo($day1, $month1, $year1);
$dateInfoArr2 = FengShuiHelper::getArrayDateInfo($day2, $month2, $year2);
$saoVan1 = FengShuiHelper::tinhSaoVan($dateInfoArr1['nam_am'], 1);
$saoVan2 = FengShuiHelper::tinhSaoVan($dateInfoArr2['nam_am'], 0);
$saoVanNguHanh1 = FengShuiHelper::$sao_van_ngu_hanh[$saoVan1];
$saoVanNguHanh2 = FengShuiHelper::$sao_van_ngu_hanh[$saoVan2];
// echo '<pre>';
// print_r($dateInfoArr1);
// print_r($dateInfoArr2);die;


?>
<div class="container-fluid">


    <?= $this->render('showErrors', [
        'errorArr' => $errorArr,
    ]) ?>

    <?php if ($formValid): ?>
        <br/>
        <div class="main-info">
        <table class="table table-bordered">
            <tr>
                <td class="text-center"><?= Yii::t('frontend', 'Tuổi chồng'); ?></td>
                <td class="text-center"><?= Yii::t('frontend', 'Tuổi vợ'); ?></td>
            </tr>
            <tr>
                <td width="50%">
                    - <?= Yii::t('frontend', 'Dương lịch'); ?>: <span
                            class="highlight"><?= $day1 . '/' . $month1 . '/' . $year1 ?></span>
                    <br/>
                    - <span class="label"><?= Yii::t('frontend', 'Âm lịch'); ?>:</span> <span
                            class="highlight"><?= date('d/m/Y', strtotime($dateInfoArr1['output_am'])) ?></span>

                    <br/>
                    -
                    <span class="label"><?= Yii::t('frontend', 'Theo can chi'); ?>:</span> <?= Yii::t('frontend', 'ngày'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr1['can_chi_ngay']) ?></span> <?= Yii::t('frontend', 'tháng'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr1['can_chi_thang']) ?></span> <?= Yii::t('frontend', 'năm'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr1['can_chi_nam']) ?></span>
                    <?php if ($showFull): ?>
                        <br/>
                        <br/>
                        - <?= Yii::t('frontend', 'Năm'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr1['can_chi_nam']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Mệnh'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr1['menh_ngu_hanh']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Cung'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr1['menh_ngu_hanh']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Niên mệnh'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $saoVanNguHanh1) ?></span>
                        <br/>
                    <?php endif; // show full ?>

                </td>
                <td width="50%">
                    - <?= Yii::t('frontend', 'Dương lịch'); ?>: <span
                            class="highlight"><?= $day2 . '/' . $month2 . '/' . $year2 ?></span>
                    <br/>
                    - <span class="label"><?= Yii::t('frontend', 'Âm lịch'); ?>:</span> <span
                            class="highlight"><?= date('d/m/Y', strtotime($dateInfoArr2['output_am'])) ?></span>
                    <br/>
                    -
                    <span class="label"><?= Yii::t('frontend', 'Theo can chi'); ?>:</span> <?= Yii::t('frontend', 'ngày'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr2['can_chi_ngay']) ?></span> <?= Yii::t('frontend', 'tháng'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr2['can_chi_thang']) ?></span> <?= Yii::t('frontend', 'năm'); ?>
                    <span class="highlight"><?= Yii::t('frontend', $dateInfoArr2['can_chi_nam']) ?></span>

                    <?php if ($showFull): ?>
                        <br/>
                        <br/>
                        - <?= Yii::t('frontend', 'Năm'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr2['can_chi_nam']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Mệnh'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr2['menh_ngu_hanh']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Cung'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $dateInfoArr2['menh_ngu_hanh']) ?></span>
                        <br/>
                        - <?= Yii::t('frontend', 'Niên mệnh'); ?>: <span
                                class="highlight"><?= Yii::t('frontend', $saoVanNguHanh2) ?></span>
                        <br/>

                    <?php endif; // show full ?>
                </td>
            </tr>
        </table>
        <?php if (!$showFull): ?>
            <?= $this->render('viewMoreContent', [
                'contentStatusCode' => $contentStatusCode,
                'contentId' => 'couples',
            ]) ?>

        <?php else: ?>
            <?php $diem = 0; ?>
            <table class="table table-bordered">
                <tr>
                    <th><?= Yii::t('frontend', 'Thông tin'); ?></th>
                    <th class="text-center"><?= Yii::t('frontend', 'Kết luận'); ?></th>
                </tr>
                <tr>
                    <td>
                        <?php
                        $can1 = $dateInfoArr1['can'];
                        $can2 = $dateInfoArr2['can'];

                        ?>
                        <?= Yii::t('frontend', 'Về Mệnh'); ?>: <br/>
                        <?= Yii::t('frontend', 'Mệnh chồng'); ?>:
                        <span class="highlight">
								<?= (in_array($can1, FengShuiHelper::$can_duong)) ? Yii::t('frontend', 'Dương') : Yii::t('frontend', 'Âm') ?>
                                <?= Yii::t('frontend', $dateInfoArr1['menh_theo_can_chi']) ?>
							</span>
                        - <?= Yii::t('frontend', 'Mệnh vợ'); ?>:
                        <span class="highlight">
								<?= (in_array($can2, FengShuiHelper::$can_duong)) ? Yii::t('frontend', 'Dương') : Yii::t('frontend', 'Âm') ?>
                                <?= Yii::t('frontend', $dateInfoArr2['menh_theo_can_chi']) ?>
							</span>

                    </td>
                    <td>
                        <?php
                        // Check sinh khac 2 chieu
                        $checkSinhKhac1 = FengShuiHelper::checkSinhKhac($dateInfoArr1['menh_theo_can_chi'] . '-' . $dateInfoArr2['menh_theo_can_chi']);
                        $checkSinhKhac2 = FengShuiHelper::checkSinhKhac($dateInfoArr2['menh_theo_can_chi'] . '-' . $dateInfoArr1['menh_theo_can_chi']);
                        if ($checkSinhKhac1 == 1 || $checkSinhKhac2 == 1) {
                            // Tuong sinh
                            echo Yii::t('frontend', 'Tương Sinh (rất tốt)');
                            $diem += 2;
                        } elseif ($checkSinhKhac1 == 2 || $checkSinhKhac2 == 2) {
                            // Tuong khac
                            echo Yii::t('frontend', 'Tương Khắc (không tốt)');

                        } else {
                            // Khong sinh khac
                            echo Yii::t('frontend', 'Bình thường');
                            $diem += 1;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>

                        <?= Yii::t('frontend', 'Về thiên can'); ?>:
                        <br/>
                        <?= Yii::t('frontend', 'Chồng'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $can1) ?> 
							</span>
                        - <?= Yii::t('frontend', 'Vợ'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $can2) ?>
							</span>

                    </td>
                    <td>
                        <?php
                        // Check sinh khac 2 chieu
                        $thienCanTuongHop = FengShuiHelper::checkThienCanTuongHop($can1, $can2);
                        if ($thienCanTuongHop == 1) {
                            // Tuong hop

                            echo Yii::t('frontend', 'Tương hợp');
                            $diem += 2;
                        } elseif ($thienCanTuongHop == 2) {
                            // Tuong pha
                            echo Yii::t('frontend', 'Tương phá');
                        } else {
                            echo Yii::t('frontend', 'Bình thường');
                            $diem += 1;
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <?php
                    $chi1 = $dateInfoArr1['chi'];
                    $chi2 = $dateInfoArr2['chi'];
                    ?>
                    <td>
                        <?= Yii::t('frontend', 'Về Địa Chi'); ?>:
                        <br/>
                        <?= Yii::t('frontend', 'Chồng'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $chi1) ?> 
							</span>
                        - <?= Yii::t('frontend', 'Vợ'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $chi2) ?>
							</span>
                    </td>
                    <td>
                        <?php $code = FengShuiHelper::checkDiaChi($chi1, $chi2); ?>
                        <?= FengShuiHelper::checkDiaChiText($code); ?>
                        <?php $diem += FengShuiHelper::getDiemDiaChi($code); ?>
                    </td>
                </tr>
                <tr>
                    <?php
                    $cung1 = FengShuiHelper::getCung($year1, 1);
                    $cung2 = FengShuiHelper::getCung($year2, 0);

                    $cungChongVo = FengShuiHelper::$cung_chong_vo[$cung1 . '-' . $cung2];
                    ?>
                    <td>
                        <?= Yii::t('frontend', 'Về Cung Phi'); ?>:
                        <br/>
                        <?= Yii::t('frontend', 'Chồng'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $cung1) ?> 
							</span>
                        - <?= Yii::t('frontend', 'Vợ'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $cung2) ?> 
							</span>
                        <!--							<br /><i>(-->
                        <? //= Yii::t('frontend', $cungChongVo['desc'])?><!--)</i>-->
                    </td>
                    <td>
                        <?= Yii::t('frontend', $cungChongVo['value']); ?>
                    </td>
                </tr>
                <tr>

                    <td>
                        <?= Yii::t('frontend', 'Về Niên mệnh năm sinh'); ?>:
                        <br/>
                        <?= Yii::t('frontend', 'Chồng'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $saoVanNguHanh1) ?> 
							</span>
                        - <?= Yii::t('frontend', 'Vợ'); ?>:
                        <span class="highlight">
								<?= Yii::t('frontend', $saoVanNguHanh2) ?> 
							</span>
                    </td>
                    <td>
                        <?php
                        // Check sinh khac 2 chieu
                        $checkSinhKhac1 = FengShuiHelper::checkSinhKhac($saoVanNguHanh1 . '-' . $saoVanNguHanh2);
                        $checkSinhKhac2 = FengShuiHelper::checkSinhKhac($saoVanNguHanh2 . '-' . $saoVanNguHanh1);
                        if ($checkSinhKhac1 == 1 || $checkSinhKhac2 == 1) {
                            // Tuong sinh
                            echo Yii::t('frontend', 'Tương Sinh (rất tốt)');
                            $diem += 2;
                        } elseif ($checkSinhKhac1 == 2 || $checkSinhKhac2 == 2) {
                            // Tuong khac
                            echo Yii::t('frontend', 'Tương Khắc (không tốt)');
                        } else {
                            // Khong sinh khac
                            echo Yii::t('frontend', 'Bình thường');
                            $diem += 1;
                        }
                        ?>
                    </td>
                </tr>
                <!-- <tr>
					<td colspan="2">
						<?= $diem; ?>
					</td>
				</tr> -->
            </table>

            </div>

        <?php endif; // show full ?>
    <?php endif; // form valid ?>

</div>
<br/>