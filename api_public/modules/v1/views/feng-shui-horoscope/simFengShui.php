<?php

use common\helpers\FengShuiHelper;

$oddEvenArr = FengShuiHelper::getOddEvenNumbers($phoneNumber);

$dateInfoArr = FengShuiHelper::getArrayDateInfo($day, $month, $year);
// echo '<pre>';
// print_r($dateInfoArr);die;
// Menh 
$saoVan = FengShuiHelper::tinhSaoVan($dateInfoArr['nam_am'], $gender);
// echo $saoVan . ' - '. FengShuiHelper::$sao_van_ngu_hanh[$saoVan];

?>

<div class="container-fluid">
    <?= $this->render('showErrors', [
        'errorArr' => $errorArr,
    ]) ?>
    <div class="search-info card ">
        <div class="card-body">
            - <span class="label"><?= Yii::t('frontend', 'Số điện thoại'); ?>:</span> <span
                    class="highlight"><?= $phoneNumber ?></span> <br/>
            - <span class="label"><?= Yii::t('frontend', 'Ngũ hành của dãy số'); ?>:</span> <span
                    class="highlight"><?= Yii::t('frontend', FengShuiHelper::nguHanhChuoiSo($phoneNumber)) ?></span>
            <br/>
            - <span class="label"><?= Yii::t('frontend', 'Thân chủ'); ?>:</span> <span
                    class="highlight"><?= ($gender == 1) ? Yii::t('frontend', 'Nam') : Yii::t('frontend', 'Nữ') ?></span>,
            <?= Yii::t('frontend', 'sinh ngày'); ?> <span
                    class="highlight"><?= $day . '/' . $month . '/' . $year ?></span>
            <br/>
            - <span class="label"><?= Yii::t('frontend', 'Âm lịch'); ?>:</span> <span
                    class="highlight"><?= date('d/m/Y', strtotime($dateInfoArr['output_am'])) ?></span>
            - <?= Yii::t('frontend', 'năm'); ?> <span
                    class="highlight"><?= Yii::t('frontend', $dateInfoArr['can_chi_nam']) ?></span>
            <br/>
            - <span class="label"><?= Yii::t('frontend', 'Theo can chi'); ?>:</span> <?= Yii::t('frontend', 'ngày'); ?>
            <span class="highlight 1"><?= Yii::t('frontend', $dateInfoArr['can_chi_ngay']) ?></span> <?= Yii::t('frontend', 'tháng'); ?>
            <span class="highlight 2"><?= Yii::t('frontend', $dateInfoArr['can_chi_thang']) ?></span> <?= Yii::t('frontend', 'năm'); ?>
            <span class="highlight 3"><?= Yii::t('frontend', $dateInfoArr['can_chi_nam']) ?></span>
            <br/>
            - <?= Yii::t('frontend', 'Tuổi âm dương'); ?>: <span
                    class="highlight"><?= Yii::t('frontend', FengShuiHelper::tuoiAmDuong($year, $gender)); ?></span>
        </div>
    </div>

    <?php if (!$showFull): ?>
        <?= $this->render('viewMoreContent', [
            'contentStatusCode' => $contentStatusCode,
            'contentId' => 'sim',
        ]) ?>

    <?php else: ?>
        <br/>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?= Yii::t('frontend', 'Theo luật âm dương'); ?></h5>
                <p class="card-text">
                    <?= Yii::t('frontend', 'Theo Hà Đồ và Lạc Thư, các số 1, 3, 5, 7, 9 là số mang vận Dương, còn các số 2, 4, 6, 8, 10 mang vận Âm. Số 0 là không có nói trong Hà Đồ và Lạc Thư, nhưng số 10 tận cùng là 0, mang vận Âm; vậy có thể xem số 0 mang vận Âm'); ?>
                </p>
                <table class="phonenumber-list" cellpadding="0" cellspacing="0" width="100%" border="1"
                       bordercolor="#DADADA">
                    <tbody>
                    <tr>
                        <?php
                        $leng = strlen($phoneNumber);
                        for ($i = 0; $i < $leng; $i++):
                            ?>
                            <td><?= $phoneNumber[$i] ?></td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <?php
                        for ($i = 0; $i < $leng; $i++):
                            ?>
                            <td><?= ($phoneNumber[$i] % 2 == 0) ? '-' : '+'; ?></td>
                        <?php endfor; ?>

                    </tr>
                    </tbody>
                </table>
                <span class="label"><?= Yii::t('frontend', 'Số mang vận dương'); ?>:</span> <span
                        class="highlight"><?= count($oddEvenArr['odd']); ?> (<?= number_format($oddEvenArr['odd_percent'], 2) ?>%)</span>
                <br/>
                <span class="label"><?= Yii::t('frontend', 'Số mang vận âm'); ?>:</span> <span
                        class="highlight"><?= count($oddEvenArr['even']); ?> (<?= 100 - number_format($oddEvenArr['odd_percent'], 2) ?>%)</span>
                <br/>
                <span class="label"><?= Yii::t('frontend', 'Cân bằng âm dương trong dãy số'); ?>:</span>

                <?php if ($oddEvenArr['odd_percent'] == 50): ?>
                    <span class="highlight"><?= Yii::t('frontend', 'Rất tốt'); ?></span>
                    <br/>
                    <?= Yii::t('frontend', 'Dãy số đạt âm dương cân bằng nên rất tốt cho bạn'); ?>
                <?php elseif ($oddEvenArr['odd_percent'] >= 40 && $oddEvenArr['odd_percent'] <= 60) : ?>
                    <span class="highlight"><?= Yii::t('frontend', 'Tốt'); ?></span>
                <?php else: ?>
                    <span class="highlight"><?= Yii::t('frontend', 'Không tốt'); ?></span>
                <?php endif; ?>

                <br/>
            </div>
        </div>

        <br/>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?= Yii::t('frontend', 'Ngũ hành bản mệnh'); ?></h5>
                <p class="card-text">
                    <?= Yii::t('frontend', 'Theo triết học cổ Trung Hoa, tất cả vạn vật đều phát sinh từ năm nguyên tố cơ bản và luôn luôn trải qua năm trạng thái được gọi là: Mộc, Hỏa, Thổ, Kim và Thủy (tiếng Trung: 木, 火, 土, 金, 水). Năm trạng thái này, gọi là Ngũ hành (五行), không phải là vật chất như cách hiểu đơn giản theo nghĩa đen trong tên gọi của chúng mà đúng hơn là cách quy ước của người Trung Hoa cổ đại để xem xét mối tương tác và quan hệ của vạn vật.'); ?>
                    <br/>
                    <?= Yii::t('frontend', 'Học thuyết Ngũ hành diễn giải sự sinh hoá của vạn vật qua hai nguyên lý cơ bản (生 - Sinh) còn gọi là Tương sinh và (克 - Khắc) hay Tương khắc trong mối tương tác và quan hệ của chúng.'); ?>
                    <br/>
                    <?= Yii::t('frontend', 'Trong mối quan hệ Sinh thì Mộc sinh Hỏa; Hỏa sinh Thổ; Thổ sinh Kim, Kim sinh Thủy, Thủy sinh Mộc.'); ?>
                    <br/>
                    <?= Yii::t('frontend', 'Trong mối quan hệ Khắc thì Mộc khắc Thổ, Thổ khắc Thủy, Thủy khắc Hỏa, Hỏa khắc Kim, Kim khắc Mộc'); ?>
                </p>
                <br/>
                <?php
                $nguHanhSo = FengShuiHelper::nguHanhChuoiSo($phoneNumber);
                $nguHanhGiaChu = $dateInfoArr['menh_theo_can_chi'];

                $checkSinhKhac = FengShuiHelper::checkSinhKhac($nguHanhSo . '-' . $nguHanhGiaChu);
                ?>
                <?= Yii::t('frontend', 'Ngũ hành của dãy số'); ?>: <span
                        class="highlight"><?= Yii::t('frontend', $nguHanhSo); ?></span>
                <br/>
                <span class="label"><?= Yii::t('frontend', 'Ngũ hành của thân chủ'); ?>:</span> <span
                        class="highlight"><?= Yii::t('frontend', $dateInfoArr['menh_theo_can_chi']) ?>
			(<?= Yii::t('frontend', FengShuiHelper::$menh_ngu_hanh[$dateInfoArr['can_chi_nam']]) ?>)
		</span>
                <br/>

                <?php
                switch ($checkSinhKhac) {
                    case 1:
                        // tuong sinh
                        $checkSinhKhacText = Yii::t('frontend', 'tương sinh');
                        $sinhKhacResult = Yii::t('frontend', 'Rất tốt');
                        break;
                    case 2:
                        // tuong khac
                        $checkSinhKhacText = Yii::t('frontend', 'tương khắc');
                        $sinhKhacResult = Yii::t('frontend', 'Không tốt');
                        break;

                    default:
                        // khong sinh khac
                        $checkSinhKhacText = Yii::t('frontend', 'không tương sinh tương khắc');
                        $sinhKhacResult = Yii::t('frontend', 'Chấp nhận được');
                        break;
                }
                ?>
                <?= Yii::t('frontend', 'Theo luật ngũ hành'); ?> <span
                        class="highlight"><?= Yii::t('frontend', $nguHanhSo); ?></span> <?= $checkSinhKhacText; ?> <?= Yii::t('frontend', 'với'); ?>
                <span class="highlight"><?= Yii::t('frontend', $nguHanhGiaChu); ?></span> ==> <span
                        class="highlight"><?= $sinhKhacResult; ?></span>
            </div>
        </div>


    <?php endif; // show full ?>
</div>
<br/>