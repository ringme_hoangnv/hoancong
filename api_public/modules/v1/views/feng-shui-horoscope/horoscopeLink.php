<?php
use yii\helpers\Url;
use common\helpers\Helpers;
?>
<ul class="luban">
<li><a class="" href="<?= Url::to(['feng-shui-horoscope/luban-ruler']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/ruler.png" />
	<?= Yii::t('frontend', 'Luban Ruler (Free)');?></a></li>

	<li><a class="" href="<?= Url::to(['feng-shui-horoscope/burmese-zodiac-signs']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/horos-icon.png"><?= Yii::t('frontend', 'Burmese Zodiac Signs');?></a></li>
    <li><a class="" href="<?= Url::to(['feng-shui-horoscope/zodiac-signs']); ?>">
            <img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/horos-icon.png"><?= Yii::t('frontend', 'Astrology Zodiac Signs');?></a></li>
<li><a class="" href="<?= Url::to(['feng-shui-horoscope/sim-feng-shui']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/sim.png" />
	<?= Yii::t('frontend', 'SIM Feng Shui');?>
		
	</a></li>
<li><a class="" href="<?= Url::to(['feng-shui-horoscope/couple']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/love.png" />
	<?= Yii::t('frontend', 'Feng Shui for couples');?>
	</a></li>
<li><a class="" href="<?= Url::to(['feng-shui-horoscope/gender-predictor']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/baby.png" />
	<?= Yii::t('frontend', 'Boy or girl? Gender predictor');?></a></li>
<li><a class="" href="<?= Url::to(['feng-shui-horoscope/ages-and-colors']); ?>">
	<img class="tuvi-icon" src="/<?= Helpers::getCurrentThemeName() ?>/images/love.png" />
	<?= Yii::t('frontend', 'Ages and colors in Feng Shui');?></a></li>

</ul>