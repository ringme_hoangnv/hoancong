<?php 
use yii\helpers\Url;
?>
<?php if ($contentStatusCode != 1): ?>
<br />
<div class="<?= ($divClass)? $divClass: 'view-more'?>">
    <?php // \common\helpers\Helpers::getTextByContentStatus($contentStatusCode); ?>
    <?= Yii::t('frontend', 'Know your future, your luck with MyZartar!') ?>
    <br />
    <?= Yii::t('frontend', 'On your first time registration, you can use service for <b>01 week FREE</b> of charge and will get 01 Lucky Code for Myluck (<a target="_blank" href="{url}">www.myluck.asia</a>) where you can get the chance to be a winner of daily and monthly amazing cash prizes up to 600,000Ks.', [
        'url' => 'http://myluck.asia'
    ]) ?>
    <br />
    <br />
    <br />
    <br />
    <?php if(Yii::$app->language == 'en'): ?>
        <center>Terms & Conditions</center>
        1. By choosing package, you agree to our terms and conditions<br />
        2. You are subscribing to an annual subscription and you will be charged in installments of 499ks/week until you unsubscribe.<br />
        3. T&C will not apply if you unsubscribed or do not recharge for 30 consecutive days.<br />
        4. If you do not recharge for 30 consecutive days, your subscription will be cancelled automatically.<br />
        5. To unsubscribe, send OFF to 599 or dial *599#.<br>
        6. For more details, access link <a style="color:gold" href="<?= Url::to(['site/terms'])?>" target="_blank">TERMS & CONDITIONS</a><br>
    <?php else: ?>
        <center>စည်းမျဉ်းစည်းကမ်းများ</center>

        ၁။ ပက်ကေ့ရွေးချယ်ခြင်းအားဖြင့် ကျွနု်ပ်တို့၏ စည်းမျဉ်းစည်းကမ်းများနှင့် အခြေအနေကိုသဘောတူသည်။
        <br>
        ၂။ သင်သည်နှစ်စဉ်ကြေးသွင်းနေပြီး ဝန်ဆောင်မှုကို ပယ်ဖျက်ပြီးသည်အထိ တစ်ပါတ်လျှင် ၄၉၉ကျပ် ကျသင့်မည်ဖြစ်ပါသည်။
        <br>
        ၃။ အကယ်၍ သင်သည် ဝန်ဆောင်မှုကိုပယ်ဖျက်ပါက သို့မဟုတ် ရက်ပေါင်း၃၀ ဆက်တိုက်မကောက်ခံပါက ဤစည်းကမ်းများသည် အသုံးမဝင်တော့ပါ။
        <br>
        ၄။ ရက်ပေါင်း ၃၀ဆက်တိုက်ငွေမသွင်းပါက သင်၏စာရင်းပေးသွင်းခြင်းကို အလိုအလျှောက်ဖျက်သိမ်းလိမ့်မည်။
        <br>
        ၅။ အချိန်မရွေးပယ်ဖျက်လိုလျှင် 599 သို့ OFF ဟုပို့ပါ။
        <br>
        ၆။ အသေးစိတ်သိလိုလျှင် လင့်ကို ဝင်ပါ။ <a style="color:gold" href="<?= Url::to(['site/terms'])?>" target="_blank">TERMS & CONDITIONS</a>
        <br>
    <?php endif; ?>
    </div>
</div>
<br />

<?php endif; ?>

