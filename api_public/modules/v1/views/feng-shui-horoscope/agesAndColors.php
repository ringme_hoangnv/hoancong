<?php 
use common\helpers\FengShuiHelper;

$dateInfoArr = FengShuiHelper::getArrayDateInfo($day, $month, $year);
// echo '<pre>';
// print_r($dateInfoArr);die;

?>

<?= $this->render('_seo', [
    'title' => Yii::t('frontend', 'Ages and colors in Fengshui, Find Your Best Personal Feng Shui Colors'),
    'description' => Yii::t('frontend', 'Choose the best personal feng shui colors to strengthen your energy and even attract good luck'),
    'keywords' => 'khmer feng shui age and color, cambodian fengshui, feng shui colors',
]); ?>

<div class="container-fluid">
	
	<?= $this->render('showErrors', [
			'errorArr' => $errorArr, 
		])?>
	<?php if ($formValid): ?>
	<div class="search-info card ">
		<div class="card-body">
			- <span class="label"><?= Yii::t('frontend', 'Thân chủ');?>:</span> <span class="highlight"><?= ($gender == 1)? Yii::t('frontend', 'Nam'): Yii::t('frontend', 'Nữ')?></span>, 
			<?= Yii::t('frontend', 'sinh ngày');?> <span class="highlight"><?= $day. '/'. $month. '/'. $year ?></span>
			<br />
			- <span class="label"><?= Yii::t('frontend', 'Âm lịch');?>:</span> <span class="highlight"><?= date('d/m/Y', strtotime($dateInfoArr['output_am'])) ?></span> - <?= Yii::t('frontend', 'năm');?> <span class="highlight"><?= Yii::t('frontend', $dateInfoArr['can_chi_nam']) ?></span>
			<br />
			- <span class="label"><?= Yii::t('frontend', 'Theo can chi');?>:</span> <?= Yii::t('frontend', 'ngày');?> <span class="highlight"><?= Yii::t('frontend', $dateInfoArr['can_chi_ngay']) ?></span> <?= Yii::t('frontend', 'tháng');?> <span class="highlight"><?= Yii::t('frontend', $dateInfoArr['can_chi_thang']) ?></span> <?= Yii::t('frontend', 'năm');?> <span class="highlight"><?= Yii::t('frontend', $dateInfoArr['can_chi_nam']) ?></span>
			<br />
			- <?= Yii::t('frontend', 'Tuổi âm dương');?>: <span class="highlight"><?= Yii::t('frontend', FengShuiHelper::tuoiAmDuong($year, $gender));?></span>
		</div>
	</div>
	
	<?php if(!$showFull): ?>
		<?= $this->render('viewMoreContent', [
			'contentStatusCode' => $contentStatusCode,
                'contentId' => 'ages_color',
		])?>
		
	<?php else: ?>
		<?php 
		$saoVan =  FengShuiHelper::tinhSaoVan($dateInfoArr['nam_am'], $gender);
		?>
		<div id="ages-and-colors" class="row">
			<div class="col-sm-6">
				
				<div class="card">
				  <div class="card-header">
				    <?= Yii::t('frontend', 'Về Niên mệnh <span class="highlight2">(coi trọng phần này hơn)</span>');?>
				  </div>
				  <div class="card-body">
				  	<div class="card-title highlight"><?= Yii::t('frontend', $saoVan) . ' - '. Yii::t('frontend', FengShuiHelper::$sao_van_ngu_hanh[$saoVan])?></div>
				    <?php 
						$saoVanNguHanh = FengShuiHelper::$sao_van_ngu_hanh[$saoVan]; 
						$colorArr = FengShuiHelper::$menh_color[$saoVanNguHanh]; 
						
						foreach ($colorArr as $key => $colorStr): 
							$arr = explode(',', $colorStr); 

					?>	
						<?php if ($key == 0):?>
							<?= Yii::t('frontend', 'Màu hợp với tuổi');?>
						<?php elseif($key == 1):?>	
							<?= Yii::t('frontend', 'Màu tương sinh (sinh) tuổi');?>
						<?php else: ?>	
							<?= Yii::t('frontend', 'Tuổi tương sinh (sinh) màu');?>
						<?php endif;?>
						<table class="table tb-table" cellpadding="6" cellspacing="5" align="center">
							<tr>
							<?php
								$countColor = count($arr); 
								foreach ($arr as $i => $value): 
							?>
								<?php if ($countColor == 4):?>
									<td width="25%" bgcolor="<?= trim($value)?>">&nbsp;</td>
								<?php else: ?>
									<td width="100%" bgcolor="<?= trim($value); ?>" style="border:1px solid #dee2e6">&nbsp;</td>
								<?php endif; ?>
							<?php endforeach; // mau ?>
							</tr>
						</table>
					<?php endforeach; // tuong sinh ?>
				  </div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card">
				  <div class="card-header">
				    <?= Yii::t('frontend', 'Về Mệnh');?>
				  </div>
				  <div class="card-body ">
				  	<div class="card-title highlight"><?= Yii::t('frontend', $dateInfoArr['menh_ngu_hanh']);?></div>
			    	<?php 
						$menhCanChi = $dateInfoArr['menh_theo_can_chi']; 
						$colorArr = FengShuiHelper::$menh_color[$menhCanChi]; 
						
						foreach ($colorArr as $key => $colorStr): 
							$arr = explode(',', $colorStr); 

					?>	
						<?php if ($key == 0):?>
							<?= Yii::t('frontend', 'Màu hợp với tuổi');?>
						<?php elseif($key == 1):?>	
							<?= Yii::t('frontend', 'Màu tương sinh (sinh) tuổi');?>
						<?php else: ?>	
							<?= Yii::t('frontend', 'Tuổi tương sinh (sinh) màu');?>
						<?php endif;?>
						<table class="table tb-table" cellpadding="6" cellspacing="5" align="center">
							<tr>
							<?php
								$countColor = count($arr); 
								foreach ($arr as $i => $value): 
							?>
								<?php if ($countColor == 4):?>
									<td width="25%" bgcolor="<?= trim($value)?>">&nbsp;</td>
								<?php else: ?>
									<td width="100%" bgcolor="<?= trim($value); ?>" style="border:1px solid #dee2e6">&nbsp;</td>
								<?php endif; ?>
								
							
							<?php endforeach; // mau ?>
							</tr>
						</table>
					<?php endforeach; // tuong sinh ?>
				  </div>
				</div>

				

			</div>
		</div>
	<?php endif; // show full ?>
	<?php endif; // form valid ?>
	
</div>