<div class="box-title box-title2 container-fluid">
    <h2 class="">Terms and Conditions (MyZartar)</h2>
</div>
<div class="container-fluid">
<p>Thank you for using Mytel&rsquo;s digital and telecom products and services and welcome to our MyZartar service <a href="https://myzartar.asia">www.myzartar.asia</a> (the &ldquo;Site&rdquo;). Our service offers maharbote, zodiac, fengshui readings that are updated daily for users (&ldquo;Services&rdquo;).</p>

<p>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY. You should familiarize yourself with these Terms and Conditions and if you do not agree with them you should not proceed any further on the Site or with registration. When you use our technologies, products and services, you agree to be bound by these Terms and Conditions, which can be found in this section. The term &ldquo;you&rdquo; or &ldquo;your&rdquo; includes any of your subsidiaries, affiliates, employees and parent or legal guardian.</p>

<p><strong>AUTHORITY</strong></p>

<p>By using our products and services, you represent that you are at least 18 years old. Persons who are under the age of 18 may only use our products and service with legal parental or guardian consent. Accordingly, you agree what you are at least 18 years of age or older or obtained legal parental or guardian consent, and are fully able and competent to enter into the Terms and Conditions; otherwise, please exit the site.</p>

<p><strong>REGISTRATION, PASSWORD AND SECURITY</strong></p>

<p>Mytel are free to choose how we provide a Service and the technology used to provide it. You agree to provide us with all information, assistance and consents reasonably requested to enable us, our contractors or suppliers to obtain authorizations, licenses, or contents that may be required to provide Services to you and to install our Equipment at your premises.</p>

<p>Whenever you provide us information on our Site, you agree that the information you provide is accurate and that you will maintain your information up-to-date. You are responsible for maintaining the security of any confidentiality of your user names, passwords and registration information, and you are solely responsible for any use (authorized or not) of your accounts. Through your provision of your information and use of the Sites you consent us to the collection and use of the information that you submit through the Sites.</p>

<p>You agree to notify immediately Mytel of any unauthorized activity regarding any of your accounts of any other breach of security so that we can prevent your mobile Services from being used. Notwithstanding, Mytel may rely on the authority of anyone accessing your account or using your password. We make no representations or warranties concerning the security or content of information passing over our Network. You are responsible for reviewing the classification information for any content you access using our Services.</p>

<p>Mytel in its discretion, may with or without notice, suspend or terminate your access to our Site and refuse any and all current or future use of our Site (or any portion thereof) if (i) you provide, or we have reasonable grounds to suspect, any information that is untrue, inaccurate, incomplete, or out-of-date or (ii) to ensure the security of the Sites and your account.&nbsp;</p>

<p><strong>ACCEPTABLE USE</strong></p>

<p>You agree to use our Site and the Content (whether provided by us or others), as well as any Software provided in connection with the Site, in a manner consistent with all applicable laws and regulations. Additionally, you will not take any of the following actions with respect to our Site, related Software, or Content, nor will you use our Site or related Software in any manner that:</p>

<p>Is unlawful, harmful to minors, threatening, harassing, abusive, defamatory, slanderous, vulgar, fraudulent, gratuitously violent, obscene, pornographic, deceptive, indecent, lewd, libelous, invasive of another&rsquo;s privacy, or radically, ethnically or otherwise offensive, hateful or abusive;</p>

<p>Infringes or misappropriates Mytel&rsquo;s or a third party&rsquo;s patent, trademark, trade secret, copyright, confidentiality or other intellectual property or other rights;</p>

<p>Removes any proprietary notices or labels on the Content;</p>

<p>Advocates or solicits violence, criminal conduct or the violation of any local, state, national or international law or the rights of any third party;</p>

<p>Is deceptive in any way, such as creates a false identify for the purpose of misleading others, offer to sell fraudulent goods or contains an impersonation of any person or entity or misrepresents an affiliation with a person or entity;</p>

<p>Disrupts, interferes or inhibits or adversely affect the ability of other people or systems from enjoying the Sites or other affiliated or linked websites, material, contents, products and/or services.</p>

<p>Use any automated script or routine, including robot, spider, offline readers, bots, web crawlers or other such programmatic or automatic device, including but not limited to automated dial-in or inquiry devices, to systematically collects, obtain and use information from the Site, to monitor or copy any portion of the Site, products and/or services or sends more request messages to servers in a given period of time than a human can reasonable produce in the same period of using a conventional online web browser;</p>

<p>Attempts to disable, bypass, modify, defeat, disrupt, damage, overburden or otherwise circumvent any of the digital rights management or other security related tools incorporated into the software, hardware, network, server or communications systems or equipment, or any Content or the Sites;</p>

<p>Systematically collects and uses any Content including the use of any data mining, or similar data gathering and extraction methods;</p>

<p>Transmit or facilitate any unsolicited or unauthorized advertising, telemarketing, promotional materials, &ldquo;junk mail,&rdquo; unsolicited commercial or bulk email, or fax;</p>

<p>Reproduces, duplicates, redisplays, frames, makes copies of, or resells the Service or Content for any purpose.</p>

<p>Additional access and use restrictions may appear elsewhere on the Sites. You agree to abide by such additional restrictions. The Sites are intended solely for your private and personal use on your computer. Any other use or any attempt to use the Sites or Content or Service for commercial or other purposes is strictly prohibited.</p>

<p>If the use of a Site or Content or Service by you or anyone using your account, in Mytel&rsquo;s sole discretion, violates these Terms or any other Mytel agreement or policy, is objectional or unlawful, or interferes with, disrupts or degrades the functioning or use of the Internet or the Mytel network by Mytel or other users then, without limiting any other right or remedy Mytel may have, Mytel may suspend, deny or restrict your access to any Site or Service or Content (and to take any other action Mytel deems appropriate to protect Mytel, our users and other Internet users).</p>


    <p><strong>DATA POLICY</strong></p>
    <p>
    Mytel is allowed to share your information securely, including internally within the Mytel company, Mytel's shareholders companies and externally our partners in accordance with this policy. Your information may, for example, be transfered or transmitted to, or stored and processed in the Myanmar or other countries outside of where you live for the purposes as described in this policy. These data transfers are necessary to provide better the services and products to you.
    </p>

<p><strong>PAYMENT AND BILLING</strong></p>

<p><strong>1. Rates and Charges</strong></p>

<p>Pricing: MyZartar service has provided 01-year package with the charging cycle at 499Ks for every 07 days or 1999Ks for every 30days (including Commercial Tax 5%). During 01-year, Customer can stop service at any time by sending OFF to 599. After using 01 year, the service will be stopped automatically and if Customer want to use again the service, they have to subscribe again. The Customer shall check be careful before make payment and customers will only be charged for Services which the Customer has actually applied for and/or used, subject to the payment of all applicable Taxes thereto. For the avoidance of doubt, Customer acknowledges and agrees that due to the nature of certain Services, including with respect to the charging of Fees for the use of pre-paid data Services, there may be cases where Customer&rsquo;s Account balance may go below zero, in which case Customer shall be liable for the deficit in accordance with the terms herein.</p>

<p>Rate Adjustments: Mytel has full right to adjust the pricing at any time and it can be made payment after receive confirmation from customers.</p>

<p>Taxes are all applicable taxes, duties, levies and other similar charges however designated, imposed under and by virtue of any Law or the law of any jurisdiction outside Myanmar with respect to, or in connection with the provision of any Service, or on any Fees or payment due and payable to Mytel from the Customer;</p>

<p><strong>2. Orders, Billing and Payment</strong></p>

<p>Billing:</p>

<p>MYTEL may issue a Bill in respect of the Services on a monthly basis or at such other intervals as MYTEL may consider appropriate or convenient and subject to any directions issued by the Myanmar government and as notified to the Customer. These Bills may be issued by MYTEL to the Customer by any of the following methods:&nbsp;</p>

<p>Istantaneous electronic communications, including but not limited to electronic mail, facsimile transmission or Short Message Service (SMS), to such email addresses or telephone/mobile number appearing in any record maintained by MYTEL or from any communication by the Customer to MYTEL as regulated in Specific Term; or</p>

<p>Delivery by hand, courier or post to the last recorded address of the Customer appearing in MYTEL maintained records or from the last received communication by the Customer to MYTEL as regulated in Specific Term.</p>

<p>Any such Bill issued and forwarded in accordance with these General Terms shall be deemed to have been received by the Customer:&nbsp;</p>

<p>On the date and at the time it was so delivered or left at that address;</p>

<p>On the date successfully delivered by electronic communication, including but not limited to electronic mail, facsimile transmission or Short Message Service (SMS) which Mytel can prove the evidence of successful delivery if requirement.</p>

<p>On the seventh (7th) day after it was posted by Mytel to any address in Myanmar;</p>

<p>On the fourteenth (14th) day after it was posted by Mytel to any address outside Myanmar; or</p>

<p>Within twenty-four (24) hours from transmission by Mytel by e-mail or facsimile transmission or other instantaneous electronic communications.</p>

<p>Payment Terms.</p>

<p>Where amounts payable by the Customer are delinquent or in delay, Mytel may, at its sole discretion:&nbsp;</p>

<p>Charge the Customer interest at the Prescribed Rate on any outstanding delinquent amount; and/or</p>

<p>Without need of prior notice or consent from the Customer, debit any Account with respect to such interest.</p>

<p>The Customer shall pay the Fees and any other sums due or payable to Mytel in accordance with the Specific Terms. In such cases where the Service is terminated (whether by Mytel or the Customer), Mytel determines that the Customer has abandoned the Service, the Customer&rsquo;s use of the Service has been significantly greater than the Customer&rsquo;s average use thereof, or any other instance where Mytel deems it necessary, the Customer shall pay to Mytel the Fees and any other sums due or payable, including Taxes, immediately upon demand.&nbsp;</p>

<p>The Customer is obliged to pay Mytel the total amount shown or stated as due or payable to Mytel in a Bill within the period prescribed therein, or in the absence of said period, the Customer shall pay Mytel the total amount shown or stated as due or payable to Mytel in a Bill on or before the twenty fifth (25th) day of the succeeding month, or the next business day, or such other later date as determined by Mytel and notified in writing to the Customer after the date that such Bill is deemed to have been received pursuant to Clause 4.4, by the Customer (such relevant period hereinafter referred to as the &ldquo;Due Date&rdquo;).&nbsp;</p>

<p><strong>3. Disputed Charges</strong></p>

<p>You are responsible for all use of your Services, including use by any third parties, and all charges however incurred, except:&nbsp;</p>

<p>Charges incurred because of our error or negligence; or</p>

<p>Charges for unauthorized use (except where such use is the result of your negligence, carelessness, breach of contract, or failure to comply with our reasonable requirements, or by a third party within your reasonable control).</p>

<p>We will not pay interest or any credit balance or security deposit in any of your accounts and can use those amounts to pay any of your outstanding Charges or any of your Services.</p>

<p>If you have an outstanding debt with us, we have the right to transfer that debt to another party who will then have the right to collect that debt from you.</p>

<p><strong>MANAGEMENT OF YOUR DATA</strong></p>

<p>You agree that you are solely responsible for maintaining the security of your computer(s) and all personal and other data, including without limitation, encryption of data. WE STRONGLY RECOMMEND THE UPDATED USE OF COMMERCIAL ANTI-VIRUS SOFTWARE. Mytel is not responsible for back-up or restoration of your information. We reserve the right to: (a) use, copy, display, store, transmit and reformat data transmitted over our network and to distribute such content to multiple Mytel servers for maintenance purposes; and (b) block or remove any unlawful content you store on or transmit to or from any Mytel server. We do not guarantee the protection of your content or data located on our servers or transmitted across our network (or other networks) against loss, alteration or improper access.&nbsp;</p>

<p><strong>ENFORCEMENT</strong></p>

<p>Mytel reserves the right to access, use, provide or disclose account, transaction and user information, including email, to third parties as required or permitted by laws (e.g., a lawful interference or interception) and to cooperate with law enforcement authorities in the investigation of any criminal or civil matters. Such cooperation may include, but is not limited to, monitoring of the Mytel network consistent with applicable law. In addition, you agree Mytel&rsquo;s right to access, use, provide or disclose your information based on Mytel&rsquo;s reasonable judgement that disclosure is necessary, or to enforce or apply our agreements (including these Terms), to initiate, render, bill, and collect for services, to protect our rights or property, or to protect users of Mytel&rsquo;s services, the Site and other persons or entities from fraudulent, abusive, or unlawful use of the Site or any such services.&nbsp;</p>

<p><strong>TRADEMARKS</strong></p>

<p>The Mytel&rsquo;s Trademarks (including but not limited to Mytel&rsquo;s name and logos and all related product and service names, design marks and slogan) are owned by Mytel&rsquo;s Intellectual Property and may not be used in any manner without the prior written permission of Mytel. All other trademarks and service marks are the property of their respective owners.&nbsp;</p>

<p><strong>THIRD-PARTY PRODUCTS AND SERVICES</strong></p>

<p>Where have no control over the Content that you access on your Device. Content may be: (1) unsuitable for children/minors; (2) unreliable or inaccurate; or (3) offensive, indecent, or objectionable. You&rsquo;re solely responsible for evaluating the Content accessed by you or anyone through your Device or Services. We are not responsible for any Content, any damage caused by any Content that you access through your Services, that you load on your Device, or that you request that our representatives access or load on your Device. If we provide you storage for content that you have purchased, then we may delete the Content without notice or place restrictions/limits on the use of storage areas. Content stored on a Device, transmitted over our networks, or stored by Mytel may be deleted modified, or damaged. Content provided by our vendors or third parties may be cancelled or terminated at any time without notice to you, and you may not receive a refund for any unused portion of the Content.&nbsp;</p>

<p>The Sites may contain links to other websites not operated by Mytel. The links are provided for your convenience. Many Services and applications offered through our Sites or your device may be provided by third parties. When leaving the Site, you should carefully review the applicable terms and policies, including privacy and data gathering practices, of that third-party website. Generally, the terms will come from the app developer, software licensor, etc. which may be Mytel or third parties. Except for Mytel branded information, products or services that are identified as being supplied by Mytel, Mytel are not responsible for the contents, links or privacy of any linked website. Access to any other websites linked to the Sites is at your own risk. Mytel does not operate, control, or endorse any information, products or services on the Sites or accessible through the Site in any ways. When you use these items, you are agreeing that the third party may access, collect, use or disclose your personal information and that Mytel is not responsible for examining or evaluating, and Mytel does not warrant the offerings of, any of these businesses or individuals or the content of their websites. Mytel does not assume any responsibility or liability for the actions, product, and content of all these and any other third parties.</p>

<p><strong>DISCLAIMER OF WARRANTIES</strong></p>

<p>YOU EXPRESSLY UNDERSTAND AND AGREE THAT:</p>

<p>Although every effort has been made to provide accurate information on these pages. The Site, software, content or information and the Services are provided on an &ldquo;as is&rdquo; and &ldquo;as available&rdquo; and &ldquo;with all faults&rdquo; basis. Neither Mytel, nor any of its employees, nor any member of Mytel, their suppliers, agents, licensors, nor any of their employees, make any warranty, expressed or implied, or assume any legal liability (to the extent permitted by law) or responsibility for (i) the requirements, (ii) materials, software or content available for download from the sites are free of viruses, (iii) the quality of any products, services, (iv) the suitability, reliability, timeliness, accuracy or completeness, of the data, information, Services or any part thereof contained on the Site or in the Services obtained by you through our Site will meet your expectations .</p>

<p><strong>LIMITATION OF LIABILITY</strong></p>

<p>Mytel shall use its reasonable endeavours to ensure the maintenance and availability of the Site and the Services but availability may be affected by your Equipment, other communications networks, too many people trying to use the Mytel network or the internet at the same time or other causes of interference and may fail or require maintenance without notice.</p>

<p>Neither Mytel nor any member of Mytel shall be liable for any special, indirect or consequential damages or any damages whatsoever, whether in an action of contract, negligence or other tortuous action, arising out of or in connection with the performance of or use of Services available on the Site an in particular, but without limitation to the foregoing, Mytel specifically excludes all liability whatsoever in respect of any loss arising as a result of:</p>

<p>Loss caused by you, or any loss that results from your failure to take reasonable steps to avoid or minimize your loss;</p>

<p>Loss of data;</p>

<p>Loss caused by something beyond our control, for example a failure by a Network Operator, an act of God, earthquake, terrorism, strike, shortage of suitable labour or materials or any other event beyond our control.</p>

<p>If we are liable to you for direct losses arising from any breach of this Agreement or for our negligence, our obligation to pay any damages or losses is limited to $20 for a number of incidents within any 12 months period. This limitation does not apply to any loss or damage caused by fraud, willful breach or willful damage. In addition, we are not liable for:</p>

<p><strong>INDEMNITY</strong></p>

<p>You agree to indemnify, defend and hold harmless Mytel from and against all claims for losses, expenses, damages and costs, including reasonable attorneys&rsquo; and experts&rsquo; fees, arising from or related to claims made by any third-party (collectively, &ldquo;Claims&rdquo;) due to or arising out of (a) your use of the Sites or Services or Content (or use of Sites or Service or Content by any parties who use your computer, with or without your permission), (c) your violation of these Terms and Conditions and/or your violation of any laws or regulations or the rights of another through the use of the Sites or Services or Content (or such violations of any parties who use your computer, with or without your permission). These indemnity obligations will survive termination of your relationship with Mytel or your ceasing to use the Sites or Services or Content. Mytel reserves the right to assume the defense and control of any matter subject to indemnification by you, in which event you will cooperate with Mytel in asserting any available defenses.</p>

<p><strong>TERMINATION/SUSPENSION</strong></p>

<p>Mytel may elect to suspend or vary the Services and the Site immediately and without prior notice at any time for repair or maintenance work or in order to upgrade or update the Site and the Services or for any other reason whatsoever.</p>

<p>You agree that Mytel may immediately terminate or suspend your account, any associated email address, and access to all or any part of the Sites or change your password without notice. Cause for such termination, suspension or change shall include, but not be limited to,</p>

<p>Breaches or violations of these Terms and Conditions or other incorporated agreements or guidelines,</p>

<p>Harassing/threatening/abusing/offending or being excessively impolite to our employees or agents,</p>

<p>Requests by lawful enforcement or other government agencies,</p>

<p>A request by you (self-initiated account deletions),</p>

<p>Discontinuance or material modification to the Sites (or any part thereof),</p>

<p>Unexpected technical or security issues or problems,</p>

<p>Extended periods of inactivity,</p>

<p>Providing false, inaccurate, dated, or unverifiable information, including identification or credit information,</p>

<p>Becoming insolvent or bankrupt;</p>

<p>Failing to use our Services or maintain an active Device,</p>

<p>If we believe the action protects our interests, any customer&rsquo;s interests, or our network, and/or</p>

<p>Engagement by you in fraudulent or illegal activities or interfering with our operations.</p>

<p>Termination of your account includes (or, if Mytel elects instead to suspend your account, may include any one or more of the following):</p>

<p>Removal of access to all offerings within the Sites,</p>

<p>Deletion of your password and all related information, files and other content associated with or inside you account (or any part thereof) and</p>

<p>Barring of further use of the Sites.</p>

<p>You agree that all terminations and suspensions for cause shall be made in Mytel&rsquo;s sole discretion and that Mytel shall not be liable to you or any third party for any termination or suspension of your account, loss of storage, any associated email address, or access to the Sites at any time for any reason and without notice to you in its sole discretion.</p>

<p><strong>FORCE MAJEURE</strong></p>

<p>Neither party will be responsible for any delay, interruption or other failure to perform under the Terms and Conditions due to acts, events or causes beyond the reasonable control of the responsible party (a &ldquo;Force Majeure Event&rdquo;). Force Majeure Events include: natural disasters; wars, riots, terrorist activities; cable cuts by third parties, and other acts or inactions of third parties; fires; embargoes and labor disputes; and court orders and government decrees.</p>

<p><strong>DISPUTE RESOLUTION AND ARBITRATION</strong></p>

<p>Negotiation. If, for some reason, we cannot resolve your concern to your satisfaction through calls to our customer care, you and Mytel each agree to try to resolve those disputes in good faith after you provide written notice of the dispute as set forth below. If we cannot resolve the dispute, you and Mytel agree that we will resolve the dispute through individual binding arbitration.&nbsp;</p>

<p>Arbitration. Instead of suing in court, you and Mytel agree to arbitrate all Disputes (as defined below) on an individual, non-representatives, basis. In arbitration, there is no judge. Instead, a neutral third-party arbitrator resolves Disputes in a less formal process than in court. In arbitration, the arbitrator must follow the terms of the Terms and Conditions and can award damages and relief, including any attorneys&rsquo; fees authorized by law.&nbsp;</p>

<p>&ldquo;Disputes&rdquo; include, but are not limited to, any claims or controversies against each other in any way related to or arising out of our Services, including billing services and practices, policies, contract practices and service, privacy, or advertising claims, even if the claim arises after Services have terminated. Disputes also include, but are not limited to, claims that: (a) you or an authorized or unauthorized user of the Services bring against our employees, agents, affiliates, or other representatives; (b) you bring against a third party, that are based on, relate to, or arise out of in any way our Services or the claims in any way related to or arising our of any aspect of the relationship between you and Mytel, whether based in contract, tort, statute, fraud, misrepresentation, advertising claims or any other legal theory.</p>

<p><strong>DISPUTE NOTICE</strong></p>

<p>Notices required under the Agreement must be submitted in writing in any address listed in the Terms and Conditions. In the case for a dispute, notices also must be sent to:</p>

<p>Telecom International Myanmar Company Limited</p>

<p>Attn:</p>

<p>No. 61, 63 Zoological Garden Road, Dagon Township, Yangon, Myanmar</p>

<p>Email:</p>

<p><strong>CHANGES TO THE TERMS AND CONDITIONS OR SITE</strong></p>

<p>Mytel may change, modify or withdraw (&ldquo;change&rdquo;) all information or any Service posted on the Site and/or the Terms and Conditions from time-to-time without notice by posting such revisions to the page location. You agree to visit the Site and/or Terms and Conditions periodically to be aware of and review any such revisions.</p>

<p>Changes to these Terms and Conditions and any information shall be effective upon posting. You understand and agree that you use the Services after the date on which the Terms have changed, Mytel will treat your use of the Services as acceptance of the updated Terms, with prospective effect.</p>

<p>Mytel reserves the right to change, modify or discontinue, temporarily or permanently, the Site (or any portion thereof), including any and all content contained on the Site, at any time without notice. You agree that Mytel shall not be liable to your or to any third party for any modification, suspension or discontinuance of the Site (or any portion thereof).</p>

<p><strong>SEVERABILITY</strong></p>

<p>These Terms and Conditions are severable in that if any provision is determined to be illegal or unenforceable by any court of competent jurisdiction such provision shall be deemed to have been deleted without affecting the remaining provisions of these Terms and Conditions.</p>

<p><strong>GENERAL</strong></p>

<p>Governing Law &ndash; These Terms and Conditions are governed by and construed in accordance with the laws of the Union of the Republic of Myanmar.</p>

<p>Waiver &ndash; Mytel&rsquo;s failure to exercise any particular right or provision of these Terms and Conditions shall not constitute a waiver of such right or provision unless acknowledged and agreed to by Mytel in writing.</p>

<p>Representations &ndash; You acknowledge and agree that in entering into these Terms and Conditions you do not rely on, and shall have no remedy in respect of, any statement, representation, warranty or understanding (whether negligently or innocently made) of any person (whether party to these Terms and Conditions or not) other than as expressly set out in these Terms and Conditions as a warranty. Nothing in this Clause shall, however, operate to limit or exclude any liability for fraud.</p>

<p>Assignment &ndash; You in entering into these Terms and Conditions undertake that you will not assign, re-sell, sub-lease or in any other way transfer your rights or obligations under these Terms and Conditions or part thereof. Contravention of this restriction in any way, whether successful or not, will result in your access to the Site and your right to use the Services being terminated by Mytel forthwith. Mytel may assign these Terms and Conditions in whole or in part to any third party at its discretion.</p>

<p><strong>INTERPRETATION</strong></p>

<p>&ldquo;Content&rdquo; means any type of media (include but not limited as text, pictures, videos, music, game&hellip;) offered or provided by Mytel;</p>

<p>&ldquo;Customer&rdquo; means any person who applies for, subscribes to, or makes use of Mytel&rsquo;s Services;</p>

<p>&ldquo;Services&rdquo; has the meaning given to it in Clause, is any telecommunications or other service (including the supply, rental or installation of any equipment) offered or provided by Mytel;</p>

<p>&ldquo;Terms and Conditions&rdquo; means the rules by which one must agree to abide in order to use a service.</p>

<p>&ldquo;You&rdquo; means the user that enters into these Terms and Conditions (and &ldquo;your&rdquo; shall be construed accordingly).</p>
<br />
</div>