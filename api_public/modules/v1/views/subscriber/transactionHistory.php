<?php

?>
    <div id="post-list" class="">
        <?php if (count($listPager)): ?>
            <div class="card-bodyxx table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <th><?= Yii::t('frontend', 'Thời gian'); ?></th>
                    <th><?= Yii::t('frontend', 'Loại giao dịch'); ?></th>
                    <th><?= Yii::t('frontend', 'Phí (cents)'); ?></th>
                    <th><?= Yii::t('frontend', 'Trạng thái'); ?></th>
                    </thead>
                    <?php foreach ($listPager as $key => $trans): ?>
                        <tr>
                            <td><?= date('d/m/Y H:i:s', strtotime($trans->CHARGE_TIME)) ?></td>
                            <td><?= $trans->getChargeTypeName(); ?></td>
                            <td><?= number_format($trans->MONEY / 10000); ?></td>
                            <td><?= ($trans->CHARGE_STATUS == 0) ? Yii::t('frontend', 'Thành công') : Yii::t('frontend', 'Thất bại'); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class=" col-md-12">
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'maxButtonCount' => 5,

                ]);
                ?>
            </div>
        <?php else: ?>
            <?= Yii::t('frontend', 'Chưa có giao dịch nào!'); ?>
        <?php endif; ?>
    </div>

    <br/>
    <br/>