<?php

use yii\helpers\Url;

?>

<br/>
<div class="box-title box-title2 container-fluid">
    <h2 class=""><?= Yii::t('frontend', 'Package'); ?></h2>
    <p class="box-desc">
        <?= Yii::t('frontend', 'Register {service_name} service to view horoscope and forturn telling content and get your fortune every day', [
            'service_name' => Yii::$app->params['service_name']
        ]); ?>
    </p>
</div>

<?php if (!$serviceSub || !$serviceSub->isActivatingService()): ?>
    <?php
    $modalTitle = Yii::t('frontend', 'Đăng ký dịch vụ');
    $btnName = 'register';
    ?>
    <?php foreach ($packageSubList as $package): ?>
        <div class="card">
            <div class="card-body">
                <?= Yii::t('frontend', '{package_name} (Fee: {pkg_fee} {money_unit}/{per_day} days)', [
                    'package_name' => $package->NAME,
                    'pkg_fee' => $package->PRICE,
                    'per_day' => $package->DAY_ADD,
                    'money_unit' => Yii::$app->params['money_unit'],
                ]); ?>
                <br/>
                <?= Yii::t('frontend', 'FREE First week') ?></a>
                <br/>
                <br/>
                <a href="<?= Url::to(['mps/register-service', 'pkg_id' => $package->PKG_ID]) ?>"
                   class="btn btn-lucky"><?= Yii::t('frontend', 'Đăng ký'); ?></a>
            </div>
        </div>
        <br/>
        <?php /*
 <div class="modal fade" id="reg-modal-<?= $package->PKG_TYPE; ?>" tabindex="-1" role="dialog" aria-labelledby="reg-modalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id=""><?= $modalTitle ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div id="modal-body" class="modal-body">
                        <?php $mpsUrl = Url::to(['mps/register-service']);?>
                        <?= Yii::t('frontend', 'Bạn có muốn đăng ký dịch vụ để tra cứu tử vi và nhận bản tin tử vi hàng ngày không?'); ?>
                        <br />
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer" >
                        <form method="post" action="<?= Url::to(['subscriber/register']); ?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('frontend', 'Đóng'); ?></button>
                            <a href="<?= $mpsUrl; ?>" class="btn btn-lucky"><?= Yii::t('frontend', 'Đồng ý'); ?></a>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
        */ ?>
    <?php endforeach; ?>
<?php else: ?>
    <?php
    $modalTitle = Yii::t('frontend', 'Hủy dịch vụ');
    $btnName = 'unregister';
    ?>
    <div class="card">
        <div class="card-body">
            <?= Yii::t('frontend', 'Bạn đang sử dụng dịch vụ {service_name}', [
                'service_name' => Yii::$app->params['service_name']
            ]); ?>
            <?php if ($serviceSub->REGISTER_DATE): ?>
                <br/>
                <?= Yii::t('frontend', 'Ngày đăng ký'); ?>: <?= date('d/m/Y', strtotime($serviceSub->REGISTER_DATE)); ?>
            <?php endif; ?>
            <?php if ($serviceSub->EXPIRE_DATE): ?>
                <br/>
                <?= Yii::t('frontend', 'Expired date'); ?>: <?= date('d/m/Y', strtotime($serviceSub->EXPIRE_DATE)); ?>
            <?php endif; ?>
            <br/>
            <br/>
            <button class="btn btn-lucky" data-toggle="modal"
                    data-target="#cancel-modal"><?= Yii::t('frontend', 'Hủy dịch vụ'); ?></button>
        </div>
    </div>
    <!-- get otp Modal -->
    <div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="reg-modalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id=""><?= $modalTitle ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div id="modal-body" class="modal-body">

                    <?php $mpsUrl = Url::to(['mps/cancel-service']); ?>
                    <?= Yii::t('frontend', 'Bạn có muốn Hủy dịch vụ?'); ?>
                    <br/>

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <form method="post" action="<?= Url::to(['subscriber/register']); ?>">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal"><?= Yii::t('frontend', 'Đóng'); ?></button>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <a href="<?= $mpsUrl; ?>" class="btn btn-lucky"><?= Yii::t('frontend', 'Đồng ý'); ?></a>

                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>"
                               value="<?= Yii::$app->request->csrfToken; ?>"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

