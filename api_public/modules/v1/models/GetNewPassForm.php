<?php

namespace api_public\modules\v1\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class GetNewPassForm extends Model
{

    public $username;
    public $rememberMe = true;
    public $isFirstLogin = false;
    public $token = null;
    private $_user;
    public $captcha;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'trim'],

            // Google captcha
            [['captcha'], ReCaptchaValidator::className(), 'secret' => Yii::$app->params['recaptcha_secret']]
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('backend', 'Password'),
            're_password' => Yii::t('backend', 'Re-type new password'),
            'captcha' => Yii::t('backend', 'Captcha'),
            'username' => Yii::t('backend', 'Username'),
        ];
    }
}
