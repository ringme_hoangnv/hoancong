<?php

namespace api_public\modules\v1\models;

use common\models\AdvertismentBase;
use Yii;

/**
 *
 * @property-read string $imageUrl
 */
class Advertisment extends AdvertismentBase
{
    public function getImageUrl()
    {
        return Yii::$app->params['media_url'] . $this->IMAGE_PATH;
    }
}