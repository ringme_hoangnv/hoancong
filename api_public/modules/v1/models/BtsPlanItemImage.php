<?php

namespace api_public\modules\v1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class BtsPlanItemImage extends \common\models\BtsPlanItemImageBase {



    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    public function rules()
    {
        return [
            [['plan_id', 'item_id', 'created_by', 'updated_by', 'approved_by'], 'integer'],
            [['plan_id', 'item_id', 'image_path'], 'required'],
            [['id', 'created_at', 'updated_at', 'approved_at'], 'safe'],
            [['image_path'], 'string', 'max' => 255],
            [['status'], 'integer'],
            [['note'], 'string', 'max' => 1000]
        ];
    }

}