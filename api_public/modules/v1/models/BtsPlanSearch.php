<?php

namespace api_public\modules\v1\models;

use backend\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BtsPlan;

/**
 * BtsPlanSearch represents the model behind the search form about `backend\models\BtsPlan`.
 */
class BtsPlanSearch extends BtsPlan
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id', 'item_category_id', 'created_by'], 'integer'],
            [['name', 'description', 'is_exported', 'status', 'start_at', 'end_at', 'bts_code', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BtsPlan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $username = Yii::$app->session->get('msisdn');
        $user = User::findOne(['username' => $username]);

        switch ($user->user_type) {
            case 'admin':
                $query->andFilterWhere(['partner_id' => $this->partner_id]);
                break;
            case 'ho':
                $query->andFilterWhere(['partner_id' => $this->partner_id]);
                break;
            case 'branch':
                // Chi show cac plan do user tao ra
                $query->andWhere([
                    'created_by' => $user->id,
                ])->andFilterWhere(['partner_id' => $this->partner_id]);
                break;
            case 'partner':
                // Chi xem duoc plan gan voi partner tuong ung
                $query->andWhere(['partner_id' => $user->partner_id]);
                break;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,

            'item_category_id' => $this->item_category_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_exported', $this->is_exported])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'bts_code', $this->bts_code]);

        return $dataProvider;
    }
}
