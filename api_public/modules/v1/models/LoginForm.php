<?php

namespace api_public\modules\v1\models;

use backend\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    public $isFirstLogin = false;
    public $token = null;
    private $_user;
    public $captcha;

    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            [['username'], 'trim'],

            // Google captcha
            [['captcha'], 'safe'], // tam bo qua
            //[['captcha'], ReCaptchaValidator::className(), 'secret' => Yii::$app->params['recaptcha_secret']]
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('backend', 'Incorrect username or password.'));
            }
        }
    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = User::findOne([
                'username' => $this->username,
                'user_type' => ['partner', 'ho'], // Chi cho ho / partner duoc phep login tu api
            ]);
        }
        return $this->_user;
    }

    public function attributeLabels() {
        return [
            'password' => Yii::t('backend', 'Password'),
            're_password' => Yii::t('backend', 'Re-type new password'),
            'captcha' => Yii::t('backend', 'Captcha'),
            'username' => Yii::t('backend', 'Username'),
        ];
    }
}
