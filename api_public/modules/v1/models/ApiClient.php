<?php

namespace api_public\modules\v1\models;

use Yii;

class ApiClient extends \common\models\ApiClientBase
{
    public static function getByIds($ids)
    {
        return ApiClient::find()
            ->where([
                'id' => $ids,
            ])
            ->all();
    }
    public static function getOneByAuthorizationCode($authorizationCode) {
        return self::find()->where(['AUTHORIZATION_CODE' => $authorizationCode])->one();
    }
}