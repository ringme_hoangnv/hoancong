<?php

namespace api_public\modules\v1\models;

use Yii;

class CungHoangDao extends \common\models\CungHoangDaoBase {

	public function beforeSave($insert)
    {
        if (!Yii::$app->user->can('approve-content')) {
            $this->STATUS = 0;
        }

        return parent::beforeSave($insert); 
    }
}