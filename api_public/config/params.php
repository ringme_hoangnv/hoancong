<?php

return [
    'datetime_format' => 'Y-m-d H:i:s',
    'client_id' => 'test',
    'client_secret' => 'test',
    'client_authorization_timeout' => 60 * 60,
    'default_language' => 'en',
    'api_token_expired_time' => 60,

    // Cau hinh google captcha
    'recaptcha_secret' => '6Ld4o8YaAAAAAA0zRURhLSBr4MhDVaPf6qSpgaEc',
    'recaptcha_site_key' => '6Ld4o8YaAAAAAEl1xdARc9TXLhRhkvVoj_nMbXMG',
    'media_url' => 'http://103.143.206.63:8002',
];