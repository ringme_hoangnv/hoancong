<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php')
);
$routing = array_merge(require(__DIR__ . '/routing_v1.php'));

return [
    'id' => 'api-public',
    'language' => 'en',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'api_public\modules\v1\Module',
        ],
    ],
    'controllerNamespace' => 'api_public\controllers',
    'defaultRoute' => 'site/index',
    'components' => [
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],

            ],
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/modules/v1/views',
                'baseUrl' => '@web/modules/v1/views',
                'pathMap' => [
                    '@app/views' => '@app/modules/v1/views',
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'api_public\models\auth\User',
            'enableAutoLogin' => false,
        ],
//        'redis' => [
//            'class' => 'yii\redis\Connection',
//            'hostname' => '192.168.146.252',
//            'port' => 9600,
//            'database' => 2,
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logFile' => '@logs/api_public/error.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logFile' => '@logs/api_public/warning.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@logs/api_public/info.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'logFile' => '@logs/api_public/queries.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/v1/api/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            //chi cho phep chay cac pretty url
            //'enableStrictParsing' => true,
            //'suffix' => '.html',
            'rules' => $routing,
        ]
    ],
    'params' => $params,
];
