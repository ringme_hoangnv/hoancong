<?php
/**
 * Created by PhpStorm.
 *
 * Date: 11/20/2015
 * Time: 1:45 PM
 */

namespace api_public\libs;

use api\modules\v1\models\TopicV1;
use api\modules\v1\models\VtAlbumV1;
use api\modules\v1\models\VtRingBackToneV1;
use api\modules\v1\models\VtSingerV1;
use api\modules\v1\models\VtSongV1;
use api\modules\v1\models\VtVideoV1;
use common\models\ChargeBase;
use frontend\models\ServiceSubscriber;
use Yii;
use yii\base\Exception;
use yii\db\Expression;

class ApiHelper
{

    static function formatResponse($errCode, $data = [], $message = '')
    {
        return [
            'errorCode' => $errCode,
            'message' => ($message) ? $message : ApiResponseCode::getMessage($errCode),
            'data' => $data,
        ];
    }

    static function errorResponse()
    {
        return [
            'errorCode' => ApiResponseCode::SYSTEM_ERROR,
            'message' => ApiResponseCode::getMessage(ApiResponseCode::SYSTEM_ERROR),
            'data' => [],
        ];
    }

    static function insertArrayIndex($array, $new_element, $index)
    {
        /*         * * get the start of the array ** */
        $start = array_slice($array, 0, $index);
        /*         * * get the end of the array ** */
        $end = array_slice($array, $index);
        /*         * * add the new element to the array ** */
        $start[] = $new_element;
        /*         * * glue them back together and return ** */
        return array_merge($start, $end);
    }

    static function imagePath($path, $type = "album")
    {
        try {
            if (strlen($path) == 0) {
                return Yii::$app->params[$type . '_default_media_path'];
            } else {
                $filename = Yii::$app->params['upload_path'] . $path;
                if (is_file($filename)) {
                    return Yii::$app->params['media_path'] . $path;
                } else {
                    return Yii::$app->params[$type . '_default_media_path'];
                }
            }
        } catch (Exception $e) {
            return Yii::$app->params[$type . '_default_media_path'];
        }
    }

    public static function getMsisdn()
    {
        // Lay tu header
        return (isset($_SERVER['HTTP_MSISDN'])) ? $_SERVER['HTTP_MSISDN'] : null;
    }

    /**
     * Che so dien thoai de dam bao ATTT
     * @param $msisdn
     * @return string
     */
    public static function hideMsisdn($msisdn)
    {
        return substr($msisdn, 0, -4) . 'xxx' . substr($msisdn, -1);
    }

    public static function getViewContentStatusCode($contentId = null)
    {
        $msisdn = self::convertMsisdn(Yii::$app->session->get('msisdn', null));
        if (!$msisdn) {
            // Chua dang nhap
            return 401;
        } else {
            // Kiem tra la sub chua
            //khanhlh
//            $serviceSub = Yii::$app->user->identity->getServiceSubscriber();
            $serviceSub = ServiceSubscriber::findOne(['MSISDN' => $msisdn]);
            if ($serviceSub && $serviceSub->isActivatingService()) {
                // Dang active dich vu
                return 1;
            } else {
                // Da dang nhap, kiem tra da mua content chua
                $check = ChargeBase::find()
                    ->where([
                        'MSISDN' => $msisdn,
                        'CHARGE_STATUS' => '0',
                        'CONTENT_ID' => $contentId,
                    ])
                    ->andWhere([
                        '>', 'CHARGE_TIME', new Expression('SYSDATE-1')  // charge_time + 24h > now
                    ])
                    ->exists();

                if ($check) {
                    // da thanh toan
                    return 1;
                }
                // Da dang nhap nhung Chua DK hoac ko active dich vu
                return 0;
            }
        }
    }

    public static function convertMsisdn($msisdn, $type = '9x')
    {
        $msisdn = trim($msisdn);
        if (preg_match(Yii::$app->params['phonenumber_pattern'], $msisdn, $matches)) {
            switch ($type) {
                case '9x':
                    return isset($matches[2]) ? $matches[2] : $msisdn;
                    break;
                case '84x':
                    return isset($matches[2]) ? Yii::$app->params['country_code'] . $matches[2] : $msisdn;
                    break;
                case '09x':
                    return isset($matches[2]) ? '0' . $matches[2] : $msisdn;
                    break;
            }
        }
        return $msisdn;
    }
}
