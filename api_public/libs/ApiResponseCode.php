<?php

namespace api_public\libs;

use Yii;

class ApiResponseCode {

    const SUCCESS = 0;

    const REQUIRE_LOGIN = 1;
    const UNKNOWN_METHOD = 2;
    const AUTHORIZATION_CODE_REQUIRED = 3;
    const LOGIN_IS_REQUIRED = 4;                        // need to login
    const CSRF_TOKEN_REQUIRED = 5;
    const CSRF_TOKEN_INVALID = 6;
    const PERMISSION_IS_REQUIRED = 7;                   // need permission to execute function
    const POST_METHOD_IS_REQUIRED = 8;                  // need post method to execute function
    const NO_DATA = 9;                                  // need post method to execute function

    const AUTH_INVALID_ID = 10;                         // invalid client (wrong id and secret)
    const AUTH_INVALID_AUTHORIZATION_CODE = 11;         // invalid authorization code
    const AUTH_INVALID_USERNAME_PASSWORD = 12;          // invalid username password
    const AUTH_USER_NOT_ACTIVE = 13;                    // user is not active
    const AUTH_USER_IS_LOCKED = 14;                     // user is locked
    const AUTH_USER_PASS_IS_REQUIRED = 15;              // username and password is required
    const AUTH_CLIENT_ID_SECRET_IS_REQUIRED = 16;       // client id and client secret is required
    const INVALID_PARAMS = 18;
    const FORM_INVALID = 19;
    const INVALID_CAPTCHA = 20;

    const PROMOTION_NO_DATA = 50;
    const PROMOTION_OUT_OF_TIME = 51;
    const PROMOTION_OVER_LUCKYCODE = 52;
    const PROMOTION_OVER_ENJOY_TIMES = 53;

    const MPS_NOT_RESPONSE = 999997;
    const NOT_IN_WHITELIST = 999998;
    const SYSTEM_ERROR = 999999;

    public static function getMessage($errorCode) {
        $mess = [
            self::SUCCESS => Yii::t('api', 'Successful'),
            self::SYSTEM_ERROR => Yii::t('api', 'System is busy. Please try again later!'),
            self::NOT_IN_WHITELIST => Yii::t('api', 'Service is testing! Please come back later!'),
            self::MPS_NOT_RESPONSE => Yii::t('api', 'Mps not response'),
            self::UNKNOWN_METHOD => Yii::t('api', 'Unknown method'),
            self::AUTHORIZATION_CODE_REQUIRED => Yii::t('api', 'Authorization code is required'),
            self::LOGIN_IS_REQUIRED => Yii::t('api', 'Please login to perform this action'),
            self::REQUIRE_LOGIN => Yii::t('api', 'Require login.'),
            self::PERMISSION_IS_REQUIRED => Yii::t('api', 'Need permission to execute function.'),
            self::POST_METHOD_IS_REQUIRED => Yii::t('api', 'Need post method to execute function.'),
            self::NO_DATA => Yii::t('api', 'No data'),

            // Authenticate & Authorization
            self::AUTH_INVALID_ID => Yii::t('api', 'Wrong client id and client secret'),
            self::AUTH_INVALID_AUTHORIZATION_CODE => Yii::t('api', 'Invalid authorization code'),
            self::AUTH_INVALID_USERNAME_PASSWORD => Yii::t('api', 'Invalid username password'),
            self::AUTH_USER_NOT_ACTIVE => Yii::t('api', 'Your account is not active'),
            self::AUTH_USER_IS_LOCKED => Yii::t('api', 'Your account is locked'),
            self::AUTH_USER_PASS_IS_REQUIRED => Yii::t('api', 'Username and Password is required'),
            self::AUTH_CLIENT_ID_SECRET_IS_REQUIRED => Yii::t('api', 'Client id and Client secret is required'),
            // SyncMedia
            self::INVALID_PARAMS => Yii::t('api', 'Invalid input parameter'),
            self::FORM_INVALID => Yii::t('api', 'Invalid input parameter'),
            self::INVALID_CAPTCHA => Yii::t('api', 'Invalid captcha'),

            self::PROMOTION_NO_DATA             => Yii::t('api', 'Promotion not found!'),
            self::PROMOTION_OUT_OF_TIME         => Yii::t('api', 'Out of promotion time'),
            self::PROMOTION_OVER_LUCKYCODE      => Yii::t('api', 'Over lucky code quota'),
            self::PROMOTION_OVER_ENJOY_TIMES    => Yii::t('api', 'Over enjoy times'),
        ];

        if ($mess[$errorCode]) {
            return $mess[$errorCode];
        }
        return '';
    }

}
