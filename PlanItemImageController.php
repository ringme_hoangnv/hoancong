<?php

namespace api_public\modules\v1\controllers;

use api_public\libs\ApiHelper;
use api_public\libs\ApiResponseCode;
use backend\models\BtsItem;
use backend\models\BtsPlan;
use api_public\modules\v1\models\BtsPlanItemImage;
use backend\models\BtsPlanItem;
use backend\models\ItemCategory;
use backend\models\User;
use common\core\SuperAppApiGw;
use common\models\BtsPlanItemBase;
use common\models\BtsPlanItemImageBase;
use common\models\db\CronMsg;
use common\models\SystemSettingBase;
use Yii;
use yii\base\BaseObject;
use yii\bootstrap\ActiveForm;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\validators\ImageValidator;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class PlanItemImageController extends ApiController
{
    public function actionUpload($plan_id, $item_id)
    {

        $errorCode = ApiResponseCode::SUCCESS;

        if (Yii::$app->request->isPost) {
            $maxFileSize = Yii::$app->params['image_upload_size'] * 1024; // Kb

            $planId = $plan_id;
            $itemId = $item_id;


            $plan = BtsPlan::findOne($planId);
            $item = BtsItem::findOne($itemId);
            if (!$plan || !$item) {
                $errorCode = ApiResponseCode::NO_DATA;
                return ApiHelper::formatResponse(
                    $errorCode,
                    [],
                    Yii::t('backend', 'Plan or item not found!')
                );
            }

            // check so luong image
            $planItem = BtsPlanItemBase::findOne([
                'plan_id' => $planId,
                'item_id' => $itemId,
            ]);
            $countImages = BtsPlanItemImageBase::find()
                ->where([
                    'plan_id' => $planId,
                    'item_id' => $itemId,
                ])
                ->count();
            if ($countImages >= $planItem->image_num) {
                return ApiHelper::formatResponse(
                    ApiResponseCode::FORM_INVALID,
                    [],
                    Yii::t('backend', 'You cannot upload more image!', [
                        'total' => $planItem->image_num
                    ])
                );
            }

            $res = [];
            $initialPreview = [];
            $initialPreviewConfig = [];
            $images = UploadedFile::getInstancesByName("image_upload");

            if (count($images) > 0) {
                foreach ($images as $key => $image) {
                    $validator = new ImageValidator();
                    if (!$validator->validate($image)) {

                        return ApiHelper::formatResponse(
                            ApiResponseCode::FORM_INVALID,
                            [],
                            Yii::t('backend', 'Invalid image!')
                        );
                    }

                    if ($image->size > $maxFileSize * 1024) {

                        return ApiHelper::formatResponse(
                            ApiResponseCode::FORM_INVALID,
                            [],
                            Yii::t('backend', 'The maximum size of the image cannot exceed 2M')
                        );
                    }
                    if (!in_array(strtolower($image->extension), array('gif', 'jpg', 'jpeg', 'png'))) {
                        return ApiHelper::formatResponse(
                            ApiResponseCode::FORM_INVALID,
                            [],
                            Yii::t('backend', 'Please upload a standard image file, support gif, jpg, png and jpeg.')
                        );
                    }

                    $dir = '/plan/' . date('ymd', strtotime($plan->created_at)) . $planId . '/' . $itemId . '/';

                    $fileNameClean = preg_replace('/[^A-Za-z0-9\-]/', '', $image->baseName); // Removes special chars.
                    $fileNameClean = Inflector::camel2id($fileNameClean) . '-' . time();
                    $filename = $fileNameClean . '.' . $image->getExtension();

                    $savePath = Yii::$app->params['media_path'] . $dir;
                    // If the folder does not exist, create a new folder
                    if (!file_exists($savePath)) {
                        FileHelper::createDirectory($savePath);
                    }

                    $file = $savePath . $filename;

                    if ($image->saveAs($file)) {
                        // resize image neu qua to
                        if ($image->size > 1.5 * 1024 * 1024) {
                            Image::thumbnail($file, 1200, null)->save($file);
                        }

                        $imgpath = '/' . basename(Yii::$app->params['media_path']) . $dir . $filename;
                        // Luu db
                        $imgObj = new BtsPlanItemImage();
                        $imgObj->plan_id = $planId;
                        $imgObj->item_id = $itemId;
                        $imgObj->image_path = $imgpath;
                        $imgObj->status = BtsPlanItemImage::STATUS_DRAFT;
                        $imgObj->created_by = $this->user->id;
                        $imgObj->updated_by = $this->user->id;
                        $imgObj->save(false);

                        // Upload thanh cong image
                        // update trang thai item
                        $planItem = BtsPlanItemBase::updateStatus($planId, $itemId);

                        $partnerUser = $this->user;
                        if ($partnerUser->user_type == 'partner') {
//                            $partner = $partnerUser->partner;
//                            $hoNumbers = explode(',', SystemSettingBase::getConfigByKey('HO_PHONE_NUMBER'));
//                            if (count($hoNumbers)) {
//                                // Gui tin nhan cho HO
//                                $msg = SystemSettingBase::getConfigByKey('MT_PARTNER_UPLOAD_NEW_IMAGE');
//                                // Partner {partner_name}: user {partner_user} has just uploaded a new image in plan {plan_name}
//                                $msg = Yii::t('backend', $msg, [
//                                    'partner_name' => ($partner)? $partner->name: '',
//                                    'partner_user' => $partnerUser->username,
//                                    'plan_name' => $plan->name,
//                                ]);
//
//                                SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $hoNumbers, $msg);
//                            }
                            $this->actionCheckSlAnh($planId, $itemId, $plan, $item);

                        }

                        return ApiHelper::formatResponse(
                            $errorCode,
                            [
                                'image_id' => $imgObj->id,
                                'plan_id' => $planId,
                                'item_id' => $itemId,
                                'image_path' => $imgObj->getImagePathUrl(),
                                'status' => $imgObj->status,
                                'note' => $imgObj->note,
                                'item_status' => ($planItem) ? $planItem->status : null
                            ]
                        );
                    } else {
                        return ApiHelper::formatResponse(
                            ApiResponseCode::SYSTEM_ERROR,
                            [],
                            Yii::t('backend', 'Cannot save file.')
                        );
                    }
                } // for
            }

            return ApiHelper::formatResponse(
                ApiResponseCode::INVALID_PARAMS
            );
        }

        return ApiHelper::formatResponse(
            ApiResponseCode::INVALID_PARAMS
        );
    }

    public function actionCheckSlAnh($planId, $itemId, $plan, $item)
    {
        // check so luong image
        $planItem = BtsPlanItemBase::findOne([ //so luong anh can
            'plan_id' => $planId,
            'item_id' => $itemId,
        ]);
        $soluonganhyeucau = $planItem->image_num;

        $soluonganhdadang = BtsPlanItemImageBase::find() //so luong anh da dang
        ->where([
            'plan_id' => $planId,
            'item_id' => $itemId,
            'status' => 0
        ])->count();

        //check xem so luong anh dang len da du chua de gui tin
        $soluonganhtuchoi  = BtsPlanItemImageBase::find() //so luong anh da dang
        ->where([
            'plan_id' => $planId,
            'item_id' => $itemId,
            'status' => 2
        ])->count();
        $partnerUser = Yii::$app->user->identity;
        if ( $soluonganhdadang == $soluonganhyeucau || $soluonganhdadang = $soluonganhtuchoi) {
            $category = ItemCategory::findOne($plan->item_category_id);
            $hoNumbers = explode(',', SystemSettingBase::getNumberQlVung($planId));
            Yii::info( $hoNumbers ."========================" );
            if (count($hoNumbers)) {
                // Gui tin nhan cho HO
                $msg = SystemSettingBase::getConfigByKey('SENT_MSG_LOCAL_MANAGEMENT');
                // {bts_code}- {category}- {item} CARGA FOTO {datetime} ({soluonganhupload}/{soluonganhyeucau})
                $msg = Yii::t('backend', $msg, [
                    'bts_code' => ($plan) ? $plan->bts_code : '',
                    'category' => $category->name,
                    'item' => $item->name,
                    'datetime' => date('Y-m-d H:i:s'),
                    'soluonganhupload' => $soluonganhdadang,
                    'soluonganhyeucau' => ($soluonganhyeucau = $soluonganhdadang) ? $soluonganhyeucau : $soluonganhtuchoi,
                ]);
                //check thoi gian xem co dang trong gio lam viec khong
                $time_now = date('Y-m-d H:i:s');
                if (CronMsg::instance()->checkTimeWork($time_now)) { // thoi gian dang bi sai
                    Yii::info('gui tin nhan ==============' . 'so cua ho ==========> ' . $hoNumbers .'msg ====> ' .$msg);
                    SuperAppApiGw::sendMt(Yii::$app->params['sms_shortcode'], $hoNumbers, $msg);
                    //tao crontab de gui nhac nho
                    $time_sent = date('Y-m-d H:i:s', strtotime('+3 hours', strtotime($time_now)));
                    $cron_msg = new CronMsg();
                    $cron_msg->plan_id = $planId;
                    $cron_msg->item_id = $itemId;
                    $cron_msg->sent_to = 'DEPUTY';
                    $cron_msg->sent_at = CronMsg::instance()->checkTimeWork($time_sent) ? $time_sent : date('Y-m-d H:i:s', strtotime("+1 days", strtotime('09:00:00')));
                    $cron_msg->msg_local_manager = $msg;
                    $cron_msg->save(false);
                } else {
                    //tao crontab de gui nhac nho
                    $cron_msg = new CronMsg();
                    $cron_msg->plan_id = $planId;
                    $cron_msg->item_id = $itemId;
                    $cron_msg->sent_to = 'MANAGER_LOCAL';
                    $cron_msg->sent_at = date('Y-m-d H:i:s', strtotime("+1 days", strtotime('09:00:00')));
                    $cron_msg->msg_local_manager = $msg;
                    $cron_msg->number_manager_local = $hoNumbers[0];
                    $cron_msg->save(false);
                }
            }
        }
    }

    public function actionInfo($id)
    {
        $errorCode = ApiResponseCode::SUCCESS;

        $image = $this->findModel($id);

        if ($image) {
            $image->image_path = $image->getImagePathUrl();
            $image->approved_by = ($image->approvedBy) ? $image->approvedBy->username : $image->approved_by;
        } else {
            $errorCode = ApiResponseCode::NO_DATA;
        }

        return ApiHelper::formatResponse(
            $errorCode,
            $image
        );
    }

    public function actionApprove($id)
    {
        $data = [];

        if ($this->user->user_type != 'ho') {
            return ApiHelper::formatResponse(
                ApiResponseCode::PERMISSION_IS_REQUIRED
            );
        }

        $image = $this->findModel($id);

        if ($image) {

            $image->status = BtsPlanItemImage::STATUS_APPROVED; // Duyet
            $image->note = null;
            $image->approved_by = $this->user->id;
            $image->approved_at = date('Y-m-d H:i:s');
            if ($image->save()) {
                // update trang thai item
                $planItem = BtsPlanItemBase::updateStatus($image->plan_id, $image->item_id);

                $errorCode = ApiResponseCode::SUCCESS;
                $data = [
                    'image_id' => $image->id,
                    'plan_id' => $image->plan_id,
                    'item_id' => $image->item_id,
                    'image_path' => $image->getImagePathUrl(),
                    'status' => $image->status,
                    'note' => $image->note,
                    'item_status' => ($planItem) ? $planItem->status : null
                ];
                $this->actionCheckItem($image);
            } else {
                $errorCode = ApiResponseCode::SYSTEM_ERROR;
                $data = $image->getErrors();
            }

        } else {
            $errorCode = ApiResponseCode::NO_DATA;
        }
        $this->actionCheckItem($image);

        return ApiHelper::formatResponse(
            $errorCode,
            $data
        );
    }

    public function actionCheckItem($model)
    {
        $plan_id = $model->plan_id;
        $item_id = $model->item_id;
        $soluonganhdaxem = \backend\models\BtsPlanItemImage::find()
            ->andWhere([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
            ])
            ->andWhere(['<>', 'status', '0'])->count();
        $item = BtsPlanItem::findOne([
            'plan_id' => $plan_id,
            'item_id' => $item_id
        ]);
        $item1 = BtsItem::findOne($item_id);
        $plan = BtsPlan::findOne($plan_id);
        $soluonganhyeucau = $item->image_num;
        $category = ItemCategory::findOne($plan->item_category_id);
        if ($soluonganhyeucau == (int)$soluonganhdaxem) {

            //LORXXXX  - CATEGORIAXXX - FOTO PARARRAYOS Y TAPA TORRE  CARGA FOTO {datetime}  {soluonganhpheduyet}/{tongsoanh} APROBADO  {tongsoanhtuchoi}/{tongsoanh} OBSERVACIÓN FECHA {datetimepheduyet}
            $msg = "{bts_code}- {category}- {item}  CARGA FOTO {datetime}  {soluonganhpheduyet}/{tongsoanh} APROBADO  {tongsoanhtuchoi}/{tongsoanh} OBSERVACIÓN FECHA {datetimepheduyet}";
            $soluonganhpheduyet = BtsPlanItemImage::find()->where([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
                'status' => 1
            ])->count();
            $soluonganhtuchoi = BtsPlanItemImage::find()->where([
                'plan_id' => $plan_id,
                'item_id' => $item_id,
                'status' => 2
            ])->count();
            $msg = \Yii::t('backend', $msg, [
                'datetime' => date("Y-m-d H:i:s"),
                'soluonganhpheduyet' => $soluonganhpheduyet,
                'tongsoanh' => $soluonganhyeucau,
                'tongsoanhtuchoi' => $soluonganhtuchoi,
                'datetimepheduyet' => date("Y-m-d H:i:s"),
                'bts_code' => ($plan) ? $plan->bts_code : '',
                'category' => $category->name,
                'item' => $item1->name,
            ]);
            $partner = User::findOne(['id' => $model->created_by]);
            if ($partner->msisdn) {
                SuperAppApiGw::sendMt(\Yii::$app->params['sms_shortcode'], $partner->msisdn, $msg);
            }
        }
    }


    public function actionDisapprove()
    {
        $data = [];
        $requestData = json_decode(Yii::$app->request->getRawBody());
        $id = $requestData->id;

        if ($this->user->user_type != 'ho') {
            return ApiHelper::formatResponse(
                ApiResponseCode::PERMISSION_IS_REQUIRED
            );
        }

        $image = $this->findModel($id);

        if ($image) {

            $note = $requestData->note;

            $image->status = BtsPlanItemImage::STATUS_DISAPPROVED; // Huy Duyet
            $image->note = $note;
            $image->approved_by = $this->user->id;
            $image->approved_at = date('Y-m-d H:i:s');

            if ($image->save()) {
                // update trang thai item
                $planItem = BtsPlanItemBase::updateStatus($image->plan_id, $image->item_id);

                $errorCode = ApiResponseCode::SUCCESS;
                $data = [
                    'image_id' => $image->id,
                    'plan_id' => $image->plan_id,
                    'item_id' => $image->item_id,
                    'image_path' => $image->getImagePathUrl(),
                    'status' => $image->status,
                    'note' => $image->note,
                    'item_status' => ($planItem) ? $planItem->status : null
                ];

            } else {
                $errorCode = ApiResponseCode::SYSTEM_ERROR;
                $data = $image->getErrors();
            }

        } else
            $errorCode = ApiResponseCode::NO_DATA;
        $this->actionCheckItem($image);
        return ApiHelper::formatResponse(
            $errorCode,
            $data
        );
    }

    public function actionDelete($id)
    {
        $image = $this->findModel($id);

        if ($image) {
            if ($image->status == BtsPlanItemImage::STATUS_APPROVED) {
                // Ko cho xoa anh da duoc duyet
                return ApiHelper::formatResponse(
                    403, [], Yii::t('backend', 'You cannot delete a approved image!')
                );
            }

            $planId = $image->plan_id;
            $itemId = $image->item_id;

            if ($image->delete()) {
                $errorCode = ApiResponseCode::SUCCESS;


                // update status
                BtsPlanItemBase::updateStatus($planId, $itemId);
            } else
                $errorCode = ApiResponseCode::SYSTEM_ERROR;
        } else {
            $errorCode = ApiResponseCode::NO_DATA;
        }
        return ApiHelper::formatResponse(
            $errorCode
        );
    }

    protected function findModel($id)
    {
        $createdBy = ($this->user->user_type == 'partner') ? $this->user->id : null;
        $model = BtsPlanItemImage::find()
            ->where(['id' => $id])
            ->andFilterWhere(['created_by' => $createdBy])
            ->one();

        return $model;
    }
}